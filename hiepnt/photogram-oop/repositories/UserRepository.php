<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 10:46 AM
 */
include_once('../../config/database.php');
include_once('../../models/User.php');
include_once('../../models/UserProfile.php');

class UserRepository extends Database
{

    /**
     * KIỂM TRA ĐĂNG NHẬP
     * @param $username
     * @param $password
     * @return null|User
     */

    public function checkUser($username, $password) {
        $sql = "SELECT * FROM tbl_user WHERE username='$username' AND password='$password'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();

        // nếu tồn tại ng dùng
        if ($row)
            return $this->rowToModelUser($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng sang object bảng user
     * @param $row
     */
    public function rowToModelUser($row) {
        $id = $row['id'];
        $username = $row['username'];
        $password = $row['password'];
        $last_login = $row['last_login'];

        return new User($id, $username, $password, $last_login);
    }

    /**
     * LẤY PROFILE NGƯỜI DÙNG
     * @param $userId
     */

    public function getProfile($userId){
        $sql = "SELECT * FROM tbl_profile, tbl_user  WHERE user_id=id AND user_id='$userId'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        return $this->rowToModelProfile($row);
    }

    /**
     * LẤY USER TỪ ID
     * @param $id
     */

    public function getUser($id){
        $sql = "SELECT * FROM tbl_user  WHERE id='$id'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        return $this->rowToModelUser($row);
    }
    /**
     * CHỈNH SỬA THÔNG TIN NGƯỜI DÙNG
     * @param: $FullName, $Address, $Gender, $Phone, $DOB, $Avatar
     */

    public function editProfile($Id ,$FullName, $Address, $Gender, $Phone, $DOB, $Avatar){
        $sql = "UPDATE tbl_profile SET FullName='$FullName', Address='$Address', Gender='$Gender', Phone='$Phone', DOB='$DOB', Avatar='$Avatar' WHERE user_id = '$Id'";
        $row =  $this->connection->query($sql);
        return $row;
    }

    /**
     * chuyển đổi từ mảng sang object ghi vào bảng UserProfile
     * @param $row
     */
    public function rowToModelProfile($row) {
        $user_id = $row['user_id'];
        $full_name = $row['FullName'];
        $dob = $row['DOB'];
        $address = $row['Address'];
        $gender = $row['Gender'];
        $phone = $row['Phone'];
        $avatar = $row['Avatar'];

        return new UserProfile($user_id, $full_name, $dob, $address, $gender, $phone, $avatar);
    }
}