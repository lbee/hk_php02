<?php
/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 3:48 PM
 */
if(!isset($_SESSION['userId'])){
    header("Location:../../routes/user/login.php");
}
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../album/index.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="<?php echo AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar(); ?>" alt="avatar" width="24" height="24"/><?php echo $user->getUsername(); ?><span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../userprofile/index.php">Profile</a></li>
                        <li><a href="../userprofile/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
