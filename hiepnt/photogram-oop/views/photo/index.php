<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/css/fullPhoto.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>
<?php include('../../views/layout/header.php'); ?>
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách photo thuộc album</h3>
        </div>
        <div class="panel-body">

            <div class="ui">
                <!-- Form để upload ảnh -->
                <form method="post" enctype="multipart/form-data">
                    Upload image:
                    <!-- Chấp nhận mọi thể loại ảnh sử dụng accept attr-->
                    <input type="file" class="ui primary button" accept="image/*" id="fileUpload" name="photoUpload"/>
                    <!-- Nút submit, bị ẩn nếu như file chưa được chọn -->
                    <button name="submit" class="ui secondary button hide submit" type="submit">
                        Save
                    </button>
                    <button class="ui button hide cancel" type="submit">
                        Cancel
                    </button>
                </form>
            </div>
            <br/>

            <!-- lấy danh sách ảnh để ở đây-->
            <div class="ui special cards three column">
                <?php foreach ($listPhoto as $value) : ?>
                    <div class="card">
                        <div class="blurring dimmable image">
                            <div class="ui inverted dimmer">
                                <div class="content">
                                    <div class="center">
                                        <div id="showFull" class="ui button">Xem</div>
                                        <!-- Truyền 2 tham số (1): Photo id để xóa photo (2): album_id để quay về album-->
                                        <a href="?album_id=<?php echo $value['album_id'] ?>&id=<?php echo $value['id']; ?>&act=delete">

                                            <div class="negative ui button">Xóa</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <img id="myImg" src="<?php echo $value['photoURL'] . '/' . $value['photoName']; ?>" alt="">
                        </div>
                    </div>
                    <!-- The Modal -->
                    <div id="myModal" class="modal">
                        <span class="close">×</span>
                        <img class="modal-content" id="img01">
                        <div id="caption"></div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>

<!-- END BODY -->

<?php include('../../views/layout/footer.php'); ?>