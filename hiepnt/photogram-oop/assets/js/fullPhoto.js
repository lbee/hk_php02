/**
 * Created by Admin on 10/19/2016.
 */
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var show = document.getElementById('showFull');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
show.onclick = function(){
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
}

// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];
//
// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//     modal.style.display = "none";
// }

//PRESS ESC OR TOUCH TO SCREEN
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        modal.style.display = "none";
    }
});
$(document).mouseup(function(){
    modal.style.display = "none";
});

