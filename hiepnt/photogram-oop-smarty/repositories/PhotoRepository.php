<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 2:45 PM
 */
include_once('../../config/database.php');
include_once('../../models/Album.php');
include_once('../../models/Photo.php');

class PhotoRepository extends Database
{
    /**
     * LẤY ALBUM ẢNH
     * @param $userId
     */

    public function getAlbum($user_id){
        $sql = "SELECT * FROM tbl_album  WHERE  user_id='$user_id'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        return $this->rowToModelAlbum($row);
    }

    /**
     * chuyển đổi từ mảng sang object bảng user
     * @param $row
     */
    public function rowToModelAlbum($row) {
        $id = $row['id'];
        $name = $row['name'];
        $user_id = $row['user_id'];

        return new Album($id, $name, $user_id);
    }

    /**
     * Lấy danh sach album thuộc người dùng $user_id
     * @param $conn
     * @param $user_id
     * @return mixed
     */
    public function retrieveAlbum($user_id)
    {
        $sql = "SELECT * FROM `tbl_album` WHERE user_id=$user_id ORDER BY id DESC";
        $obj = $this->connection->query($sql);
        $rows = $obj ->fetchAll();
        return $rows;
    }

    /**
     * Thêm mới album
     * @param $conn : MYSQL connection
     * @param $name : Album name
     */
    public function createAlbum($name, $user_id)
    {
        $sql = "INSERT INTO `tbl_album`(`name`,`user_id`) VALUES('$name','$user_id')";
        $result = $this->connection->query($sql);
        return $result;
    }

    /**
     * @param $conn : MYSQL connection
     * @param $id : id của album
     */
    public function editAlbum($id, $name)
    {
        $sql = "UPDATE `tbl_album` SET `name`='$name' WHERE id=" . $id;
        $result = $this->connection->query($sql);
        return $result;
    }
    /**
     * @param $conn : MYSQL connection
     * @param $id : id của album
     */
    public function deleteAlbum($id)
    {
        $sql = "DELETE FROM `tbl_album` WHERE id='$id'";
        $result = $this->connection->query($sql);
        return $result;
    }
    /**
     * Load ảnh đầu tiên để hiển thị lên album
     * @param $conn : MYsqli connection
     * @param $album_id : ID album
     */
    public function getFirstImage($album_id)
    {
        //giới hạn 1 kết quả trả về, sắp xếp theo id giảm dần (tức là ảnh mới nhất)
        $sql = "SELECT * FROM `tbl_photo` WHERE `album_id`=$album_id ORDER BY `id` DESC LIMIT 1";
        $object = $this->connection->query($sql);
        $row1 = $object->fetch();
        //kiểm tra xem có ảnh hay không bằng cách đếm số lượng object khi trả về, nếu có thì đưa ra đường dẫn và tên ảnh
        if($row1) {
            echo $row1['photoURL'] . '/' . $row1['photoName'];
        }
        //nếu album trống (ko có ảnh) thì load ảnh mặc định (GREY.PNG)
        else{
            return "../../img/grey.PNG";
        }

    }

    /*-------------------------------------HẾT ALBUM ẢNH-----------------------------------------*/

    /**
     * LẤY ẢNH
     * @param $userId
     */

    public function getPhoto($album_id){
        $sql = "SELECT * FROM tbl_photo  WHERE  id='$album_id'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        return $this->rowToModelPhoto($row);
    }

    /**
     * chuyển đổi từ mảng sang object bảng photo
     * @param $row
     */
    public function rowToModelPhoto($row) {
        $id = $row['id'];
        $album_id = $row['album_id'];
        $photoName = $row['photoName'];
        $photoURL = $row['photoURL'];

        return new Album($id, $album_id, $photoName, $photoURL);
    }
    /**
     * Upload photo
     * @param $conn : PDO connection
     * @param $album_id : Album_id
     * @param $path : Đường dẫn lưu ảnh
     * @param $photoName : Tên ảnh
     */
    public function uploadPhoto($album_id, $path, $photoName)
    {
        $sql = "INSERT INTO `tbl_photo`(`album_id`,`photoName`,`photoURL`) VALUES ($album_id,'$photoName','$path')";
        // echo $sql; die();
        $result = $this->connection->query($sql);
        return $result ? true : false;
    }

    /**
     * @param $conn : MYSQL connection
     * @param $id : id của album
     */
//    public function editPhoto($id, $name)
//    {
//        $sql = "UPDATE `tbl_photo` SET `name`='$name' WHERE id=" . $id;
//        $result = $this->connection->query($sql);
//        return $result;
//    }
    /**
     * @param $conn : MYSQL connection
     * @param $id : id của album
     */
    public function deletePhoto($id)
    {
        $sql = "DELETE FROM `tbl_photo` WHERE id='$id'";
        $result = $this->connection->query($sql);
        return $result;
    }

    public function retrievePhoto($album_id)
    {
        $sql = "SELECT * FROM `tbl_photo` WHERE album_id=$album_id ORDER BY id DESC";
        $obj = $this->connection->query($sql);
        $rows = $obj ->fetchAll();
        return $rows;
    }
}