<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 10:50:28
  from "C:\xampp\htdocs\photogram-oop\views\user\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_58106e54c7a6e4_80729564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b322219c6104d385df67223bcd0589fe978d8967' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\user\\login.tpl',
      1 => 1477308967,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58106e54c7a6e4_80729564 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '1475058106e54c0c057_84503876';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
</head>
<body>
<div class="container">
    <form action="" class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
            <p class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p>
        <?php }?>
        <!-- Username -->
        <label class="sr-only" for="inputUsername">Username</label>
        <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
               name="username">
        <!-- Password -->
        <br/>
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
               name="password">
        <br/>
        <!-- Submit btn -->
        <p><a href="../user/register.php">Chưa có tài khoản, đăng kí ở đây</a></p>
        <br/>
        <button type="submit" name="signin" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>
</div>
</body>
</html><?php }
}
