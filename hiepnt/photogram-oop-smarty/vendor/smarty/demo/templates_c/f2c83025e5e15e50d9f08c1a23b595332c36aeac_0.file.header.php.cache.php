<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 05:41:03
  from "C:\xampp\htdocs\photogram-oop\views\layout\header.php" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_581025cfab0179_36932678',
  'has_nocache_code' => true,
  'file_dependency' => 
  array (
    'f2c83025e5e15e50d9f08c1a23b595332c36aeac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\layout\\header.php',
      1 => 1477316427,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_581025cfab0179_36932678 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '26810581025cfaa8104_97218289';
echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'<?php
\';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>
/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 3:48 PM
 */
if(!isset($_SESSION['userId'])){
    header("Location:../../routes/user/login.php");
}
<?php echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'?>\';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../album/index.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="<?php echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'<?php \';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>
echo AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar(); <?php echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'?>\';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>
" alt="avatar" width="24" height="24"/><?php echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'<?php \';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>
echo $user->getUsername(); <?php echo '/*%%SmartyNocache:26810581025cfaa8104_97218289%%*/<?php echo \'?>\';?>/*/%%SmartyNocache:26810581025cfaa8104_97218289%%*/';?>
<span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../userprofile/index.php">Profile</a></li>
                        <li><a href="../userprofile/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php }
}
