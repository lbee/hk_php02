<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 10:29:40
  from "C:\xampp\htdocs\photogram-oop\views\album\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_581069745031a1_13940851',
  'has_nocache_code' => true,
  'file_dependency' => 
  array (
    'ee3d7dc7b175f33888d0f9db51d782e0a3a0477e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\album\\index.tpl',
      1 => 1477453403,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../views/layout/header.tpl' => 1,
    'file:../../views/layout/footer.php' => 1,
  ),
),false)) {
function content_581069745031a1_13940851 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '8171581069744678c6_63664588';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>
<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách album</h3>
        </div>
        <div class="panel-body">
            <div class="content addNew">
                <div class="ui input">
                    <button class="ui primary button">
                        Thêm mới
                    </button>

                    <form id="addNewForm" method="POST" style="display: none">
                        <input placeholder="Enter album name" type="text" name="albumName">
                        <button type="submit" class="ui primary button" name="submit">
                            Save
                        </button>
                    </form>
                </div>
            </div>
            <br/>
            <!-- dẩy UI card từ semantic UI về thành 3 cột-->
            <div class="ui three column grid">
                <!-- Album đầu tiên(dùng để add)-->
                <?php echo '/*%%SmartyNocache:8171581069744678c6_63664588%%*/<?php echo \'<?php
                \';?>/*/%%SmartyNocache:8171581069744678c6_63664588%%*/';
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listAlbum']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <!-- Album list-->
                <div class="column">
                    <div class="ui fluid card">
                        <a class="image" href="../photo/index.php?album_id=<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">
                            <!-- load ảnh đầu tiên trong album -->
                            <img src="<?php ob_start();
echo $_smarty_tpl->tpl_vars['row']->value['id'];
$_prefixVariable1=ob_get_clean();
echo $_smarty_tpl->tpl_vars['firtPhoto']->value->getFirstImage($_prefixVariable1);?>
" alt=""/>
                        </a>
                        <div class="content">
                            <a class="header"
                               href="../photo/index.php?album_id=<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</a>
                        </div>
                        <div class="extra content">
                            <a href="#" data-toggle="modal" data-target="#exampleModal<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">
                                <i class="edit icon"></i>
                                Sửa
                            </a>
                            <a href="?act=delete&album_id=<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" >
                                <i class="delete icon"></i>
                                Xóa
                            </a>
                        </div>
                    </div>
                </div>
                <!-- modal box dùng cho edit, mỗi album có 1 modal box riêng, xác định thông qua #data-target(dòng 72) và #exampleModal (dòng 84)-->
                <div id="exampleModal<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Chỉnh sửa tên album</h4>
                            </div>
                            <form method="post" action="?act=edit&album_id=<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">
                                <div class="modal-body">
                                    <input name="album_name" class="form-control" type="text"
                                           placeholder="Tên album"
                                           value="<?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="edit" class="btn btn-primary">Save
                                    </button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            </div>
            <!-- -->
        </div>
        <!-- END BODY -->

<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/footer.php', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
