<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 10:50:28
  from "C:\xampp\htdocs\photogram-oop\views\layout\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_58106e547b8dc5_46426202',
  'has_nocache_code' => true,
  'file_dependency' => 
  array (
    '53d141a4853fd81b6ba5f0504396579dfb3c6c8e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\layout\\header.tpl',
      1 => 1477471822,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58106e547b8dc5_46426202 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '2614458106e547a76c3_70200187';
echo '/*%%SmartyNocache:2614458106e547a76c3_70200187%%*/<?php echo \'<?php
\';?>/*/%%SmartyNocache:2614458106e547a76c3_70200187%%*/';?>
/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 3:48 PM
 */

<?php echo '/*%%SmartyNocache:2614458106e547a76c3_70200187%%*/<?php echo \'?>\';?>/*/%%SmartyNocache:2614458106e547a76c3_70200187%%*/';?>

<?php if (isset($_smarty_tpl->tpl_vars['userId']->value)) {?>
    <?php echo '<script'; ?>
 LANGUAGE=JavaScript>location.href='../../routes/user/login.php';<?php echo '</script'; ?>
>
<?php }?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../album/index.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['LINK_AVATAR']->value;?>
" alt="avatar" width="24" height="24"/><?php echo $_smarty_tpl->tpl_vars['user']->value->getUsername();?>
<span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../userprofile/index.php">Profile</a></li>
                        <li><a href="../user/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php }
}
