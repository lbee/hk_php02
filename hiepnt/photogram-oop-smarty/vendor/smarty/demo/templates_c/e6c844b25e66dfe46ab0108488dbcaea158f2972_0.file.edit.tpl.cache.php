<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 10:22:20
  from "C:\xampp\htdocs\photogram-oop\views\userprofile\edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_581067bc9b15c7_76877199',
  'has_nocache_code' => true,
  'file_dependency' => 
  array (
    'e6c844b25e66dfe46ab0108488dbcaea158f2972' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\userprofile\\edit.tpl',
      1 => 1477470138,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../views/layout/header.tpl' => 1,
    'file:../../views/layout/footer.php' => 1,
  ),
),false)) {
function content_581067bc9b15c7_76877199 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\xampp\\htdocs\\photogram-oop\\vendor\\smarty\\libs\\plugins\\modifier.date_format.php';
$_smarty_tpl->compiled->nocache_hash = '1433581067bc914366_61661009';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chỉnh sửa thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
</head>
<body>
<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- Chỉnh sửa profile -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Chỉnh sửa profile</h3>
        </div>
        <div class="panel-body">
            <!-- Form cập nhật-->

            <form method="post" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="fullname">FullName</label>
                    <input type="text" name="fname" class="form-control" id="fullname" placeholder="FullName"
                           value="<?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getFullName();?>
">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea class="form-control" name='address' id="address"
                              placeholder="Địa chỉ"><?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getAddress();?>
</textarea>
                </div>

                <!-- Giới tính -->
                <div class="form-group">
                    <label for="gender">Gender</label>
                    <div class="radio">
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['userProfile']->value->getGender();
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 == 1) {?>
                        <label>
                            <input type="radio" name="gender" id="gender" value="1" checked>
                            Nam
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender" value="0">
                            Nữ
                        </label>
                        <?php echo '/*%%SmartyNocache:1433581067bc914366_61661009%%*/<?php echo \'<?php \';?>/*/%%SmartyNocache:1433581067bc914366_61661009%%*/';?>
} else { <?php echo '/*%%SmartyNocache:1433581067bc914366_61661009%%*/<?php echo \'?>\';?>/*/%%SmartyNocache:1433581067bc914366_61661009%%*/';?>

                        <label>
                            <input type="radio" name="gender" id="gender" value="1">
                            Nam
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender" value="0" checked>
                            Nữ
                        </label>
                        <?php }?>
                    </div>
                </div>

                <!-- Phone -->
                <div class="form-group">
                    <label for="phoneNum">Phone number</label>
                    <input class="form-control" name="phone" id="phoneNum" placeholder="Phone number"
                           value="<?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getPhone();?>
"/>
                </div>

                <!-- DOB -->
                <div class="form-group">
                    <label for="dob">DOB</label>
                    <input class="form-control" id="dob" name='dob' placeholder="DOB" data-date-format="dd-mm-yyyy"
                           value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['userProfile']->value->getDob(),"%d-%m-%Y");?>
"/>
                </div>

                <!-- Avatar -->
                <p><label for="avatar">Avatar</label></p>
                <img src="<?php echo $_smarty_tpl->tpl_vars['LINK_AVATAR']->value;?>
" alt="" width="200" height="auto">
                <input type="file" id="avatar" name="avatar" accept="image/*"/>
                <br/>
                <!-- Gửi form -->
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>

            </form>
            <!-- Hết form -->

        </div>
    </div>
</div>
<!-- End -->
<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/footer.php', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
