<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 10:50:28
  from "C:\xampp\htdocs\photogram-oop\views\userprofile\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_58106e5478ede7_43444581',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f775e5f546df86d7761c5a32d9dd28ed9af0af01' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\userprofile\\index.tpl',
      1 => 1477453403,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../views/layout/header.tpl' => 1,
    'file:../../views/layout/footer.php' => 1,
  ),
),false)) {
function content_58106e5478ede7_43444581 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\xampp\\htdocs\\photogram-oop\\vendor\\smarty\\libs\\plugins\\modifier.date_format.php';
$_smarty_tpl->compiled->nocache_hash = '2077158106e546f92e8_48622189';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>

<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- START BODY -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <?php if ($_smarty_tpl->tpl_vars['userProfile']->value->getAvatar() != '') {?>
                        <img src="<?php echo $_smarty_tpl->tpl_vars['LINK_AVATAR']->value;?>
" alt="" class="img-rounded img-responsive" />
                        <?php } else { ?>
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                        <?php }?>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getFullName();?>
</h4>
                        <small><cite title="San Francisco, USA"><?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getAddress();?>
 <i class="glyphicon glyphicon-map-marker">
                                </i></cite></small>
                        <p>
                            Gender : <?php if ($_smarty_tpl->tpl_vars['userProfile']->value->getGender() == '1') {?>Nam<?php } else { ?>Nữ<?php }?>
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i><?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getFullName();?>

                            <br />
                            <i class="glyphicon glyphicon-phone"></i><?php echo $_smarty_tpl->tpl_vars['userProfile']->value->getPhone();?>

                            <br />
                            <i class="glyphicon glyphicon-gift"></i><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['userProfile']->value->getDob());?>

                            <br/>
                            <i class="glyphicon glyphicon-pencil"></i><a href="edit.php" >Edit Profile</a>
                            <br />
                            <a href="photo.php" class="btn btn-primary">View albums </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END BODY -->

<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/footer.php', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
