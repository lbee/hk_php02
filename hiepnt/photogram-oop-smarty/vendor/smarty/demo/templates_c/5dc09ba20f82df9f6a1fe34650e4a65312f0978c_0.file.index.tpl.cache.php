<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-26 09:53:36
  from "C:\xampp\htdocs\photogram-oop\views\photo\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_58106100ee9559_28476256',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5dc09ba20f82df9f6a1fe34650e4a65312f0978c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\photogram-oop\\views\\photo\\index.tpl',
      1 => 1477455065,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../../views/layout/header.tpl' => 1,
    'file:../../views/layout/footer.php' => 1,
  ),
),false)) {
function content_58106100ee9559_28476256 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '2737858106100e5be82_96565266';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/css/fullPhoto.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>
<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách photo thuộc album</h3>
        </div>
        <div class="panel-body">

            <div class="ui">
                <!-- Form để upload ảnh -->
                <form method="post" enctype="multipart/form-data">
                    Upload image:
                    <!-- Chấp nhận mọi thể loại ảnh sử dụng accept attr-->
                    <input type="file" class="ui primary button" accept="image/*" id="fileUpload" name="photoUpload"/>
                    <!-- Nút submit, bị ẩn nếu như file chưa được chọn -->
                    <button name="submit" class="ui secondary button hide submit" type="submit">
                        Save
                    </button>
                    <button class="ui button hide cancel" type="submit">
                        Cancel
                    </button>
                </form>
            </div>
            <br/>

            <!-- lấy danh sách ảnh để ở đây-->
            <div class="ui special cards three column">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listPhoto']->value, 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                <div class="card">
                    <div class="blurring dimmable image">
                        <div class="ui inverted dimmer">
                            <div class="content">
                                <div class="center">
                                    <div id="showFull" class="ui button">Xem</div>
                                    <!-- Truyền 2 tham số (1): Photo id để xóa photo (2): album_id để quay về album-->
                                    <a href="?album_id=<?php echo $_smarty_tpl->tpl_vars['value']->value['album_id'];?>
&id=<?php echo $_smarty_tpl->tpl_vars['value']->value['album_id'];?>
&act=delete">

                                        <div class="negative ui button">Xóa</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <img id="myImg" src="<?php echo $_smarty_tpl->tpl_vars['value']->value['photoURL'];?>
/<?php echo $_smarty_tpl->tpl_vars['value']->value['photoName'];?>
" alt="">
                    </div>
                </div>
                <!-- The Modal -->
                <div id="myModal" class="modal">
                    <span class="close">×</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            </div>
        </div>
    </div>
</div>

<!-- END BODY -->

<?php $_smarty_tpl->_subTemplateRender('file:../../views/layout/footer.php', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
