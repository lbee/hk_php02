<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 2:38 PM
 */
require_once('../../controllers/Controller.php');
include_once ('../../repositories/PhotoRepository.php');
include_once ('../../repositories/UserRepository.php');
include_once ('../../controllers/FunctionImage.php');

class PhotoController extends Controller
{
    private $userRepository;
    private $photoRepository;

    public function __construct()
    {
        parent::__construct();
        $this->photoRepository = new PhotoRepository();
        $this->userRepository = new UserRepository();
    }

    /**
     * HIỂN THỊ PROFILE
     */
    public function viewAlbum(){
        $user_id = $_SESSION['userId'];
        //LẤY THÔNG TIN TRONG BẢNG USER
        $user = $this->userRepository->getUser($user_id);
        //LẤY THÔNG TIN TRONG BẢNG PROFILE
        $userProfile = $this->userRepository->getProfile($user_id);
        //LẤY THÔNG TIN TRONG BẢNG ALBUM
        $userAlbum = $this->photoRepository->getAlbum($user_id);

        //var_dump($object); die();

        //DS ALBUM
        $listAlbum = $this->photoRepository->retrieveAlbum($user_id);
        $firtPhoto = $this->photoRepository;
        //SMARTY
        $LINK_AVATAR = AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar();
        //VIEW
        $this->render('../../views/album/index.tpl', compact('listAlbum','userProfile','userAlbum','user','firtPhoto','LINK_AVATAR','userId'));
        //include_once ('../../views/album/index.php');
    }

    /**
     * ADD ALBUM
     */
    public function addAlbum(){
        $user_id = $_SESSION['userId'];
        if (isset($_POST['submit'])) {
            $albumName = $_POST['albumName'];
            $albumCreate = $this->photoRepository->createAlbum($albumName,$user_id);
            if ($albumCreate) {
                //var_dump($albumCreate);
                //Refresh lại trang web
                header("location: index.php");
            } else {
                echo "<script>alert('Kiểm tra lại câu lệnh SQL');</script>";
            }
        }
    }
    /**
     * SỦA ALBUM
     */
    public function editAlbum(){
        if (isset($_GET['album_id'])) {
            $album_id = $_GET['album_id'];
            if (isset($_GET['act']) && $_GET['act']=='edit') {
                $name = $_POST['album_name'];
                $result = $this->photoRepository->editAlbum($album_id,$name);
                if ($result) {
                    header("Location:index.php");
                }
            }
        }
    }

    /**
     * XÓA ALBUM
     */
    public function deleteAlbum(){
        if (isset($_GET['album_id'])) {
            $album_id = $_GET['album_id'];
            //echo $album_id;
            if (isset($_GET['act']) && $_GET['act']=='delete') {
                //echo $_GET['act'];
                $result = $this->photoRepository->deleteAlbum($album_id);
                if ($result) {
                    header("Location:index.php");
                }
            }
        }
    }
    /**
     * HIỂN THỊ ẢNH
     */
    public function viewPhoto(){
        $user_id = $_SESSION['userId'];
        $album_id = $_GET['album_id'];
        //LẤY TRONG THÔNG TIN USER
        $user = $this->userRepository->getUser($user_id);
        //LẤY TRONG THÔNG TIN PROFILE
        $userProfile = $this->userRepository->getProfile($user_id);
        //LẤY TRONG THÔNG TIN ALBUM
        $userAlbum = $this->photoRepository->getPhoto($album_id);

        //DS ẢNH
        $listPhoto = $this->photoRepository->retrievePhoto($album_id);

        // Code xử lí upload photo
        if (isset($_POST['submit'])) {
            $file = $_FILES['photoUpload']['name'];
            $targetDir = PHOTOPATH.$user->getUsername();
            // di chuyển photo từ ngoài vào thư mục đích
            FunctionImage::savePhoto($user->getUsername(), $file, $_FILES['photoUpload']['tmp_name']);
            $result = $this->photoRepository->uploadPhoto($album_id, $targetDir, basename($file));
            //trả về TRUE/FALSE
                if ($result) {
                    //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                    header("Refresh:0");
                }
        }
        //SMARTY
        $LINK_AVATAR = AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar();
        //VIEW
        $this->render('../../views/photo/index.tpl', compact('listPhoto','userProfile','userAlbum','user','LINK_AVATAR','userId'));
        //include_once ('../../views/photo/index.php');
    }

    /**
     * XÓA ẢNH
     */
    public function deletePhoto(){
        if (isset($_GET['album_id'])&& isset($_GET['id'])) {
            $album_id = $_GET['album_id'];
            $photo_id = $_GET['id'];
            //echo $album_id;
            if (isset($_GET['act']) && $_GET['act']=='delete') {
                //echo $_GET['act'];
                $result = $this->photoRepository->deletePhoto($photo_id);
                if ($result) {
                    header("Location:index.php?album_id=".$album_id);
                }
            }
        }
    }
}