<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 11:36 AM
 */
class FunctionImage
{
    /**
     * LƯU AVATAR
     * @param $username
     * @param $file
     * @param $fileTmpName
     */
    public static function saveAvatar($username, $file, $fileTmpName){

        //mỗi $username có 1 đường dẫn avatar khác nhau, được định nghĩa bới imgavatar/[tên username]/
        $targetDir = AVATARPATH . $username;
        //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới , basename là tên file
        $targetFile = $targetDir . '/' . basename($file);

        //kiểm tra xem có tồn tại đường dẫn $targetDir không, nếu không có sẽ thêm mới, tránh trường hợp vào server để tạo bằng tay
        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/X
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;

        if (isset($_POST["submit"])) {
            if($fileTmpName!=null){
                $check = getimagesize($fileTmpName);
                if ($check === false) {
                    echo "<script>alert('File is not image')</script>";
                    $uploadOk = 0;
                }
            }else{
                $uploadOk = 0;
            }

        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            //di chuyển file từ bên ngoài (chỗ chọn file upload) về thư mục $targetDir
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                header("Refresh:0");
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }

    }

    /**
     * Hàm dùng để di chuyển photo từ ngoài (thư mục chọn) vào server (đường dẫn được khai báo PHOTOPATH/[tên usename]
     * @param $conn : MYSQLi connection
     * @param $album_id : album_id
     * @param $username : tên thư mục của người dùng
     * @param $file : file vật lí
     * @param $fileTmpName : tên tạm thời của file
     */
    public static function savePhoto($username, $file, $fileTmpName)
    {
        $targetDir = PHOTOPATH . $username;
        //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới
        $targetFile = $targetDir . '/' . basename($file);

        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/E
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;

        if (isset($_POST["submit"])) {
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File is not image')</script>";
                $uploadOk = 0;
            }
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //lưu thông tin vào cơ sở dữ liệu
                //$result = uploadPhoto($conn, $album_id, $targetDir, basename($file));
                //trả về TRUE/FALSE
//                if ($result) {
//                    //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
//                    header("Refresh:0");
//                }

            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }
    }
}