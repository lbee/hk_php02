<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 10:55 AM
 */
require_once('../../controllers/Controller.php');
include_once ('../../repositories/UserRepository.php');
include_once ('../../controllers/FunctionImage.php');

class UserController extends Controller
{
    private $userRepository;
    private $image;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->image = new FunctionImage();
    }

    /**
     * XỬ LÝ LOGIN
     */
    public function actionLogin() {
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);

            $user = $this->userRepository->checkUser($username, $password_hash);
            if ($user != null) {
                // tạo session
                $_SESSION['userId'] = $user->getId();
                header('Location: ../userprofile/index.php');
            } else {
                $error = 'Xin hãy kiểm tra lại tên người dùng và mật khẩu!';
            }
        }

        // view
        //include_once('../../views/user/login.php');
        $this->render('../../views/user/login.tpl', compact('error'));
    }

    public function actionLogout(){
        session_destroy();
        // chuyển người dùng sang trang đăng nhập
        return $this->redirect('login.php');
    }

    /**
     * HIỂN THỊ PROFILE
     */
    public function viewProfile(){
        $userId = $_SESSION['userId'];
        //LẤY THÔNG TIN TỪ BẢNG USER
        $user = $this->userRepository->getUser($userId);
        //LẤY THÔNG TIN TỬ BẢNG PROFILE
        $userProfile = $this->userRepository->getProfile($userId);

        //SMARTY
        $LINK_AVATAR = AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar();

        //VIEW
        //include_once ('../../views/userprofile/index.php');
        $this->render('../../views/userprofile/index.tpl', compact('LINK_AVATAR','userProfile','user','userId'));
    }

    /**
     * CHỈNH SỬA THÔNG TIN CÁ NHÂN
     */
    public function editProfile(){
        $userId = $_SESSION['userId'];
        $user = $this->userRepository->getUser($userId);
        $userProfile = $this->userRepository->getProfile($userId);

        if (isset($_POST['submit'])) {
            //$username = $_POST['username'];
            //khi cập nhật lại user profile thì phải cấp lại session cho username để lưu trên menu
            //$_SESSION['username'] = $username;
            //$password = $_POST['password'];
            $fullName = $_POST['fname'];
            $address = $_POST['address'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            //do dùng datepicker nên phải chuyển format từ datepicker dạng d-m-Y sang dạng Y-m-d để lưu vào CSDL
            $dob = DateTime::createFromFormat("d-m-Y", $_POST['dob'])->format("Y-m-d");
            $file = $_FILES['avatar']['name'];
            $file == null ? $file = $userProfile->getAvatar() : '';
            //lưu ảnh avatar vào máy
            FunctionImage::saveAvatar($user->getUsername(), $file, $_FILES['avatar']['tmp_name']);
            if(!isset($error_img)){
                $result = $this->userRepository->editProfile($userId, $fullName, $address, $gender, $phone, $dob, $file);
                if ($result) {
                    header("Refresh:0");
                } else {
                    echo "<script>alert('123');</script>";
                }
            }else{
                echo $error_img;
            }
        }

        //SMARTY
        $LINK_AVATAR = AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar();

        //VIEW
        //include_once ('../../views/userprofile/edit.php');
        $this->render('../../views/userprofile/edit.tpl', compact('LINK_AVATAR','userProfile','user','userId'));
    }

}