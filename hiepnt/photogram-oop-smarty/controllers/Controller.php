<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/24/2016
 * Time: 7:48 PM
 */
require '../../vendor/smarty/libs/Smarty.class.php';

class Controller
{
    protected $smarty;
    public function __construct()
    {
        // khởi tạo smarty
        $this->smarty = new Smarty();

        // cấu hình các thư mục lưu trữ
        $this->smarty->template_dir = '../../views';
        $this->smarty->compile_dir = '../../vendor/smarty/demo/templates_c';

        // cấu hình smarty
        $this->smarty->force_compile = true; // chỉ sử dụng dòng này trong quá trình phát triển hệ thống
        $this->smarty->debugging = true;
        $this->smarty->caching = true;
        $this->smarty->cache_lifetime = 120;
    }

    /**
     * chuyển người dùng sang trang với $url được cung cấp
     * @param $url
     */
    public function redirect($url) {
        // nếu url chứa / = chuyển trang khác controller -> thêm ../ để đi ra folder /routes
        if( strpos($url, '/') !== false )
            $url = "../$url";

        header("Location: $url");
    }

    /**
     * @param $template template view file
     * @param array $data data array to assign for template view
     */
    public function render($template, $data = array()) {
        foreach ($data as $name => $value) {
            $this->smarty->assign($name, $value);
        }

        $this->smarty->display($template);
    }
}