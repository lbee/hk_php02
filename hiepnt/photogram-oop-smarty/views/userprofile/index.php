<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>
<?php include('../../views/layout/header.php'); ?>

<!-- START BODY -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <?php if ($userProfile->getAvatar() != null): ?>
                            <img src="<?php echo AVATARPATH.$user->getUsername().'/'.$userProfile->getAvatar(); ?>" alt="" class="img-rounded img-responsive" />
                        <?php else: ?>
                            <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?php echo $userProfile->getFullName(); ?></h4>
                        <small><cite title="San Francisco, USA"><?php echo $userProfile->getAddress(); ?> <i class="glyphicon glyphicon-map-marker">
                                </i></cite></small>
                        <p>
                            <?php echo 'Gender : ' .($userProfile->getGender() == '1'?'Nam':'Nu '); ?>
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i><?php echo $userProfile->getFullName(); ?>
                            <br />
                            <i class="glyphicon glyphicon-phone"></i><?php echo $userProfile->getPhone(); ?>
                            <br />
                            <i class="glyphicon glyphicon-gift"></i><?php echo date('F j, Y', strtotime($userProfile->getDob())); ?>
                            <br/>
                            <i class="glyphicon glyphicon-pencil"></i><a href="edit.php" >Edit Profile</a>
                            <br />
                            <a href="photo.php" class="btn btn-primary">View albums </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END BODY -->
<?php include('../../views/layout/footer.php'); ?>