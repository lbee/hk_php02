<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>

{include file='../../views/layout/header.tpl'}

<!-- START BODY -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        {if $userProfile->getAvatar() != '' }
                        <img src="{$LINK_AVATAR}" alt="" class="img-rounded img-responsive" />
                        {else}
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                        {/if }
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>{$userProfile->getFullName()}</h4>
                        <small><cite title="San Francisco, USA">{$userProfile->getAddress()} <i class="glyphicon glyphicon-map-marker">
                                </i></cite></small>
                        <p>
                            Gender : {if $userProfile->getGender() == '1'}Nam{else}Nữ{/if}
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i>{$userProfile->getFullName()}
                            <br />
                            <i class="glyphicon glyphicon-phone"></i>{$userProfile->getPhone()}
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>{$userProfile->getDob()|date_format}
                            <br/>
                            <i class="glyphicon glyphicon-pencil"></i><a href="edit.php" >Edit Profile</a>
                            <br />
                            <a href="photo.php" class="btn btn-primary">View albums </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END BODY -->

{include file='../../views/layout/footer.php'}