<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chỉnh sửa thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
</head>
<body>
{include file='../../views/layout/header.tpl'}
<!-- Chỉnh sửa profile -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Chỉnh sửa profile</h3>
        </div>
        <div class="panel-body">
            <!-- Form cập nhật-->

            <form method="post" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="fullname">FullName</label>
                    <input type="text" name="fname" class="form-control" id="fullname" placeholder="FullName"
                           value="{$userProfile->getFullName()}">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea class="form-control" name='address' id="address"
                              placeholder="Địa chỉ">{$userProfile->getAddress()}</textarea>
                </div>

                <!-- Giới tính -->
                <div class="form-group">
                    <label for="gender">Gender</label>
                    <div class="radio">
                        {if {$userProfile->getGender()} == 1}
                        <label>
                            <input type="radio" name="gender" id="gender" value="1" checked>
                            Nam
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender" value="0">
                            Nữ
                        </label>
                        <?php } else { ?>
                        <label>
                            <input type="radio" name="gender" id="gender" value="1">
                            Nam
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender" value="0" checked>
                            Nữ
                        </label>
                        {/if}
                    </div>
                </div>

                <!-- Phone -->
                <div class="form-group">
                    <label for="phoneNum">Phone number</label>
                    <input class="form-control" name="phone" id="phoneNum" placeholder="Phone number"
                           value="{$userProfile->getPhone()}"/>
                </div>

                <!-- DOB -->
                <div class="form-group">
                    <label for="dob">DOB</label>
                    <input class="form-control" id="dob" name='dob' placeholder="DOB" data-date-format="dd-mm-yyyy"
                           value="{$userProfile->getDob()|date_format:"%d-%m-%Y"}"/>
                </div>

                <!-- Avatar -->
                <p><label for="avatar">Avatar</label></p>
                <img src="{$LINK_AVATAR}" alt="" width="200" height="auto">
                <input type="file" id="avatar" name="avatar" accept="image/*"/>
                <br/>
                <!-- Gửi form -->
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>

            </form>
            <!-- Hết form -->

        </div>
    </div>
</div>
<!-- End -->
{include file='../../views/layout/footer.php'}