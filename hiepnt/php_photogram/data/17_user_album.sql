-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2016 at 04:30 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `17_user_album`
--

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `PhotoID` int(11) NOT NULL,
  `AlbumID` int(11) NOT NULL,
  `Username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `PhotoURL` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`PhotoID`, `AlbumID`, `Username`, `PhotoURL`) VALUES
(1, 1, 'hiepnt', 'http://i.imgur.com/vIoW5DVb.jpg'),
(2, 2, 'mrhhh', 'http://i.imgur.com/elkieqGb.jpg'),
(3, 2, 'hiepnt', 'http://i.imgur.com/6X6cpgQb.jpg'),
(4, 3, 'hiepnt', 'http://i.imgur.com/JatUyarb.jpg'),
(5, 4, 'hiepnt', 'http://i.imgur.com/qvVioFdb.jpg'),
(6, 5, 'hiepnt', 'http://i.imgur.com/UnSNVbdb.jpg'),
(12, 1, 'mrhhh', 'http://i.imgur.com/x38L99db.jpg'),
(13, 3, 'mrhhh', 'http://i.imgur.com/2WL7SJqb.jpg'),
(14, 4, 'mrhhh', 'http://i.imgur.com/cPEVAV7b.jpg'),
(15, 5, 'mrhhh', 'http://i.imgur.com/KdUEa0E.jpg'),
(16, 1, 'hiepnt', 'http://i.imgur.com/VyAlcbqb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(32) CHARACTER SET latin1 NOT NULL,
  `Lastlogin` date NOT NULL,
  `DeleteFlag` tinyint(1) NOT NULL DEFAULT '0',
  `Email` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Username`, `Password`, `Lastlogin`, `DeleteFlag`, `Email`) VALUES
('hiepnt', '202cb962ac59075b964b07152d234b70', '2016-09-06', 0, 'mrhbat@gmail.com'),
('mrhhh', 'c4ca4238a0b923820dcc509a6f75849b', '2016-09-01', 0, 'hieppro@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `Username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Avatar` varchar(100) CHARACTER SET latin1 NOT NULL,
  `Fullname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `DoB` date NOT NULL,
  `Gender` tinyint(1) NOT NULL,
  `Address` varchar(200) CHARACTER SET latin1 NOT NULL,
  `Phone` varchar(15) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`Username`, `Avatar`, `Fullname`, `DoB`, `Gender`, `Address`, `Phone`) VALUES
('hiepnt', 'http://pixel.nymag.com/imgs/daily/vulture/2015/04/28/28-dragon-ball-z-goku.w529.h529.jpg', 'Nguyen Tien Hiep', '1994-05-03', 1, 'Quang Ninh', '01234567789'),
('mrhhh', 'http://xyz.com', 'Nguyen Tien Hiep', '1994-05-03', 1, 'Quang Ninh', '01234567789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`PhotoID`),
  ADD KEY `Username` (`Username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD PRIMARY KEY (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `PhotoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `user` (`Username`);

--
-- Constraints for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD CONSTRAINT `userprofile_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `user` (`Username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
