<?php
/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 24-Oct-16
 * Time: 23:50
 */
require_once('../../controllers/PhotoController.php');

// khởi tạo controller
$controller = new PhotoController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionView();