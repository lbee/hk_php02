<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 13-Oct-16
 * Time: 21:53
 */
include ('../../repositories/AlbumRepository.php');
include('../../repositories/ProfileRepository.php');
include_once ('../../repositories/PhotoRepository.php');
include ('../../models/Album.php');
include ('../../models/User.php');
class AlbumController
{
    private $albumRepository;

    public function __construct()
    {
        $this->albumRepository = new AlbumRepository();
    }

    /**
     * xem danh sách album
     */
    public function actionIndex()
    {
        if (!isset($_SESSION['_user'])) {
            header("Location: ../../routes/user/login.php");
        } else {
            $user = unserialize($_SESSION['_user']);

            $profileRepository = new ProfileRepository();

            $profile = $profileRepository->loadProfile($user->getId());
            $albums = $this->albumRepository->retrieveAlbum($user->getId());
        }
        include('../../views/album/index.php');
    }

    /**
     * xử lý thêm album
     */
    public function actionCreate() {
        // yêu cầu login
        if (!isset($_SESSION['_user'])) {
            header("Location: ../../routes/user/login.php");
        }

        $user = unserialize($_SESSION['_user']);
        // thêm mới album
        if (isset($_POST['submit'])) {
            $albumName = $_POST['albumName'];
            $albumCreate = $this->albumRepository->createAlbum($albumName, $user->getId());
            if (!$albumCreate) {
                $error = 'Kiểm tra lại câu lệnh SQL';
            }
        }

        // chuyển sang trang ds albums
        header('Location: index.php');
    }

    public function actionedit()
    {
        if (isset($_GET['id'])) {
            $album_id = $_GET['id'];
        }

        if (isset($_POST['submit'])) {
            $user = unserialize($_SESSION['_user']);

            $name = $_POST['album_name'];
            $result = $this->albumRepository->editAlbum($album_id, $name);
            if ($result) {
                header("Location:../../routes/album/index.php");
            }
        }
        include('../../views/album/index.php');
    }

    public function delete()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $result = $this->albumRepository->deleteAlbum($id);
            //nếu xóa thành công
            if ($result) {
                header("Location:../../routes/album/index.php");
            }
        }
        include('../../views/album/index.php');
    }
}