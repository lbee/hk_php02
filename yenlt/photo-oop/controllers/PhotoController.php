<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 17-Oct-16
 * Time: 20:24
 */
include_once('../../repositories/PhotoRepository.php');
include_once('../../repositories/AlbumRepository.php');
include_once('../../repositories/ProfileRepository.php');
include('../../helpers/UploadHelpers.php');
include_once('../../models/Album.php');
include_once('../../models/User.php');
include_once('../../models/Photo.php');
class PhotoController
{
    private $photoController;

    public function __construct()
    {
        $this->photoController=new PhotoRepository();
    }
    public function actionIndex()
    {
        if (!isset($_SESSION['_user'])) {
            header("Location: ../../routes/user/login.php");
        } else {
            // nếu k tồn tại album_id -> chuyển sang trang album/index

            // lấy ra album_id
            $album_id = $_GET['album_id'];

            // lấy ra album

            // nếu album k tồn tại -> chuyển sang trang album/index

            // lấy ra dsach ảnh trong album

            $user = unserialize($_SESSION['_user']);

            $profileRepository = new ProfileRepository();

            $profile = $profileRepository->loadProfile($user->getId());

            $photos = $this->photoController->retrievePhotoAlbum($album_id);
            include ('../../views/photo/index.php');
        }
    }

    public function actionUpload(){
       // nếu không có album_id trên đường dẫn
      if (!isset($_GET['album_id']))
          return header("Location: ../album/index.php");

        $album_id = $_GET['album_id'];
        if (isset($_POST['submit'])) {
            $file = $_FILES['photoUpload'];

            // upload file và trả về đường dẫn file sau upload
            $photoURL = UploadHelpers::uploadImage($file, '../../uploads/img/');

            if ($photoURL !== false) { // upload thành công
                // tạo photo
                $photo = new Photo(null, $album_id, $file['name'], $photoURL);

                // lưu vào csdl và cập nhật id tự sinh cho $photo
                $created = $this->photoController->create($photo);
                if (!$created) {
                    $error = 'Upload file không thành công. vui lòng thử lại.';
                }
            }
        }




        return header("Location: index.php?album_id=$album_id");
    }
    public function actionView()
    {
        if(!iset($_GET['id'])){
            return header("Location:../album/index.php");
            $id=$this->photoController->getId();
            if($photo==null){
                return header('Location:../album/index.php');
            }
            include ('../../views/photo/view.php');
        }
    }
}