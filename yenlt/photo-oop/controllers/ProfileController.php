<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 10-Oct-16
 * Time: 20:33
 */
include('../../repositories/ProfileRepository.php');
include('../../models/User.php');

class ProfileController
{
    private $profileRepository;

    public function __construct()
    {
        $this->profileRepository = new ProfileRepository();
    }

    public function actionUpdate()
    {
        // kiểm tra login
        if (!isset($_SESSION['_user'])) {
            header("Location: ../../routes/user/login.php");
        } else {
            $user = unserialize($_SESSION['_user']);

            $profile = $this->profileRepository->loadProfile($user->getId());
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
    //khi cập nhật lại user profile thì phải cấp lại session cho username để lưu trên menu
            $_SESSION['username'] = $username;
             $password = $_POST['password'];
             $fullName = $_POST['fname'];
             $address = $_POST['address'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
        //do dùng datepicker nên phải chuyển format từ datepicker dạng d-m-Y sang dạng Y-m-d để lưu vào CSDL
            $dob = DateTime::createFromFormat("d-m-Y", $_POST['dob'])->format("Y-m-d");
            $file = $_FILES['avatar']['name'];
            $result = $this->profileRepository->editProfile( $user->getId(), $username, $password, $fullName, $dob, $address, $gender, $phone, $file);
    //lưu ảnh avatar vào máy
            $this->profileRepository->saveAvatar($username, $file, $_FILES['avatar']['tmp_name']);
        if ($result) {
                header("Refresh:0");
         } else {
                echo "<script>alert('123');</script>";
            }
        }
        }

        include('../../views/profile/update.php');
    }

}