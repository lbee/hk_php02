<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 07-Oct-16
 * Time: 19:51
 */
include ('../../repositories/UserRepository.php');

class UserController
{
    private $userRepositore;

    public function __construct()
    {
        $this->userRepositore = new UserRepository();
    }

    public function Login()
    {
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user = $this->userRepositore->getUser($username, md5($password));
            if ($user != null) {
                $_SESSION['_user'] = serialize($user);
                header('Location:../profile/update.php');
            } else {
                $error = 'Xin hãy kiểm tra lại tên người dùng và mật khẩu!';
            }
        }

        include('../../views/user/login.php');
    }
    public function actionRegister(){

            if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fname = $_POST['fname'];
            $add = $_POST['address'];
                //format datepicker thành dạng Y-m-d giống với CSDL
            $dob = date_format(DateTime::createFromFormat("d-m-Y", $_POST['dob']), "Y-m-d");
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            $file = $_FILES['avatar']['name'];
           $this->userRepositore->register($username, $password, $fname, $dob, $add, $gender, $phone, $file);
        }
        include('../../views/user/register.php');
    }

}