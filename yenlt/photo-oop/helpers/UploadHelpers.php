<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/17/2016
 * Time: 6:34 PM
 */
class UploadHelpers
{
    /**
     * upload file tới thư mục $target_file đã cho
     * @return trả về đường dẫn tới file đã upload
     *  trả về false nếu upload thất bại
     * @param $file
     * @param $target_dir
     */
    public static function uploadImage($file, $target_dir)
    {
        // người dùng không chọn file để upload
        if (!file_exists($file['tmp_name']) || !is_uploaded_file($file['tmp_name'])) {
            return false;
        }

        // tạo thư mục nếu chưa tồn tại
        if (!file_exists($target_dir))
            mkdir($target_dir, 0777, true);

        // thêm thông tin ngày giờ trước tên file để tránh bị trùng vs file đã tồn tại trên server
        $target_file = $target_dir . date('Ymdhis_'). basename($file['name']);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // kiểm tra file có phải ảnh không
        $check = getimagesize($file['tmp_name']);
        if ($check === false) {
            return false;
        }

        // kiểm tra kích thước file
        if ($file['size'] > 2*1024*1024) { // 2Mb
            die('file to large');
            return false;
        }

        // kiểm tra định dạng file
        $allowedFileTypes = ['jpg', 'png', 'jpeg', 'gif']; // các định dạng file cho phép
        // nếu định dạng file không nằm trong mảng các định dạng cho phép
        if (!in_array($imageFileType, $allowedFileTypes)) {
            return false;
        }

        // nếu chạy đến đây = mọi thứ đều ok -> lưu file
        if (move_uploaded_file($file['tmp_name'], $target_file)) {
            // trả về đường dẫn sau upload
            return $target_file;
        } else {
            // có lỗi khi lưu file
            return false;
        }
    }
}