<?php

/**
 * Created by PhpStorm.
 * User: nhoks
 * Date: 06-Oct-16
 * Time: 11:00
 */
class userprofile
{
    private $id;
    private $username;
    private $avatar;
    private  $fullname;
    private $DoB;
    private $Gender;
    private $Adress;
    private $Phone;

    public function __construct($id,$avatar,$fulname,$dob,$gender,$add,$phone)
    {
        $this->id=$id;

        $this->avatar=$avatar;
        $this->fullname=$fulname;
        $this->DoB=$dob;
        $this->Gender=$gender;
        $this->Adress=$add;
        $this->Phone=$phone;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->Adress;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @return mixed
     */
    public function getDoB()
    {
        return $this->DoB;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->Gender;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $Adress
     */
    public function setAdress($Adress)
    {
        $this->Adress = $Adress;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @param mixed $DoB
     */
    public function setDoB($DoB)
    {
        $this->DoB = $DoB;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @param mixed $Gender
     */
    public function setGender($Gender)
    {
        $this->Gender = $Gender;
    }

    /**
     * @param mixed $Phone
     */
    public function setPhone($Phone)
    {
        $this->Phone = $Phone;
    }
}