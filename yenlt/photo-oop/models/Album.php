<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 13-Oct-16
 * Time: 21:54
 */
class Album
{
    private $id;
    private $name;
    private $user_id;

    public function __construct($id,$name,$user_id)
    {
        $this->id=$id;
        $this->name=$name;
        $this->user_id=$user_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
}