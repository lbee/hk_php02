<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 07-Oct-16
 * Time: 18:50
 */
class photo
{
    private $id;
    private $AlbumId;
    private $photoName;
    private $PhotoURL;

    /**
     * photo constructor.
     * @param $id
     * @param $AlbumId
     * @param $username
     * @param $PhotoURL
     */
    public function __construct($id, $AlbumId, $photoName, $PhotoURL)
    {
        $this->id = $id;
        $this->AlbumId = $AlbumId;
        $this->photoName = $photoName;
        $this->PhotoURL = $PhotoURL;
    }


    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->AlbumId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPhotoURL()
    {
        return $this->PhotoURL;
    }

    /**
     * @return mixed
     */
    public function getPhotoName()
    {
        return $this->photoName;
    }

    /**
     * @param mixed $AlbumId
     */
    public function setAlbumId($AlbumId)
    {
        $this->AlbumId = $AlbumId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $PhotoURL
     */
    public function setPhotoURL($PhotoURL)
    {
        $this->PhotoURL = $PhotoURL;
    }

    /**
     * @param mixed $username
     */
    public function setPhotoName($photoName)
    {
        $this->photoName = $photoName;
    }

}