<?php

/**
 * Created by PhpStorm.
 * User: nhoks
 * Date: 06-Oct-16
 * Time: 10:49
 */
class User
{
    private $id;
    private $username;
    private $password;
    private $lastlogin;

    public function __construct($id,$username,$password,$lastlogin)
    {
        $this->id=$id;
        $this->username=$username;
        $this->password=$password;
        $this->lastlogin=$lastlogin;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
    /**
     * @return mixed
     */
   /**
    * @return mixed
    */public function getLastlogin()
    {
    return $this->Lastlogin;
    }
/**
 * @param mixed $Lastlogin
 */public function setLastlogin($Lastlogin)
    {
    $this->Lastlogin = $Lastlogin;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
}