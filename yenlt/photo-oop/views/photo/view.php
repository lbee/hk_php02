<!DOCTYPE html>
<html>
<head>
    <title> Xem ảnh </title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>
<!-- main menu -->
<?php include "../../views/partial/_header.php" ?>

<div class="container">
    <img src="<?php echo $photo->getPhotoUrl(); ?>" alt="<?php echo $photo->getPhotoName(); ?>" class="img-responsive"/>
</div>

<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../assets/components/semantic/dist/semantic.min.js"></script>
<script src="../../assets/js/dim.js"></script>
</body>
</html>