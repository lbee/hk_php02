<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 10-Oct-16
 * Time: 20:34
 */
include_once ('../../models/UserProfile.php');
class ProfileRepository
{
    private $conn;

    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function __construct()
    {
        $this->connect();
    }
    public function editProfile($user_id, $username, $password, $fullName, $dob, $address, $gender, $phone, $avt)
    {
        $passwordEncrypted = md5($password);
        $sql = "UPDATE `tbl_profile` AS t INNER JOIN `tbl_user` AS u ON t.user_id = u.id SET t.user_id='$user_id',t.FullName='$fullName',t.DOB='$dob',t.Address='$address',t.Gender='$gender',
t.Phone='$phone',t.Avatar='$avt',u.username='$username',u.password='$passwordEncrypted' WHERE user_id=" . $user_id;

        $result = $this->conn->query($sql);
        return $result;
    }

    /**
     * trả về object profile theo $user_id
     * trả về null nếu không tồn tại
     * @param $user_id
     * @return array|null
     */
    public function loadProfile($user_id)
    {
        //var_dump($user_id);die;
        //$user_id = 40;
        $sql = "SELECT * FROM `tbl_profile` AS p INNER JOIN `tbl_user` AS u ON p.user_id=u.id WHERE user_id= '$user_id'";
        //var_dump($this->conn);die;
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        //var_dump($row);die;
            if($row){

                return $this->rowToModel($row);
            }
        return null;
    }
    public function rowToModel($row){
        $id=$row['id'];
        $avatar=$row['Avatar'];
        $fulname=$row['FullName'];
        $dob=$row['DOB'];
        $gender=$row['Gender'];
        $add=$row['Address'];
        $phone=$row['Phone'];
        return new UserProfile($id,$avatar,$fulname,$dob,$gender,$add,$phone);
    }
    public function saveAvatar($username,$file,$fileTmpName){
        $targetDir="../../imgavatar/$username/";
        $targetFile=$targetDir.'/'.basename($file);

        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/X
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;
        if (isset($_POST["submit"])) {
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File is not image')</script>";
                $uploadOk = 0;
            }
        }
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            //di chuyển file từ bên ngoài (chỗ chọn file upload) về thư mục $targetDir
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                header("Refresh:0");
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }

    }


}