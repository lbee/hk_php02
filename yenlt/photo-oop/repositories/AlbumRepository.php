<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 13-Oct-16
 * Time: 10:49
 */
class AlbumRepository
{
    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function __construct()
    {
        $this->connect();
    }
    public function retrieveAlbum($user_id)
    {
        $sql = "SELECT * FROM `tbl_album` WHERE user_id=$user_id ORDER BY id DESC";
        $smt=$this->conn->prepare($sql);
        $smt->execute();
        $rows=$smt->fetchAll();

        $albums = [];
        foreach ($rows as $row) {
            $album = $this->rowToModel($row);
            $albums[] = $album;
        }

        return $albums;
    }

    public function rowToModel($row){
        $id=$row['id'];
        $name=$row['name'];
        $user_id=$row=['user_id'];
        return new Album($id,$name,$user_id);
    }

    /**
     * thêm album vs $name, $user_id
     * trả về true nếu thành công và ngược lại
     * @param $name
     * @param $user_id
     * @return Album|null
     */
    function createAlbum($name, $user_id)
    {
        $sql = "INSERT INTO `tbl_album`(`name`,`user_id`) VALUES('$name',$user_id)";
        $result = $this->conn->query($sql);

        return $result ? true : false;
    }
    function deleteAlbum($id)
    {
        $sql = "DELETE FROM `tbl_album` WHERE id=" . $id;
        $result = $this->conn->query($sql);

        return $result ? true : false;
    }

    function editAlbum($id, $name)
    {
        $sql = "UPDATE `tbl_album` SET `name`='$name' WHERE id=" . $id;
        $result=$this->conn->query($sql);
       return $result? true: false;
    }

    function loadFirstImage($album_id)
    {
        //giới hạn 1 kết quả trả về, sắp xếp theo id giảm dần (tức là ảnh mới nhất)
        $sql = "SELECT * FROM `tbl_photo` WHERE `album_id`=$album_id ORDER BY `id` DESC LIMIT 1";
        $result = $this->conn->query($sql);
        $row = $result->fetch();
        //kiểm tra xem có ảnh hay không bằng cách đếm số lượng object khi trả về, nếu có thì đưa ra đường dẫn và tên ảnh
        if($row) {
            return $row['photoURL'] . '/' . $row['photoName'];
        }
        //nếu album trống (ko có ảnh) thì load ảnh mặc định (GREY.PNG)
        else{
            return "../../img/grey.PNG";
        }
    }

}