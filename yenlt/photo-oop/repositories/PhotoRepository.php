<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 17-Oct-16
 * Time: 20:25
 */
include_once('../../models/Photo.php');
include_once ('../../models/User.php');
include_once ('../../models/UserProfile.php');
include_once ('../../models/Album.php');
require_once ('../../models/Photo.php');


class PhotoRepository
{
    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function __construct()
    {
        $this->connect();
    }

    /**
     * trả về mảng các object Photo trong album có $album_id
     *
     * @param $album_id
     */
    public function retrievePhotoAlbum($album_id) {
        $sql="SELECT * FROM `tbl_photo` WHERE album_id='$album_id'ORDER BY id DESC";
        $result=$this->conn->prepare($sql);
        $result->execute();
        $row = $result->fetchAll();

        $photos=[];
        foreach ($row as $row)
        {
            $photo=$this->rowToModel($row);
            $photos[]=$photo;
        }
        return $photos;
    }

    public function rowToModel($row)
    {

        $album_id = $row['album_id'];
        $photoName = $row['photoName'];
        $photoURL = $row['photoURL'];

        return new Photo($id, $album_id, $photoName, $photoURL);

    }
    public function getById($id) {
        $sql = "SELECT * FROM tbl_photo WHERE id = $id";
        $result = $this->conn->query($sql);
        $row = $result->fetch();

        if ($row)
            return $this->rowToModel($row);

        return null;
    }
    public function getFirstPhoto($album_id) {
        $sql = "SELECT * FROM tbl_photo WHERE album_id = $album_id LIMIT 1";
        $result = $this->conn->query($sql);
        $row = $result->fetch();

        if ($row)
            return $this->rowToModel($row);

        return null;
    }
    public function create($photo) {
        $album_id = $photo->getAlbumId();
        $photoName = $photo->getPhotoName();
        $photoURL = $photo->getPhotoURL();

        $sql = "INSERT INTO tbl_photo (album_id, photoName, photoURL)
                  VALUES ('$album_id', '$photoName', '$photoURL')";

        $result = $this->conn->query($sql);
        var_dump($result);die();

        if ($result) {
            // cập nhật id tự sinh cho $album
            $photo->setId($this->conn->lastInsertId());

            return true;
        }

        return false;
    }
    public function delete($photo){
        $id=$photo->getId();
        $sql="DELETE FROM tbl_photo WHERE id=$id";
        $result=$this->conn->query($sql);
        return $result ? true: false;
    }
}