<?php

/**
 * Created by PhpStorm.
 * User: nhok
 * Date: 07-Oct-16
 * Time: 18:52
 */
include ('../../models/User.php');
class UserRepository
{
    private $conn;

    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function __construct()
    {
        $this->connect();
    }

    public function getUser($username, $password)
    {
        $sql = "SELECT * FROM tbl_user where username='$username' AND password='$password'";
        $result = $this->conn->query($sql);
        $row = $result->fetch();
        if ($row)
            return $this->rowToModel($row);
        return null;

    }

    public function rowToModel($row)
    {
        $id = $row['id'];
        $username = $row['username'];
        $password = $row['password'];
        $lastlogin = $row['lastlogin'];

        return new User($id, $username, $password, $lastlogin);

    }

    public function register($username, $password, $fName, $DOB, $add, $gender, $phone, $avt)
    {
        //mã hóa password
        $passEncrypt = md5($password);
        //Đã có CURRENT_TIMESTAMP trong CSDL nên k cần nhập vào tbl_user
        $sql = "INSERT INTO `tbl_user`(`username`,`password`) VALUES('$username','$passEncrypt')";
        $result = $this->conn->query($sql);
        //nếu thực hiện thành công
        if ($result) {
            //cấp session cho username
            $_SESSION['username'] = $username;
            //cấp session cho user_id, dùng để xử lí tác vụ liên quan đến người dùng
            $last_id = $this->conn->lastInsertId();
            $_SESSION['user_id'] = $last_id;
            //thêm record profile cho người dùng, i.e. nếu đăng kí mới thì INSERT vào bảng profile
            $sqlProfile = "INSERT INTO `tbl_profile`(`user_id`,`FullName`,`DOB`,`Address`,`Gender`,`Phone`,`Avatar`) VALUES('$last_id','$fName','$DOB','$add','$gender','$phone','$avt')";
            $resultProfile = $this->conn->query($sqlProfile);
            if ($resultProfile) {
                header("Location:../../routes/profile/update.php");
            } else {
                echo "<script>alert('Xin hãy kiểm tra lại câu lệnh truy vấn profile')</script>";
            }
        } //trong trường hợp câu lệnh SQL thực thi thất bại
        else {
            echo "<script>alert('Xin hãy kiểm tra lại câu lệnh truy vấn')</script>";

        }
    }
}