<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/7/2016
 * Time: 6:36 PM
 */
class Repository {
    protected $connection;

    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect() {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }




    }

    public function close() {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();

        //định nghĩa đường dẫn cứng cho thư mục photo (ko thay đổi)
        if (!defined('PHOTOPATH')) {
            define('PHOTOPATH', '../../img/photo/');
        }

        //định nghĩa đường dẫn cho thư mục avatar
        if (!defined('AVATARPATH')) {
            define('AVATARPATH', '../../imgavatar/');
        }
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }
}