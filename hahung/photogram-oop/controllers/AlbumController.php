<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/14/2016
 * Time: 7:32 PM
 */
include_once  ('../../repositories/UserRepository.php');
include_once  ('../../repositories/ProfileRepository.php');
include_once ('../../repositories/AlbumRepository.php');

class AlbumController
{
    private $albumRepository;
    private $profileRepository;
    public function __construct(){
        $this->albumRepository = new AlbumRepository();
        $this->profileRepository = new ProfileRepository();
    }

    public function actionAlbum() {

        // kiểm tra đăng nhập
        if (!isset($_SESSION['_user'])) {
            header('Location: ../../routes/user/login.php');
        } else {
            // biến chuỗi đã serialize trở lại thành object
            $user = unserialize($_SESSION['_user']);

            // lấy avatar
            $avatar = $this->profileRepository->getAvatar($user->getId());
            // load album
            // $load = $this->albumRepository->loadFirstImage($user->getId());
            // albumlist
            $albumList = $this->albumRepository->retrieveAlbum($user->getId());
//            foreach ($albumList as $item) {
//                $album_id = $item['id'];
//            }
            //echo  $album_id;die();
            // var_dump($albumList);exit;
            // thêm album
            if (isset($_POST['submit'])) {
                $albumName = $_POST['albumName'];
                $albumCreate = $this->albumRepository->createAlbum($albumName, $user->getId());
                if ($albumCreate) {
                    //Refresh lại trang web
                    header("Refresh:0");
                } else {
                    echo "<script>alert('Kiểm tra lại câu lệnh SQL');</script>";
                }
            }
        }

        // view
        include('../../views/album/album.php');
    }

    public function editAlbum() {
        if (isset($_GET['id'])) {
            $album_id = $_GET['id'];
        }

        if (isset($_POST['submit'])) {
            $user = unserialize($_SESSION['_user']);

            $name = $_POST['album_name'];
            $result = $this->albumRepository->editAlbum($album_id, $name);
            if ($result) {
                header("Location:../../routes/album/album.php");
            }
        }
        include('../../views/album/album.php');
    }

    public function deleteAlbum()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $result = $this->albumRepository->deleteAlbum($id);
            //nếu xóa thành công
            if ($result) {
                header("Location:../../routes/album/album.php");
            }
        }
    }
}