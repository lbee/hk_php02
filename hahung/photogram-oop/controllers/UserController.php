<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:28 PM
 */
include_once  ('../../repositories/UserRepository.php');
include_once  ('../../repositories/ProfileRepository.php');


class UserController {
    private $userRepository;
    private $profileRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->profileRepository = new ProfileRepository();
    }

    public function actionLogin() {
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);

            $user = $this->userRepository->getUser($username, $password_hash);
            if ($user != null) {
                // tạo session
                // biến object trở thành 1 chuỗi
                $_SESSION['_user'] = serialize($user);
                header('Location: ../../routes/user/profile.php');
            } else {
                $error = 'Xin hãy kiểm tra lại tên người dùng và mật khẩu!';
//                echo "<script>alert('Xin hãy kiểm tra lại tên người dùng và mật khẩu')</script>";
            }
        }

        // view
        include('../../views/user/login.php');
    }

    public function actionRegister() {
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            //format datepicker thành dạng Y-m-d giống với CSDL
            $dob = date_format(DateTime::createFromFormat("d-m-Y", $_POST['dob']), "Y-m-d");
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            $avatar = $_FILES['avatar']['name'];

            // tạo bản ghi người dùng
            $thanhCong = $this->profileRepository->insert($username, $password);

            // nếu thành công
            if ($thanhCong) {
                //cấp session cho username
                $_SESSION['username'] = $username;
                // lấy ra user_id = last_id
                $user_id = $this->profileRepository->getLastID();
                $_SESSION['user_id'] = $user_id;
                // tạo bản ghi profile
                $this->profileRepository->insertProfile($user_id, $fullname, $dob, $address, $gender, $phone, $avatar);

                // lưu avatar
                $this->profileRepository->saveAvatar($username, $_FILES['avatar']['name'], $_FILES['avatar']['tmp_name']);
                // chuyển ng dùng sang trang login
                header("Location:../profile.php");
            } else {
                // else
                // báo lỗi
                $error = 'Xin hãy kiểm tra lại câu lệnh truy vấn';
            }
        }
            // view
            include('../../views/user/register.php');
    }

    public function actionProfile() {
        // kiểm tra đăng nhập
        if (!isset($_SESSION['_user'])) {
            header('Location: login.php');
        }

        // biến chuỗi đã serialize trở lại thành object
        $user = unserialize($_SESSION['_user']);

        // Code xử lí PHP để update profile
        if (isset($_POST['submit'])) {
            $user_id = $user->getId();
            $fullname = $_POST['fname'];
            $address = $_POST['address'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            //do dùng datepicker nên phải chuyển format từ datepicker dạng d-m-Y sang dạng Y-m-d để lưu vào CSDL
            $dob = DateTime::createFromFormat("d-m-Y", $_POST['dob'])->format("Y-m-d");
            $file = $_POST['avatar'];

            if ($_FILES['avatar']['name'] != "") {
                $file = $_FILES['avatar']['name'];
            }
            $result = $this->profileRepository->editProfile($user_id, $fullname, $dob, $address, $gender, $phone, $file);

            if ($result) {
                if ($_FILES['avatar']['name'] != "") {
                    //lưu ảnh avatar vào máy
                    $this->saveAvatar($user->getUsername(), $file, $_FILES['avatar']['tmp_name']);
                }

                $message = 'Cập nhật thành công!';
            } else {
                $error = 'Lỗi cập nhật profile. vui lòng thử lại.';
            }
        }

        // view data
        $profile = $this->profileRepository->loadProfile($user->getId());

        // view
        include('../../views/user/profile.php');
    }

    public function saveAvatar($username, $file, $fileTmpName) {
        //mỗi $username có 1 đường dẫn avatar khác nhau, được định nghĩa bới imgavatar/[tên username]/
        $targetDir = AVATARPATH . $username;
        //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới , basename là tên file
        $targetFile = $targetDir . '/' . basename($file);

        //kiểm tra xem có tồn tại đường dẫn $targetDir không, nếu không có sẽ thêm mới, tránh trường hợp vào server để tạo bằng tay
        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/X
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;

        if (isset($_POST["submit"])) {
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File is not image')</script>";
                $uploadOk = 0;
            }
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            //di chuyển file từ bên ngoài (chỗ chọn file upload) về thư mục $targetDir
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                header("Refresh:0");
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }
    }
}