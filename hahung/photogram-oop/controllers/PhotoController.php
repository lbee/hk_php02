<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/17/2016
 * Time: 3:03 PM
 */
include_once  ('../../repositories/UserRepository.php');
include_once  ('../../repositories/ProfileRepository.php');
include_once ('../../repositories/AlbumRepository.php');
include_once ('../../repositories/PhotoRepository.php');

class PhotoController
{
    private $photoRepository;
    private $profileRepository;
    public function __construct(){
        $this->photoRepository = new PhotoRepository();
        $this->profileRepository = new ProfileRepository();
    }

    public function actionPhoto() {
        if (!isset($_SESSION['_user'])) {
            header('Location: ../../routes/user/login.php');
        } else {
            // biến chuỗi đã serialize trở lại thành object
            $user = unserialize($_SESSION['_user']);

            // lấy avatar
            $avatar = $this->profileRepository->getAvatar($user->getId());

            //photo list
            $photoList = $this->photoRepository->retrievePhoto($_GET['album_id']);

            //var_dump($photoList); die();

            // Code xử lí upload photo
             if (isset($_POST['submit'])) {
                 $album_id = $_GET['album_id'];
                // di chuyển photo từ ngoài vào thư mục đích
                $this->movePhoto($album_id, $user->getUsername(), $_FILES['photoUpload']['name'], $_FILES['photoUpload']['tmp_name']);
            }
        }
        // view
        include('../../views/photo/photo.php');
    }

    function movePhoto($album_id, $username, $file, $fileTmpName)
    {
        $targetDir = PHOTOPATH . $username;
        //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới
        $targetFile = $targetDir . '/' . basename($file);

        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/E
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;

        if (isset($_POST["submit"])) {
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File is not image')</script>";
                $uploadOk = 0;
            }
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //lưu thông tin vào cơ sở dữ liệu
                $result = $this->photoRepository->uploadPhoto($album_id, $targetDir, basename($file));
                //trả về TRUE/FALSE
                if ($result) {
                    //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                    header("Refresh:0");
                }

            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }
    }

    // xoa ảnh
    public function deletePhoto() {
        //lấy id của album dựa trên id của URL
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }

        //lấy album_id dựa trên album_id trên URL
        if(isset($_GET['album_id'])){
            $album_id = $_GET['album_id'];
        }

        $result = $this->photoRepository->deletePhoto($id);
        if ($result) {
            header("Location:photo.php?album_id=$album_id");
        }
    }

    // xem ảnh
    public function viewPhoto() {
        // biến chuỗi đã serialize trở lại thành object
        $user = unserialize($_SESSION['_user']);
        $username = $user->getUsername();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        $viewphoto = $this->photoRepository->viewPhoto($id);
        //var_dump($viewphoto);die();
        // view
        include('../../views/photo/view.php');
    }
}