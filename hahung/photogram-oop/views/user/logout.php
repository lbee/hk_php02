<?php
/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/17/2016
 * Time: 9:15 PM
 */
//do vẫn còn session của username và user_id nên vẫn phải có session_start()
session_start();

session_unset();
$session_destroyed = session_destroy();
//nếu destroy session thành công
if($session_destroyed) {
    header("Location:../../routes/user/login.php");
}