
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>
<!-- Thêm phần navigation vào -->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../../routes/album/album.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <!--  sổ menu-->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="../../imgavatar/<?php echo $user->getUsername().'/'.$avatar['Avatar']; ?>" alt="avatar" width="24" height="24"/><?php echo $user->getUsername(); ?><span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../../routes/user/profile.php">Profile</a></li>
                        <li><a href="../../views/user/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- ========================= Hết phần navigation -->
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách album</h3>
        </div>
        <div class="panel-body">
            <div class="content addNew">
                <div class="ui input">
                    <button class="ui primary button">
                        Thêm mới
                    </button>

                    <form id="addNewForm" method="POST" style="display: none">
                        <input placeholder="Enter album name" type="text" name="albumName">
                        <button type="submit" class="ui primary button" name="submit">
                            Save
                        </button>
                    </form>
                </div>
            </div>
            <br/>
            <!-- dẩy UI card từ semantic UI về thành 3 cột-->
            <div class="ui three column grid">
                <!-- Album đầu tiên(dùng để add)-->
                <?php foreach ($albumList as $value) :  ?>
                    <!-- Album list-->
                    <div class="column">
                        <div class="ui fluid card">
                            <div class="image">
                                <!-- load ảnh đầu tiên trong album -->
                                <img src="<?php echo $this->albumRepository->loadFirstImage($value['id']); ?>" alt=""/>
                            </div>
                            <div class="content">
                                <a class="header"
                                   href="../photo/photo.php?album_id=<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a>
                            </div>
                            <div class="extra content">
                                <a href="#" data-toggle="modal" data-target="#exampleModal<?php echo $value['id']; ?>">
                                    <i class="edit icon"></i>
                                    Sửa
                                </a>
                                <a href="../../routes/album/delete.php?id=<?php echo $value['id']; ?>">
                                    <i class="delete icon"></i>
                                    Xóa
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- modal box dùng cho edit, mỗi album có 1 modal box riêng, xác định thông qua #data-target(dòng 72) và #exampleModal (dòng 84)-->
                    <div id="exampleModal<?php echo $value['id']; ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Chỉnh sửa tên album</h4>
                                </div>
                                <form method="post" action="edit.php?id=<?php echo $value['id']; ?>">
                                    <div class="modal-body">
                                        <input name="album_name" class="form-control" type="text"
                                               placeholder="Tên album"
                                               value="<?php echo $value['name']; ?>"/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Save
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
            <!-- -->
        </div>
    </div>
    <!-- End -->
    <script src="../../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/components/semantic/dist/semantic.min.js"></script>
    <script src="../../assets/js/addNew.js"></script>


</body>
</html>