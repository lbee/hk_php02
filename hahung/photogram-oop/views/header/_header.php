<?php
/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/14/2016
 * Time: 7:51 PM
 */

//include_once  ('../../repositories/UserRepository.php');
//include_once  ('../../repositories/ProfileRepository.php');
include_once ('../../controllers/UserController.php');

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
}

//lấy user_id
if (isset($_SESSION['user_id'])) {
    $user_id = $_SESSION['user_id'];
}
//load ảnh avatar của người dùng lên thanh navigation bar
$result = $this->profileRepository->loadProfile($user->getId());

//trong trường hợp ko có session của username,
// sẽ trỏ lại đến trang login, không cho ấn nút "Back" trên trình duyệt, i.e. (Không ấn back lại được đâu)
//vì các trang trong views/ đều đi qua _header.php này nên đặt ở đây
if(!isset($_SESSION['username'])){
    header("Location:../../routes/user/login.php");
}
?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../album/index.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <!--  sổ menu-->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="<?php echo AVATARPATH.$username.'/'.$result['Avatar']?>" alt="avatar" width="24" height="24"/><?php echo $username; ?><span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../userprofile/index.php">Profile</a></li>
                        <li><a href="../../views/user/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>