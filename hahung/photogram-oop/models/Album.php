<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/7/2016
 * Time: 7:26 PM
 */
class Album
{
    private $id;
    private $name;
    private $user_id;

    /**
     * Album constructor.
     * @param $id
     * @param $name
     * @param $user_id
     */
    public function __construct($id, $name, $user_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->user_id = $user_id;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
}