/**
 * Created by camnh on 7/21/2016.
 */
$('.special.cards .image,.fluid.cards .image').dimmer({
    on: 'hover'
});

//kiểm tra xem input type="file" có file hay không
//nếu có file thì hiện nút Save

$("#fileUpload").change(function () {
    if ($(this).val() != "") {
        $(".hide.submit").removeClass('hide');
        $(".hide.cancel").removeClass('hide');
    }
});

//nếu chọn cancel thì hủy giá trị file đi
$(".cancel").click(function(){
    $("#fileUpload").val("");
});