<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 12/10/2016
 * Time: 20:36 CH
 */
include_once ('../../models/UserProfile.php');
class ProfileRepository
{
    private $connection;

    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect() {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function close() {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }

    /**
     * tạo ra profile người dùng vs những thông tin đã cho
     * trả về true nếu thành công và ngược lại
     * @param $user_id
     * @param $fullName
     * @param $dob
     * @param $address
     * @param $gender
     * @param $phone
     * @param $avatar
     */
    public function create($user_id, $fullName, $dob, $address, $gender, $phone, $avatar)
    {
        $sql = "INSERT INTO tbl_profile (user_id, FullName, DOB, Address, Gender, Phone, Avatar) 
                VALUES ('$user_id', '$fullName', '$dob', '$address', '$gender', '$phone', '$avatar')";
        $result = $this->connection->query($sql);

        if ($result)
            return true;
        else
            return false;
        // execute

    }
    public function rowToModel($row) {
        $user_id = $row['user_id'];
        $fullname = $row['FullName'];
        $dob = $row['DOB'];
        $address = $row['Address'];
        $gender = $row['Gender'];
        $phone = $row['Phone'];
        $avatar = $row['Avatar'];
        return new UserProfile($user_id, $fullname, $dob, $address, $gender, $phone, $avatar);
    }
    // lấy thông tin profile
    public function getProfile($user_id)
    {
        $sql = "SELECT * FROM tbl_profile WHERE user_id = '$user_id'";
        $result = $this->connection->query($sql)->fetch();


        if ($result)
            return $this->rowToModel($result);
        else
            return false;
        // execute

    }
    // sửa thông tin profile
    public function editProfile($user_id, $fullname, $dob, $address, $gender, $phone, $avatar)
    {
        $sql = "UPDATE tbl_profile SET avatar ='$avatar',fullname = '$fullname', DoB ='$dob', Gender=$gender, address='$address', phone ='$phone' WHERE user_id='$user_id'";
        $result = $this->connection->query($sql);


        if ($result)
            return $result;
        else
            return false;
        // execute

    }


// xử lý ava
    public function saveAvatar($username, $avatar, $fileTmpName)
    {
        //mỗi $username có 1 đường dẫn avatar khác nhau, được định nghĩa bới imgavatar/[tên username]/
        $targetDir = '../user/imgavatar/'.$username;
        //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới , basename là tên file
        $targetFile = $targetDir . '/' . basename($avatar);

        //kiểm tra xem có tồn tại đường dẫn $targetDir không, nếu không có sẽ thêm mới, tránh trường hợp vào server để tạo bằng tay
        if (!file_exists($targetDir)) {
            //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/X
            mkdir($targetDir, 0777, true);
        }
        $uploadOk = 1;

        if (isset($_POST["submit"])) {
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File không phải ảnh')</script>";
                $uploadOk = 0;
            }
        }
        // Check if $upload Ok is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, file chưa upload.";
            // if everything is ok, try to upload file
        } else {
            //di chuyển file từ bên ngoài (chỗ chọn file upload) về thư mục $targetDir
            if (move_uploaded_file($fileTmpName, $targetFile)) {
                //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                header("Refresh:0");
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            }
        }
    }
}