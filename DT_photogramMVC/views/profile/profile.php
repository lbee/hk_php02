<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
</head>
<body>
<div class="container">
    <h1>Hồ sơ người dùng</h1>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Họ tên</th>
            <th>Ngày sinh</th>
            <th>Avatar</th>
            <th>Giới tính</th>
            <th>Địa chỉ</th>
            <th>Điện thoại</th>
            <th>Album</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $profile->getFullname(); ?></td>
            <td><?php echo $profile->getDob(); ?></td>
            <td><img src='../user/imgavatar/<?php echo $user->getUsername(); ?>/<?php echo $profile->getAvatar(); ?>' width=40px /></td>
            <td><?php if ($profile->getGender()==1) {
                    echo "Nam";
                }else{echo "Nu";}
                ?></td>
            <td><?php echo $profile->getAddress(); ?></td>
            <td><?php echo $profile->getPhone(); ?></td>
            <td><a href="album.php">album</a></td>
            <td><a href="editProfile.php">Sửa</a></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>