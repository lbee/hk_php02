<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sửa thông tin cá nhân</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">

    <h1 class="text-center">Sửa thông tin cá nhân</h1>

    <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">

        <div class="form-group">
            <label class="control-label col-sm-2" for="ava">Avata:</label>
            <div class="col-sm-10">
                <img style="width: 100px" src="../user/imgavatar/<?php echo $user->getUsername(); ?>/<?php echo $profile->getAvatar(); ?>" alt="avatar" />
                <input type="file" name="avatar" multiple="" />
                <input type="hidden" name="avatar" value="<?php echo  $profile->setAvatar(); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="fullname">Họ và tên:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="fullname" name="fullname" value="<?php echo $profile->getFullname(); ?>" placeholder="Enter fullName">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="dob">Ngày sinh:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="dob" name="dob" value="<?php echo $profile->getDob(); ?>" placeholder="Enter yyyy/mm/dd">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="gender">Giới tính:</label>
            <div class="col-sm-10">
                <label class="radio-inline"><input type="radio" name="gender" value="1" <?php echo $profile->getGender()==1 ? 'checked': ''; ?> />Nam</label>
                <label class="radio-inline"><input type="radio" name="gender" value="0" <?php echo $profile->getGender()==0 ? 'checked': ''; ?> />Nữ</label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="address">Địa chỉ:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="address" name="address" value="<?php echo $profile->getAddress(); ?>" placeholder="Enter address">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="phone">Số điện thoại:</label>
            <div class="col-sm-10">
                <input type="numbers" class="form-control" id="phone" name="phone" value="<?php echo $profile->getPhone(); ?>" placeholder="Enter Phonenumbers">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-default">Submit</button>
                <button type="submit" name="submit" class="btn btn-default"><a href="profile.php">Xem thông tin </a></button>
            </div>
        </div>
    </form>
</div>
</body>
</html>