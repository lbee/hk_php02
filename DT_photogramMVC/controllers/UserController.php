<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:28 PM
 */

include('../../repositories/UserRepository.php');
include('../../repositories/ProfileRepository.php');

class UserController
{
    private $userRepository;
    private $profileRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->profileRepository = new ProfileRepository();
    }

    /**
     * phương thức xử lý hành động login
     */
    public function actionLogin()
    {
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);

            $user = $this->userRepository->getUser($username, $password_hash);
            if ($user != null) {
                // tạo session
                $_SESSION['_user'] = serialize($user);
                header('Location: profile.php');
            } else {
                $error = 'Xin hãy kiểm tra lại tên người dùng và mật khẩu!';
            }
        }

        // view
        include('../../views/user/login.php');
    }

    /**
     * phương thức xử lý hành động đăng ký
     */
    public function actionRegister()
    {
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);
            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $dob = $_POST['dob'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];

            if (isset($_FILES['avatar']['name'])) {
                $avatar = $_FILES['avatar']['name'];
            } else {
                $avatar = $_POST['avatar'];
            }

            // tạo người dùng vs username & password
            $thanhCong = $this->userRepository->create($username, $password_hash);

            // nếu thành công
            if ($thanhCong) {
                // lấy ra id của ng dùng vừa thêm vào
                $user_id = $this->userRepository->getLastId();
                // tạo profile
                $this->profileRepository->create($user_id, $fullname, $dob, $address, $gender, $phone, $avatar);
                // lưu ảnh avatar

                $this->profileRepository->saveAvatar($username, $avatar, $_FILES['avatar']['tmp_name']);
                // Chuyển đến login sau khi đăng kí

                header('Location: ../../routes/user/login.php');
            } else { // nếu không thành công
                // báo lỗi

            }


        }
        include('../../views/user/register.php');
    }
}