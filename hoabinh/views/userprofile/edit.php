<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chỉnh sửa thông tin cá nhân</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
</head>
<body>
<?php include '../../views/layout/header.php';?>
<!-- Chỉnh sửa profile -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Chỉnh sửa profile</h3>
        </div>
        <div class="panel-body">
            <!-- Form cập nhật-->

            <form method="post" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="fullname">FullName</label>
                    <input type="text" name="fname" class="form-control" id="fullname" placeholder="FullName"
                           value="<?php echo $userProfile->getFullName(); ?>">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea class="form-control" name='address' id="address"
                              placeholder="Địa chỉ"><?php echo $userProfile->getAddress(); ?></textarea>
                </div>

                <!-- Giới tính -->
                <div class="form-group">
                    <label for="gender">Gender</label>
                    <div class="radio">
                        <?php if ($userProfile->getGender() == 1) {?>
                            <label>
                                <input type="radio" name="gender" id="gender" value="1" checked>
                                Nam
                            </label>
                            <label>
                                <input type="radio" name="gender" id="gender" value="0">
                                Nữ
                            </label>
                        <?php } else {?>
                            <label>
                                <input type="radio" name="gender" id="gender" value="1">
                                Nam
                            </label>
                            <label>
                                <input type="radio" name="gender" id="gender" value="0" checked>
                                Nữ
                            </label>
                        <?php }?>
                    </div>
                </div>

                <!-- Phone -->
                <div class="form-group">
                    <label for="phoneNum">Phone number</label>
                    <input class="form-control" name="phone" id="phoneNum" placeholder="Phone number"
                           value="<?php echo $userProfile->getPhone(); ?>"/>
                </div>

                <!-- DOB -->
                <div class="form-group">
                    <label for="dob">DOB</label>
                    <input type="date" class="form-control" id="dob" name='dob' placeholder="DOB"
                    value="<?php echo $userProfile->getDob(); ?>"/>
                </div>

                <!-- Avatar -->
                <p><label for="avatar">Avatar</label></p>
                <img src="<?php echo AVATARPATH . $user->getUsername() . '/' . $userProfile->getAvatar(); ?>" alt="" width="200" height="auto">
                <input type="file" id="avatar" name="avatar" accept="image/*"/>
                <br/>
                <!-- Gửi form -->
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>

            </form>
            <!-- Hết form -->

        </div>
    </div>
</div>