<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>
<?php include '../../views/layout/header.php';?>
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách album</h3>
        </div>
        <div class="panel-body">
            <div class="content addNew">
                <div class="ui input">
                    <button class="ui primary button">
                        Thêm mới
                    </button>

                    <form id="addNewForm" method="POST" style="display: none">
                        <input placeholder="Enter album name" type="text" name="albumName">
                        <button type="submit" class="ui primary button" name="saveAlbum">
                            Save
                        </button>
                    </form>
                </div>
            </div>
            <br/>
            <!-- dẩy UI card từ semantic UI về thành 3 cột-->
            <div class="ui three column grid">
                <!-- Album đầu tiên(dùng để add)-->
                <?php
///var_dump($listAlbum);
foreach ($listAlbum as $row): ?>
                    <!-- Album list-->

                    <?php //var_dump($listAlbum);exit;?>
                    <div class="column">
                        <div class="ui fluid card">
                            <a class="image" href="../photo/index.php?album_id=<?php echo $row['id'] ?>">
                                <!-- load ảnh đầu tiên trong album -->
                                <img src="<?php echo $object->getPhotoURL() . '/' . $object->getPhotoName(); ?>" alt=""/>
                            </a>
                            <div class="content">
                                <a class="header"
                                   href="../photo/index.php?album_id=<?php echo $row['id'] ?>"><?php echo $row['name']; ?></a>
                            </div>
                            <div class="extra content">
                                <a href="#" data-toggle="modal" data-target="#exampleModal<?php echo $row['id']; ?>">
                                    <i class="edit icon"></i>
                                    Sửa
                                </a>
                                <a href="delete.php?id=<?php echo $row['id']; ?>">
                                    <i class="delete icon"></i>
                                    Xóa
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- modal box dùng cho edit, mỗi album có 1 modal box riêng, xác định thông qua #data-target(dòng 72) và #exampleModal (dòng 84)-->
                    <div id="exampleModal<?php echo $row['id']; ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Chỉnh sửa tên album</h4>
                                </div>
                                <form method="post" action="edit.php?id=<?php echo $row['id']; ?>">
                                    <div class="modal-body">
                                        <input name="album_name" class="form-control" type="text"
                                               placeholder="Tên album"
                                               value="<?php echo $row['name'] ?>"/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Save
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>

            </div>
            <!-- -->
        </div>
    <!-- End -->
    <script src="../../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/components/semantic/dist/semantic.min.js"></script>
    <script src="../../assets/js/addNew.js"></script>
</body>
</html>