<?php
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>
<div>
<?php include '../../views/layout/header.php';?>
</div>
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách photo thuộc album</h3>
        </div>
        <div class="panel-body">

            <div class="ui">
                <!-- Form để upload ảnh -->
                <form method="post" enctype="multipart/form-data">
                    Upload image:
                    <!-- Chấp nhận mọi thể loại ảnh sử dụng accept attr-->
                    <input type="file" class="ui primary button" accept="image/*" id="fileUpload" name="photoUpload"/>
                    <!-- Nút submit, bị ẩn nếu như file chưa được chọn -->
                    <button name="submit" class="ui secondary button hide submit" type="updateImg">
                        Save
                    </button>
                    <button class="ui button hide cancel" type="submit">
                        Cancel
                    </button>
                </form>
            </div>
            <br/>

            <!-- lấy danh sách ảnh để ở đây-->
            <div class="ui special cards three column">
            <?php foreach ($albumPhoto as $photo): ?>
                <div class="card">
                    <div class="blurring dimmable image">
                        <div class="ui inverted dimmer">
                            <div class="content">
                                <div class="center">
                                    <a href="view.php?album_id=<?php echo $photo['album_id']; ?>&id=<?php echo $photo['id']; ?>">
                                        <div class="ui button">Xem</div>
                                    </a>
                                    <!-- Truyền 2 tham số (1): Photo id để xóa photo (2): album_id để quay về album-->
                                    <a href="?act=del&album_id=<?php echo $album_id; ?>&id=<?php echo $photo['id']; ?>">
                                        <div class="negative ui button">Xóa</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <img src="<?php echo $photo['photoURL'] . '/' . $photo['photoName']; ?>" alt="">
                    </div>
                </div>
            <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../assets/components/semantic/dist/semantic.min.js"></script>
<script src="../../assets/js/dim.js"></script>
</body>
</html>