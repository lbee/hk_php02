<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/11/2016
 * Time: 10:26 AM
 */
class UserProfile
{
    private $user_id;
    private $full_name;
    private $dob;
    private $address;
    private $gender;
    private $phone;
    private $avatar;

    public function __construct($user_id, $full_name, $dob, $address, $gender, $phone, $avatar)
    {
        $this->user_id = $user_id;
        $this->full_name = $full_name;
        $this->dob = $dob;
        $this->address = $address;
        $this->gender = $gender;
        $this->phone = $phone;
        $this->avatar = $avatar;
    }

    public function getUserId() {
        return $this->user_id;
    }
    public function getFullName() {
        return $this->full_name;
    }
    public function getDob() {
        return $this->dob;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getGender() {
        return $this->gender;
    }
    public function getPhone() {
        return $this->phone;
    }
    public function getAvatar() {
        return $this->avatar;
    }

    public function setFullName($fullName){
        $this->full_name = $fullName;
    }
    public function setDob($dob){
        $this->dob = $dob;
    }
    public function setAddress($address){
        $this->address = $address;
    }
    public function setGender($gender){
        $this->gender = $gender;
    }
    public function setPhone($phone){
        $this->phone = $phone;
    }
    public function setAvatar($avatar){
        $this->avatar = $avatar;
    }


}