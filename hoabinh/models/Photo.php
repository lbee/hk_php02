<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 2:44 PM
 */
class Photo
{
    private $id;
    private $album_id;
    private $photoName;
    private $photoURL;

    public function __construct($id, $album_id, $photoName, $photoURL)
    {
        $this->id = $id;
        $this->album_id = $album_id;
        $this->photoName = $photoName;
        $this->photoURL = $photoURL;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @return mixed
     */
    public function getPhotoName()
    {
        return $this->photoName;
    }

    public function getPhotoURL()
    {
        return $this->photoURL;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setPhotoName($photoName)
    {
        $this->photoName = $photoName;
    }

    public function setPhotoURL($photoURL)
    {
        $this->photoURL = $photoURL;
    }
}
