<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 2:45 PM
 */
include_once 'Repository.php';
include_once '../../models/Album.php';
include_once '../../models/Photo.php';

class PhotoRepository extends Repository
{
    /**
     * KHỞI TẠO DATABASE
     */
    public function __construct()
    {
        //KẾ THỪA LỚP DATABASE
        parent::connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        parent::close();
    }

    /**
     * LẤY ALBUM ẢNH
     * @param $userId
     */

    public function getAlbum($user_id)
    {
        $sql = "SELECT * FROM tbl_album  WHERE  user_id='$user_id'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        return $this->rowToModelAlbum($row);
    }

    /**
     * chuyển đổi từ mảng sang object bảng user
     * @param $row
     */
    public function rowToModelAlbum($row)
    {
        $id = $row['id'];
        $name = $row['name'];
        $user_id = $row['user_id'];

        return new Album($id, $name, $user_id);
    }

    /**
     * Lấy danh sach album thuộc người dùng $user_id
     * @param $conn
     * @param $user_id
     * @return mixed
     */
    public function retrieveAlbum($user_id)
    {
        $sql = "SELECT * FROM `tbl_album` WHERE user_id=$user_id ORDER BY id DESC";
        $obj = $this->connection->query($sql);
        $rows = $obj->fetchAll();
        return $rows;
    }

    /**
     * Thêm mới album
     * @param $conn : MYSQL connection
     * @param $name : Album name
     */
    public function createAlbum($name, $user_id)
    {
        $sql = "INSERT INTO `tbl_album`(`name`,`user_id`) VALUES('$name','$user_id')";
        $result = $this->connection->query($sql);
        return $result;
    }

    /**
     * Load ảnh đầu tiên để hiển thị lên album
     * @param $conn : MYsqli connection
     * @param $album_id : ID album
     */
    public function getFirstImage($album_id)
    {
        //giới hạn 1 kết quả trả về, sắp xếp theo id giảm dần (tức là ảnh mới nhất)
        $sql = "SELECT * FROM `tbl_photo` WHERE `album_id`=$album_id ORDER BY `id` DESC LIMIT 1";
        $obj = $this->connection->query($sql);
        $rows = $obj->fetch();
        return $this->rowToModelPhoto($rows);
        // return $rows;

    }
    /*Lay danh sach photo thuoc album_id*/
    public function retrievePhoto($album_id)
    {
        $sql = "SELECT * FROM `tbl_photo` WHERE album_id=$album_id ORDER BY id DESC";
        $object = $this->connection->query($sql);
        $photos = $object->fetchAll();
        return $photos;
    }
    /**
     * chuyển đổi từ mảng sang object bảng user
     * @param $row
     */
    public function rowToModelPhoto($rows)
    {
        $id = $rows['id'];
        $album_id = $rows['album_id'];
        $photoName = $rows['photoName'];
        $photoURL = $rows['photoURL'];
        return new Photo($id, $album_id, $photoName, $photoURL);
    }
    /*Upload photo*/
    public function uploadPhoto($album_id, $path, $photoName)
    {
        $sql = "INSERT INTO `tbl_photo`(`album_id`,`photoName`,`photoURL`) VALUES ($album_id,'$photoName','$path')";
        $result = $this->connection->query($sql);
        return $result;
    }
    /**
     * xoa ảnh
     * @param $row
     */

    public function delete($id)
    {
        $sql = "DELETE FROM `tbl_photo` WHERE id=" . $id;
        //echo $sql;exit;
        $result = $this->connection->query($sql);
        return $result;
    }
}
