<?php

/**
 * Created by PhpStorm.
 * User: MrHbat
 * Date: 10/12/2016
 * Time: 2:38 PM
 */
include_once '../../repositories/PhotoRepository.php';
include_once '../../repositories/UserRepository.php';

class PhotoController
{
    private $userRepository;
    private $photoRepository;

    public function __construct()
    {
        $this->photoRepository = new PhotoRepository();
        $this->userRepository = new UserRepository();
    }

    /**
     * HIỂN THỊ PROFILE
     */
    public function viewAlbum()
    {
        $user_id = $_SESSION['userId'];
        $user = $this->userRepository->getUser($user_id);
        $userProfile = $this->userRepository->getProfile($user_id);
        $userAlbum = $this->photoRepository->getAlbum($user_id);
        // var_dump($userAlbum);die();
        $object = $this->photoRepository->getFirstImage($userAlbum->getId());
        //var_dump($object);die();

        //DS ALBUM
        $listAlbum = $this->photoRepository->retrieveAlbum($user_id);
        //VIEW
        include_once '../../views/album/index.php';
    }

    /**
     * ADD ALBUM
     */
    public function addAllbum()
    {
        $user_id = $_SESSION['userId'];
        if (isset($_POST['saveAlbum'])) {
            $albumName = $_POST['albumName'];
            $albumCreate = $this->photoRepository->createAlbum($albumName, $user_id);
            if ($albumCreate) {
                //var_dump($albumCreate);
                //Refresh lại trang web
                header("Refresh:0");
            } else {
                echo "<script>alert('Kiểm tra lại câu lệnh SQL');</script>";
            }
        }
        include_once '../../views/album/index.php';
    }

    /*Index photo*/

    public function indexPhoto()
    {
        $user_id = $_SESSION['userId'];
        $user = $this->userRepository->getUser($user_id);
        $userProfile = $this->userRepository->getProfile($user_id);
        $userAlbum = $this->photoRepository->getAlbum($user_id);
        //var_dump($userAlbum);
        $album_id = $userAlbum->getId();
        $albumPhoto = $this->photoRepository->retrievePhoto($album_id);
        //var_dump($albumPhoto);
        include_once '../../views/photo/index.php';
    }
    //$album_id, $username, $file, $fileTmpName
    /*Add photo vao db*/
    public function addPhoto()
    {
        $user_id = $_SESSION['userId'];
        $user = $this->userRepository->getUser($user_id);
        $userName = $user->getUsername();
        $userAlbum = $this->photoRepository->getAlbum($user_id);
        //var_dump($userAlbum);exit;
        $album_id = $userAlbum->getId();

        if (isset($_POST["submit"])) {
            $targetDir = PHOTOPATH . $userName;
            $file = $_FILES['photoUpload']['name'];
            $fileTmpName = $_FILES['photoUpload']['tmp_name'];
            //kiểm tra xem có tồn tại đường dẫn $targetFile hay không, nếu ko có thì thêm mới
            $targetFile = $targetDir . '/' . basename($file);

            if (!file_exists($targetDir)) {
                //phải có parameter thứ 3 = true để tạo thư mục mới bên trong thư mục khác, 0777: có quyền cao nhất R/W/E
                mkdir($targetDir, 0777, true);
            }
            $uploadOk = 1;
            $check = getimagesize($fileTmpName);
            if ($check === false) {
                echo "<script>alert('File is not image')</script>";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($fileTmpName, $targetFile)) {
                    //lưu thông tin vào cơ sở dữ liệu
                    $result = $this->photoRepository->uploadPhoto($album_id, $targetDir, basename($file));
                    //trả về TRUE/FALSE
                    if ($result) {
                        //refresh trang web, cần có ob_start() ở đầu file, nếu không bị báo lỗi "cannot modify header information,header already sent by.."
                        header("Refresh:0");
                    }

                } else {
                    echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
                }
            }
        }
        include_once '../../views/photo/index.php';
    }

    /*Xóa photo*/
    public function deletePhoto()
    {
        if (isset($_GET['act']) == 'del') {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
            }

            //lấy album_id dựa trên album_id trên URL
            if (isset($_GET['album_id'])) {
                $album_id = $_GET['album_id'];
            }

            //bắt đầu truy vấn
            $result1 = $this->photoRepository->delete($id);
            if ($result1) {
                header("Location:index.php?album_id=$album_id");
            }
        }
        include_once '../../views/photo/index.php';
    }
}
