<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 7:41 PM
 */
include ('../../Repositories/UserRepositories.php');
include ('../../Repositories/UserProfileRepositories.php');
class UserController
{
    private $userlogin;
    public function __construct()
    {
        $this->userlogin = new UserRepositories();
    }

    /**
     * Action log in
     */
    public function loginAction(){


        if(isset($_COOKIE['user'])){
            setcookie('error_login',null, -1);
            $_SESSION['user'] = $_COOKIE['user'];
            header('location: profile.php');
        }

        if(isset($_POST['submit'])){
            $username = $_POST['username'];
            $pass = md5($_POST['password']);
            $user=$this->userlogin->login($username,$pass);
            if($user!=null){
                $_SESSION['user'] = $user;
                if(isset($_POST['remember'])){
                    setcookie('user', $user, time()+3000);
                }
                header('location: profile.php');
            }else{
                echo '<div class="error_login">Tài khoản hoặc mật khẩu không đúng</div>';
            }
        }
        include ('../../View/login.php');
    }

    /**
     * Action show profile
     */
    public function profileAction(){
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            $user_profile = new UserpProfileRepositories();
            $profile = $user_profile->retrieveUser($user->getId());
            include ('../../View/profile.php');
        }else{
            header('location: login.php');
        }
    }

    /**
     * Action đăng xuất
     */
    public function logoutAction(){
        session_destroy();
        setcookie('user',null,-1);
        header('location: login.php');
    }

    /**
     * Action sửa profile
     */
    public function editProfileAction(){
        $user=$_SESSION['user'];
        $user_profile = new UserpProfileRepositories();
        $profile = $user_profile->retrieveUser($user->getId());

        if(isset($_POST['submit'])){
            $phone = $_POST['phone'];
            $DoB = $_POST['DoB'];
            $fullname = $_POST['fullname'];
            $email = $_POST['email'];
            $gender = $_POST['gender'];
            $address = $_POST['address'];
            if($_FILES['avatar']['name']!= "") {
                $avatar = $_FILES['avatar']['name'];
                $tempt = $_FILES['avatar']['tmp_name'];
            }
            else{
                $avatar = $_POST['avatar'];
            }

            $user_profile->updateUser($user->getId(), $fullname, $email, $DoB, $address, $phone, $avatar, $gender);
                if(isset($tempt)){
                    move_uploaded_file($tempt, '../../img/'.$avatar);
                }
             header("location: profile.php");

        }
        include ('../../View/edit_profile.php');
    }

    /**
     * Action đăng ký
     */
    public function registerAction(){
        if(isset($_POST['submit'])){
            $username = $_POST['username'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $pass = md5($_POST['pass']);
            $DoB = $_POST['DoB'];
            $fullname = $_POST['fullname'];
            $gender = $_POST['gender'];
            $address = $_POST['address'];
            if($_FILES['avatar']['name']!='') {
                $avatar = $_FILES['avatar']['name'];
                $tempt = $_FILES['avatar']['tmp_name'];
            }
            else{
                $avatar = "default-avatar.jpg";
            }

            $register_user= new UserRepositories();
            //kiểm tra tên người dùng đã tồn tại chưa
            $check = $register_user->checkUser($username);
            if($check==null) {
                $register_user->createUser($username, $pass);

                $register_profile = new UserpProfileRepositories();
                $register_result=$register_profile->createUserProfile($email, $fullname, $DoB, $address, $phone, $avatar, $gender);

                if ($register_result) {
                    if (isset($tempt)) {
                        move_uploaded_file($tempt, '../img/' . $avatar);
                    }
                    header("location: login.php");
                } else {
                    $error_register = "lỗi tạo profile";
                }
            }else
                $error = "tên người dùng đã tồn tại";
        }
        include ('../../View/register.php');
    }
}