<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/11/2016
 * Time: 5:22 PM
 */
include ('../../Repositories/Connect.php');
include ('../../Repositories/PhotoRepositories.php');
include ('../../Repositories/AlbumRepositories.php');
include ('../../Model/User.php');
class ImageController
{
    private $image;
    private $album;
    public function __construct()
    {
        $this->image= new photoRepositories();
        $this->album = new AlbumRepositories();
    }

    /**
     * Action thêm ảnh
     */
    public function addImageAction(){
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            if(isset($_GET['albumID'])) {
                $albumID = $_GET['albumID'];
                $nameAlbum = $this->album->nameAlbum($albumID);
                $photos = $this->image->retrievePhotoAlbum($user->getId(), $albumID);
                $add = "thêm ảnh";


                if (isset($_POST['submit'])) {
                    $names = [];
                    $temps = [];
                    $uploadOk = [];
                    foreach ($_FILES['img']['tmp_name'] as $key => $temp) {
                        $check = getimagesize($temp);
                        if ($check !== false) {
                            $uploadOk[$key] = 1;
                        } else {
                            $uploadOk[$key] = 0;
                        }
                    }

                    //validate ảnh
                    foreach ($_FILES['img']['size'] as $key => $size) {
                        if ($size < 50000) {
                            $uploadOk[$key] = 1;
                        } else {
                            $uploadOk[$key] = 0;
                        }
                    }

                    foreach ($_FILES['img']['type'] as $key => $type) {
                        if ($type != "jpg" && $type != "png" && $type != "jpeg"
                            && $type != "gif"
                        ) {
                            $uploadOk[$key] = 0;
                        } else {
                            $uploadOk[$key] = 1;
                        }
                    }

                    //up ảnh
                    foreach ($_FILES['img']['name'] as $key => $name) {
                        if ($uploadOk[$key] == 1) {
                            $result = $this->image->createPhoto($albumID, $name, $user->getId());
                            move_uploaded_file($_FILES['img']['tmp_name'][$key], '../../img/' . basename($name));
                        }
                    }

                    if (!$result) {
                        $error_addimage = "Thêm ảnh không thành công";
                    } else {
                        header("location: showimg.php?albumID=$albumID");
                    }
                }
                include('../../View/addimg.php');
            }
        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Action show ảnh theo album
     */
    public function showImageAction(){
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            if(isset($_GET['albumID'])) {
                //kiểm tra xem có lỗi không để thông báo cho người dùng
                if(isset($_GET['error'])&&isset($_GET['photoID'])){
                    $error = $_GET['error'];
                    $photoID = $_GET['photoID'];
                }
                $albumID = $_GET['albumID'];
                $nameAlbum = $this->album->nameAlbum($albumID);
                $photos  =$this->image->retrievePhotoAlbum($user->getId(), $albumID);
                $albums = $this->album->showAlbum($user->getId());
                if(!$photos){
                    $error_show_img = "Lỗi truy xuất ảnh";
                }
                include ("../../View/addimg.php");
            }

        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Xóa từng ảnh theo ID
     */
    public function deleteImageAction(){
        if(isset($_SESSION['user'])){
            if(isset($_GET['photoID'])&&isset($_GET['AlbumID'])){
                $photoID = $_GET['photoID'];
                $albumID = $_GET['AlbumID'];
                $result = $this->image->deletePhoto($photoID);
                if(!$result){
                    $error_delete_img = "Không xóa được ảnh này";
                    header("location: showimg.php?albumID=$albumID&error=$error_delete_img&photoID=$photoID");
                }else{
                    header("location: showimg.php?albumID=$albumID");
                }
            }
        }else{
            header('location: ../user/login.php');
        }
    }
}