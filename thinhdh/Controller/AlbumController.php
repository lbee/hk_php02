<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/10/2016
 * Time: 5:33 PM
 */
include ('../../Repositories/Connect.php');
include ('../../Repositories/AlbumRepositories.php');
include ('../../Repositories/PhotoRepositories.php');

class AlbumController
{
    private $album;
    private $photo;
    public function __construct()
    {
        $this->album =new AlbumRepositories();
        $this->photo = new photoRepositories();
    }

    /**
     * Action show các album của người dùng
     */
    public function showAlbumAction(){
        //kiểm tra xem có lỗi không để thông báo cho người dùng
        if(isset($_GET['error'])&&isset($_GET['albumID'])){
            $error = $_GET['error'];
            $albumID = $_GET['albumID'];
        }
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            $albums = $this->album->showAlbum($user->getId());
            if($albums==null){
                header('location: addalbum.php');
            }else{
                foreach ($albums as $album) {
                    $photos[$album['AlbumID']][] = $this->photo->retrievePhotoAlbum($album['AlbumID'], $user->getId());
                }
            }
            include ('../../View/album.php');
        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Action thêm album
     */
    public function addAlbumAction(){
        if(isset($_SESSION['user'])){
            $user = $_SESSION['user'];
            $add = "thêm";
            $albums = $this->album->showAlbum($user->getId());
            if(isset($_POST['submit'])) {
                $albumName = $_POST['albumName'];
                $album = $this->album->createAlbum($user->getId(), $albumName);
                if (!$album) {
                    $error_show = "Thêm album không thành công";
                }else{
                    header('location: album.php');
                }
            }
            include ('../../View/album.php');
        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Action xóa album
     */
    public function deleteAlbumAction(){
        if(isset($_SESSION['user'])){
            if(isset($_GET['AlbumID'])){
                $albumID = $_GET['AlbumID'];
                $result = $this->album->deleteAlbum($albumID);
                if(!$result){
                    $error_edit_album = 'Không xóa được album';
                    //thông báo không xóa được album
                    header("location: album.php?error=$error_edit_album &albumID=$albumID");
                }else{
                    header('location: album.php');
                }
            }else{
                header('location: album.php');
            }
        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Action Xóa ảnh trong album trước khi xóa album
     */
    public function deleteImgAlbum(){
        if(isset($_SESSION['user'])){
            if(isset($_GET['AlbumID'])){
                $albumID = $_GET['AlbumID'];
                $result = $this->photo->deletePhotoAlbum($albumID);
                if(!$result){
                    $error_delete_img_album = 'Không xóa được ảnh trong album';
                    //thông báo không xóa được ảnh trong album => không xóa album
                    header("location: album.php?error=$error_delete_img_album&albumID=$albumID");
                }else{
                    header("location: deleteAlbum.php?albumID=$albumID");
                }
            }else{
                header('location: album.php');
            }
        }else{
            header('location: ../user/login.php');
        }
    }

    /**
     * Action sửa album
     */
    public function editAlbumAction(){
        if(isset($_GET['AlbumID'])){
            $albumID = $_GET['AlbumID'];
            if(isset($_POST['submit'])) {
                $albumName = $_POST['albumName'];
                $result = $this->album->updateAlbum($albumID, $albumName);
                if(!$result){
                    $error_edit_album = 'Không sửa được album';
                    //thông báo không sửa được
                    header("location: album.php?error=$error_edit_album &albumID=$albumID");
                }else{
                    header('location: album.php');
                }
            }
        }else{
            header('location: album.php');
        }
    }
}