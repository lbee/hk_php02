<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 2:57 PM
 */
class user
{
    private $id;
    private $username;
    private $pass;
    private $lastlogin;

    /**
     * @return mixed
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @param mixed $lastlogin
     */
    public function setLastlogin($lastlogin)
    {
        $this->lastlogin = $lastlogin;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }


    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
}
