<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/10/2016
 * Time: 5:40 PM
 */
include ('../../Model/Album.php');

class AlbumRepositories extends connect
{
    /**
     * tạo album mới
     * @param $userID
     * @param $albumName
     * @return bool
     */
    public function createAlbum($userID, $albumName){
        $sql_create_album = "INSERT INTO album(UserID, AlbumName)VALUES ('$userID', '$albumName')";
        $statement = $this->conn->prepare($sql_create_album);
        $result = $statement->execute();
        if(!$result){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * thực hiện sửa album theo album id và username
     * @param $albumID
     * @param $albumName
     * @return bool
     */
    public function updateAlbum($albumID, $albumName){
        $sql_update_album = "UPDATE album SET AlbumName = '$albumName' WHERE AlbumID = '$albumID'";
        $statement = $this->conn->prepare($sql_update_album);
        $result = $statement->execute();
        if(!$result){
            return false;
        }else{
            return true;
        }
    }

    /**
     * thực hiện lấy ra danh sách album
     * @param $userID
     * @return null
     */
    public function showAlbum($userID){
        $sql_show_album = "SELECT * FROM album WHERE UserID = '$userID'";
        $statement = $this->conn->prepare($sql_show_album);
        $statement->execute();
        $albums = $statement->fetchAll();
        if(count($albums)==0){
            return null;
        }else{
            foreach ($albums as $album){
                $this->convertToObject($album);
            }
            return $albums;
        }
    }

    /**
     * thực hiện lấy tên album theo album id
     * @param $albumID
     * @return bool
     */
    public function nameAlbum($albumID){
        $sql_name_album = "SELECT AlbumName FROM album WHERE AlbumID='$albumID'";
        $statement = $this->conn->prepare($sql_name_album);
        $statement->execute();
        $result = $statement->fetch();
        if(!$result){
            return false;
        }else{
            return $result;
        }
    }

    /**
     * thực hiện xóa album trong csdl
     * @param $albumID
     * @return bool
     */
    public function deleteAlbum($albumID){
        $sql_delete_album = "DELETE FROM album WHERE AlbumID='$albumID'";
        $startement = $this->conn->prepare($sql_delete_album);
        $result = $startement->execute();
        if(!$result){
            return false;
        }else{
            return true;
        }
    }

    /**
     * chuyển từ mảng album sang đối tượng album
     * @param $arr_Album
     * @return Album
     */
    private function convertToObject($arr_Album){
        $object_Album = new Album();
        $object_Album->setAlbumID($arr_Album['AlbumID']);
        $object_Album->setAlbumName($arr_Album['AlbumName']);
        $object_Album->setUserID($arr_Album['UserID']);
        return $object_Album;
    }
}