<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 7:51 PM
 */

include 'Connect.php';
include '../../Model/User.php';
class UserRepositories extends connect
{
    /**
     * thực hiện kiểm tra xem trong csdl có người dùng với pass đó không
     * @param $username
     * @param $pass
     * @return null|user
     */
    public function login($username, $pass){
        $sql_login="SELECT * FROM user WHERE Username = '$username' AND Password= '$pass'";
        $statement = $this->conn->prepare($sql_login);
        $statement->execute();
        $row = $statement->fetchAll();

        if(count($row)==0){
            return null;
        }else{

            return $this->convertToObject($row);
        }
    }

    /**
     * thực hiện chuyển user từ mảng sang object
     * @param $arrUser
     * @return user
     */
    public function convertToObject($arrUser){
        $objUser = new user();

        $objUser->setId($arrUser[0]['UserID']);
        $objUser->setUsername($arrUser[0]['Username']);
        $objUser->setPass($arrUser[0]['Password']);
        $objUser->setLastlogin($arrUser[0]['Lastlogin']);

        return $objUser;
    }

    /**
     * Tạo người dùng mới
     * @param $username
     * @param $pass
     */
    public function createUser($username, $pass){
        $sql_create_user = "INSERT INTO user(Username, Password) VALUES('$username', '$pass')";
        $statement = $this->conn->prepare($sql_create_user);
        $result = $statement->execute();
        if(!$result){
            echo "tạo người dùng không thành công";
        }
    }

    /**
     * lấy ra username
     * @param $username
     * @return null
     */
    public function checkUser($username){
        $sql_check_user = "SELECT * FROM user WHERE Username='$username'";
        $statement = $this->conn->prepare($sql_check_user);
        $statement->execute();
        $result = $statement->fetchAll();
        if(count($result)==0){
            return null;
        }
        else{
            return $result;
        }
    }
}