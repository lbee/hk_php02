<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 6:07 PM
 */
include ('../../Model/Images.php');

class photoRepositories extends connect
{
    /**
     * tạo ảnh mới
     * @param $albumID
     * @param $photoURL
     * @param $userid
     * @return bool
     */
    public function createPhoto($albumID, $photoURL, $userid){
        $sql_create_image = "INSERT INTO image(AlbumID, PhotoURL, UserID)VALUES ('$albumID', '$photoURL', $userid)";
        $statement = $this->conn->prepare($sql_create_image);
        $result_create_image = $statement->execute();
        if(!$result_create_image){
            return false;
        }else{
            return true;
        }
    }

    /**
     * lấy ảnh của người dùng qua album id
     * @param $userID
     * @param $albumID
     * @return bool
     */
    public function retrievePhotoAlbum($userID, $albumID){
        $sql_retrieve_image = "SELECT * FROM image WHERE UserID = '$userID' AND AlbumID = '$albumID'";
        $statement = $this->conn->prepare($sql_retrieve_image);
        $statement->execute();
        $result = $statement->fetchAll();
        if(count($result)==0){
            return false;
        }
        else{
            return $result;
        }
    }

    /**
     * thực hiện xóa ảnh trong album
     * @param $albumID
     * @return bool
     */
    public function deletePhotoAlbum($albumID){
        $sql_delete_image_album = "DELETE FROM image WHERE AlbumID = '$albumID'";
        $statement = $this->conn->prepare($sql_delete_image_album);
        $result = $statement->execute();
        if(!$result){
            return false;
        }
        else{
            return true;
        }
    }
    public function deletePhoto($photoID){
        $sql_delete_image = "DELETE FROM image WHERE PhotoID = '$photoID'";
        $statement = $this->conn->prepare($sql_delete_image);
        $result = $statement->execute();
        if(!$result){
            return false;
        }
        else{
            return true;
        }
    }
}