<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 2:31 PM
 */
class connect
{
    protected  $conn;

    public function __construct()
    {
        $this->connect();
    }

    /**
     * thực hiện kết nối csdl
     */
    public function connect(){
        $servername="localhost";
        $username = "root";
        $password = "";
        $dbname = "photogram";
        $option = array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES utf8");

        try{
            $this->conn = new PDO("mysql: host=$servername;dbname=$dbname",$username,$password,$option);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        }catch (PDOException $e) {
            echo "Lỗi kết nối cơ sở dữ liệu:" . $e->getMessage();
            exit();
        }
    }
}