<?php

/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 3:31 PM
 */
include ('../../Model/userProfile.php');
class UserpProfileRepositories  extends connect
{
    /**
     * thực hiện thêm profile của người dùng
     * return true nếu thành công
     * @param $email
     * @param $fullname
     * @param $DoB
     * @param $address
     * @param $phone
     * @param $avatar
     * @param $gender
     * @return false nếu không tạo thành công và ngược lại
     */
    public function createUserProfile( $email, $fullname, $DoB, $address, $phone, $avatar, $gender){
        $sql_create_userprofile = "INSERT INTO userprofile(Email, Fullname, DoB, Address, Avatar,  Phone, Gender)VALUES('$email', '$fullname', '$DoB', '$address', '$avatar', '$phone', $gender)";
        $statment = $this->conn->prepare($sql_create_userprofile);
        $result_create = $statment->execute();
        if($result_create){
           return true;
        } else
            return false;
    }

    /**
     * lấy ra profile của người dùng theo userID
     * @param $userID
     * @return userProfile
     * @return false nếu không lấy thành công
     */
    public function retrieveUser($userID){
        $sql_retireve_user = "SELECT * FROM userprofile WHERE UserID = '$userID'";
        $statement_retrieve = $this->conn->prepare($sql_retireve_user);
        $statement_retrieve->execute();
        $result_retrieve = $statement_retrieve->fetch();
        if(!$result_retrieve){
            return false;
        }
        else{
            $user = new userProfile();
            $user->setId($userID);
            $user->setAvatar($result_retrieve['Avatar']);
            $user->setFullname($result_retrieve['Fullname']);
            $user->setDoB($result_retrieve['DoB']);
            $user->setAddress($result_retrieve['Address']);
            $user->setGender($result_retrieve['Gender']);
            $user->setEmail($result_retrieve['Email']);
            $user->setPhone($result_retrieve['Phone']);
            return $user;
        }

    }

    /**
     * Sửa profile của người dùng
     * @param $userID
     * @param $fullname
     * @param $email
     * @param $DoB
     * @param $address
     * @param $phone
     * @param $avatar
     * @param $gender
     * @return  false nếu không sửa thành công
     */
    public function updateUser($userID, $fullname, $email, $DoB, $address, $phone, $avatar, $gender){
        $sql_update_user = "UPDATE userprofile 
                            SET Fullname = '$fullname', Email = '$email',DoB = '$DoB', 
                                Address = '$address', Phone = '$phone', Avatar = '$avatar', Gender = $gender 
                            WHERE UserID = '$userID'";
        $statement_update = $this->conn->prepare($sql_update_user);
        $result_update = $statement_update->execute();
        if(!$result_update){
            setcookie('error_update','sửa người dùng bị lỗi', time()+300);
        }
    }
}