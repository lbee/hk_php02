-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2016 at 12:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photogram`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `AlbumID` int(11) NOT NULL,
  `AlbumName` varchar(50) NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`AlbumID`, `AlbumName`, `UserID`) VALUES
(3, 'Friend', 1),
(4, 'Family', 1);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `PhotoID` int(11) NOT NULL,
  `AlbumID` int(11) NOT NULL,
  `PhotoName` varchar(50) NOT NULL,
  `PhotoURL` text NOT NULL,
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`PhotoID`, `AlbumID`, `PhotoName`, `PhotoURL`, `UserID`) VALUES
(49, 4, '', '10387846_673467349373362_1377885546_a.jpg', 1),
(50, 4, '', '49128695.jpg', 1),
(51, 4, '', '49128742.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(32) NOT NULL,
  `Lastlogin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Username`, `Password`, `Lastlogin`) VALUES
(1, 'thinh', '87ef067531ad5e77c15a8709c37953ef', NULL),
(6, 'thinh123', '578f1deba58c1127f9254e77c6053638', NULL),
(7, 'nam', '54e193277215c92570bc9659bd48ff40', NULL),
(8, 'Tuấn', '0a49dd9af0104d505403a2aba1f1293a', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `UserID` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Fullname` varchar(50) NOT NULL,
  `DoB` date NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Avatar` varchar(255) NOT NULL,
  `Phone` varchar(14) NOT NULL,
  `Gender` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`UserID`, `Email`, `Fullname`, `DoB`, `Address`, `Avatar`, `Phone`, `Gender`) VALUES
(1, 'huuthinh95.do@gmail.com', 'Đỗ Hữu Thịnh', '1995-07-30', 'Hà Nội', 'anh1.jpg', '098425095', 1),
(6, 'huuthinh99.do@gmail.com', 'Thịnh Hữu Đỗ', '1978-10-16', 'Hà Nam', 'anh5.jpg', '096797696', 1),
(7, 'nam123@gmail.com', 'Nam ĐT', '1876-06-30', 'Mỹ', 'default-avatar.jpg', '0987654567', 1),
(8, 'tuan134556@gmail.com', 'Tuấn abc', '1789-12-10', 'Anh', 'default-avatar.jpg', '0965844735', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`AlbumID`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`PhotoID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `AlbumID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `PhotoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `userprofile`
--
ALTER TABLE `userprofile`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
