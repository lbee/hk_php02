<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/10/2016
 * Time: 2:14 PM
 */
?>
<!DOCTYPE HTML>
<html>
<header>
    <meta charset="utf-8">
    <title>Thông tin cá nhân</title>
    <script type="text/javascript" src="../../Assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../Assets/bootsrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/css/style_profile.css">
</header>
<body>
<div class="profile">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $user->getUsername(); ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="../../img/<?php echo $profile->getAvatar();?>" class="img-circle img-responsive"> </div>

                            <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                              <dl>
                                <dt>DEPARTMENT:</dt>
                                <dd>Administrator</dd>
                                <dt>HIRE DATE</dt>
                                <dd>11/12/2013</dd>
                                <dt>DATE OF BIRTH</dt>
                                   <dd>11/12/2013</dd>
                                <dt>GENDER</dt>
                                <dd>Male</dd>
                              </dl>
                            </div>-->
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Full Name:</td>
                                        <td><?php echo $profile->getFullname(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td><?php echo $profile->getEmail(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth:</td>
                                        <td><?php echo $profile->getDoB();?></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>
                                            <?php
                                            $gender = $profile->getGender();
                                            if($gender == 1){
                                                echo "Nam";
                                            }elseif($gender==0){
                                                echo "Nữ";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Home Address:</td>
                                        <td><?php echo $profile->getAddress();?></td>
                                    </tr>
                                    <td>Phone Number:</td>
                                    <td>
                                        <?php echo $profile->getPhone();?>
                                    </td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="logout">
                            <a href="logout.php" data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-log-out"></i></a>
                        </div>
                        <div class="myalbum">
                            <a href="../album/album.php" type="button" class="btn"><i class="btn-block">My Album</i></a>
                        </div>
                        <div class="edit">
                            <a href="edit_profile.php" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
