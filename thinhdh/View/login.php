<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/7/2016
 * Time: 7:55 PM
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Đăng nhập</title>
    <script type="text/javascript" src="../Asset/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../Asset/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/css/style_login.css">
</head>
<body>
<section class="login">
    <div class="titulo">User Login</div>
    <div class="default_img"><img src="../../img/images%20(1).jpg" alt=""></div>
    <form method="post" enctype="application/x-www-form-urlencoded">
        <input type="text" required title="Username required" placeholder="Username" data-icon="U" name="username">
        <input type="password" required title="Password required" placeholder="Password" data-icon="x" name="password">
        <div class="remember">Remember me <input type="checkbox" name="remember" value="yes"></div>
        <input type="submit"  class="enviar" name="submit" value="Submit"/>
        <div class="olvido">
            <div class="col" style="text-align: left"><a href="register.php" title="Ver Carásteres" >Register</a></div>
            <div class="col" style="text-align: right"><a href="#" title="Recuperar Password" >Forgot Password?</a></div>
        </div>
    </form>
</section>
</body>
</html>

