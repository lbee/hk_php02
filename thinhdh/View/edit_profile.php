<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/10/2016
 * Time: 3:17 PM
 */
?>
<!DOCTYPE HTML>
<html>
<header>
    <meta charset="utf-8">
    <title>Sửa thông tin</title>
    <script type="text/javascript" src="../../Assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../Assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="../../Assets/css/style_editprofile.css">
</header>
<body>
<div class="container" style="padding-top: 20px;">
    <h1 class="page-header">Edit Profile</h1>
    <div class="row ">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $user->getUsername(); ?></h3>
            </div>
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post">
                <!-- left column -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="text-center">

                        <img src="../../img/<?php echo $profile->getAvatar(); ?>" class="avatar img-thumbnail" id="avatar_img" alt="avatar">
                        <h6>Upload a different photo...</h6>
                        <input type="file" class="text-center center-block well well-sm" name="avatar" onchange="readURL(this)">
                        <input type="hidden" value="<?php echo $profile->getAvatar();?>" name="avatar">
                    </div>
                </div>
                <!-- edit form column -->
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">

                    <h3>Personal info</h3>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Full Name:</label>
                        <div class="col-lg-8">
                            <input class="form-control" value="<?php echo $profile->getFullname();?>" name="fullname" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-8">
                            <input class="form-control" value="<?php echo $profile->getEmail();?>" name="email" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Date of Birth:</label>
                        <div class="col-lg-8">
                            <input class="form-control" value="<?php echo $profile->getDoB();?>" id="birthDate" name="DoB" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Gender:</label>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input type="radio" id="femaleRadio" value="0" name="gender">Female
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input type="radio" id="maleRadio" value="1" name="gender">Male
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input type="radio" id="uncknownRadio" value="2" name="gender">Unknown
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Home Address:</label>
                        <div class="col-lg-8">
                            <input class="form-control" value="<?php echo $profile->getAddress(); ?>" type="text" name="address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Phone:</label>
                        <div class="col-md-8">
                            <input class="form-control" value="<?php echo $profile->getPhone();?>" type="text" name="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input class="btn btn-primary" value="Save Changes" type="submit" name="submit">
                            <span></span>
                            <input class="btn btn-default" value="Cancel" type="reset">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script language="javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar_img')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#birthDate').datepicker;

    var gender= <?php echo $profile->getGender();?>;
    if($('#maleRadio').val()==gender){
        $('#maleRadio').attr('checked', 'checked');
    }else if($('#femaleRadio').val()==gender){
        $('#femaleRadio').attr('checked', 'checked');
    }else {
        $('#uncknownRadioRadio').attr('checked', 'checked');
    }

</script>
</body>
</html>

