<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/11/2016
 * Time: 2:32 PM
 */?>
<!DOCTYPE HTML>
<html>
<header>
    <meta charset="utf-8">
    <title>ALBUM</title>
    <script type="text/javascript" src="../../Assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../Assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/css/album.css">
</header>
<body>
<div class="profile">
    <div class="container">
        <div class="row">
            <div class="navbar">
                <ul>
                    <li >
                        <a href="../user/profile.php" ><button class="glyphicon glyphicon-arrow-left"></button></a>

                    </li>
                    <li>
                        <div class="title">
                            <h1><?php echo $user->getUserName();?> Album</h1>
                        </div>
                    </li>
                    <li>
                        <a href="addalbum.php" ><button class="btn_add glyphicon glyphicon-credit-card"></button></a>
                        <div>
                            <form id="addNewForm" method="POST" style="display: none">
                                <input placeholder="Enter album name" type="text" name="albumName">
                                <button type="submit" class="ui primary button" name="submit">
                                    Save
                                </button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>

            <?php
                if(isset($albums)):
                    foreach ($albums as $album):
                        $i=0;
                        if(isset($error)&&$albumID == $album['AlbumID']){?>
                            <p class="label label-danger"><?php echo $error; ?></p>
                        }
                        <?php }?>
            <div class="album">
                <a href="../image/showimg.php?albumID=<?php echo $album['AlbumID']; ?>" >
                    <h1 class="albumName"><?php echo $album['AlbumName']; ?></h1>
                </a>
                <div class="extra content">
                    <a href="#" data-toggle="modal" data-target="#<?php echo $album['AlbumID']; ?>">
                        <i class="edit icon"></i>
                        Sửa
                    </a>
                    <a href="#" onClick="if(confirm('Bạn có chắc chắn muốn xóa: <?php echo $album['AlbumName'];?> không?')){location.href='deleteImgAlbum.php?AlbumID=<?php echo $album['AlbumID']; ?>'}" >
                        <i class="delete icon"></i>
                        Xóa
                    </a>
                </div>
            </div>
            <div id="<?php echo $album['AlbumID']; ?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Chỉnh sửa tên album</h4>
                        </div>
                        <form method="post" action="editalbum.php?AlbumID=<?php echo $album['AlbumID']; ?>">
                            <div class="modal-body">
                                <input name="albumName" class="form-control" type="text"
                                       placeholder="Tên album"
                                       value="<?php echo $album['AlbumName']; ?>"/>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="submit" class="btn btn-primary">Save
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <?php

            if($i%3==2){
                echo '<div class="clearfix"></div>';
            }
            $i++;
            endforeach;
            endif;?>

        </div>
    </div>
</div>
<?php
   if (isset($add)):
?>
<script>
        $('#addNewForm').css('display', 'block');
        $('.btn_add').css('display', 'none');

</script>
<?php
    endif;
?>

</body>
</html>

