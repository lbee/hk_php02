<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/12/2016
 * Time: 1:02 AM
 */

?>
<!DOCTYPE HTML>
<html>
<header>
    <meta charset="utf-8">
    <title>Thông tin cá nhân</title>
    <script type="text/javascript" src="../../Assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../Assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/css/style_img.css">
</header>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="navbar">
                <ul>
                    <li>
                        <a href="../album/album.php" ><button class="glyphicon glyphicon-arrow-left"></button></a>

                    </li>
                    <li>
                        <div class="title">
                            <h1><?php echo $user->getUserName();?> Photo</h1>
                            <h3><?php echo $nameAlbum['AlbumName']; ?></h3>
                        </div>
                    </li>
                    <li>
                        <a href="addimg.php?albumID=<?php echo $albumID; ?>" ><button class="btn_add glyphicon glyphicon-credit-card"></button></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="upload" <?php if(isset($add)){echo 'style="display: block"';};?>>
    <div class="container">
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <form method="post" enctype="multipart/form-data">
                        <ul>
                            <li>
                                <span class="btn btn-default btn-file">
                                    Select photo<input type="file" class="text-center center-block well well-sm" name="img[]" onchange="readURL(this)" multiple=””>
                                </span>

                            </li>
                            <li>
                                <input type="submit" value="save" name="submit">
                            </li>
                        </ul>
                    </form>
                </div>

                <div id="photo_frame">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="gallery">
    <div class="container">
        <div class="row">
            <div class="gallery-item">
                <?php
                    if($photos):
                        foreach ($photos as $photo):
                            if(isset($error)&&$photoID == $photo['PhotoID']){?>
                                <p class="label label-danger"><?php echo $error; ?></p>
                                }
                            <?php }?>

                    <a href="#galleryImg1" id="img_modal" class="link-gallery" data-toggle="modal" data-target="#modalGallery">
                        <img src="../../img/<?php echo $photo['PhotoURL'];?>" class="img-responsive img-gallery" alt="First image">
                    </a>
                            <div class="modal fade" id="modalGallery" tabindex="-1" role="dialog" aria-labelledby="modalGalleryLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <a href="#" id="del" onClick="if(confirm('Bạn có chắc chắn muốn xóa bức ảnh này không?')){location.href='deleteImg.php?photoID=<?php echo $photo['PhotoID']; ?>&AlbumID=<?php echo $albumID;?>'}" >
                                                <i class="delete icon"></i>
                                                Xóa
                                            </a>
                                        </div> <!-- /.modal-header -->

                                        <div class="modal-body">
                                            <div id="carouselGallery" class="carousel slide" data-ride="carousel" data-interval="false">
                                                <div class="carousel-inner">
                                                </div> <!-- /.carousel-inner -->
                                            </div> <!-- /.carousel -->
                                        </div> <!-- /.modal-body -->

                                        <div class="modal-footer">
                                            <ul class="pagination">
                                            </ul>
                                        </div> <!-- /.modal-footer -->
                                    </div> <!-- /.modal-content -->
                                </div> <!-- /.modal-dialog -->
                            </div> <!-- /.modal -->
                <?php endforeach;
                    endif;?>

            </div> <!-- /.col -->
        </div> <!--/.row  -->
    </div> <!-- /.container -->
</div> <!-- /.gallery -->


<script>
    $(document).ready(function(){

        $('.link-gallery').click(function(){
            var galleryId = $(this).attr('data-target');
            var currentLinkIndex = $(this).index('a[data-target="'+ galleryId +'"]');

            createGallery(galleryId, currentLinkIndex);
            createPagination(galleryId, currentLinkIndex);

            $(galleryId).on('hidden.bs.modal', function (){
                destroyGallry(galleryId);
                destroyPagination(galleryId);
            });

            $(galleryId +' .carousel').on('slid.bs.carousel', function (){
                var currentSlide = $(galleryId +' .carousel .item.active');
                var currentSlideIndex = currentSlide.index(galleryId +' .carousel .item');

                setTitle(galleryId, currentSlideIndex);
                setPagination(++currentSlideIndex, true);
            })
        });

        function createGallery(galleryId, currentSlideIndex){
            var galleryBox = $(galleryId + ' .carousel-inner');

            $('a[data-target="'+ galleryId +'"]').each(function(){
                var img = $(this).html();
                var galleryItem = $('<div class="item">'+ img +'</div>');

                galleryItem.appendTo(galleryBox);
            });

            galleryBox.children('.item').eq(currentSlideIndex).addClass('active');
            setTitle(galleryId, currentSlideIndex);
        }

        function destroyGallry(galleryId){
            $(galleryId + ' .carousel-inner').html("");
        }

        function createPagination(galleryId, currentSlideIndex){
            var pagination = $(galleryId + ' .pagination');
            var carouselId = $(galleryId).find('.carousel').attr('id');
            var prevLink = $('<li><a href="#'+ carouselId +'" data-slide="prev">«</a></li>');
            var nextLink = $('<li><a href="#'+ carouselId +'" data-slide="next">»</a></li>');

            prevLink.appendTo(pagination);
            nextLink.appendTo(pagination);

            $('a[data-target="'+ galleryId +'"]').each(function(){
                var linkIndex = $(this).index('a[data-target="'+ galleryId +'"]');
                var paginationLink = $('<li><a data-target="#carouselGallery" data-slide-to="'+ linkIndex +'">'+ (linkIndex+1) +'</a></li>');

                paginationLink.insertBefore('.pagination li:last-child');
            });

            setPagination(++currentSlideIndex);
        }

        function setPagination(currentSlideIndex, reset = false){
            if (reset){
                $('.pagination li').removeClass('active');
            }

            $('.pagination li').eq(currentSlideIndex).addClass('active');
        }

        function destroyPagination(galleryId){
            $(galleryId + ' .pagination').html("");
        }

        function setTitle(galleryId, currentSlideIndex){
            $(galleryId + ' .item').eq(currentSlideIndex).find('img').attr('alt');
        }
    });

    function readURL(input) {
        <?php $i = 0;?>
        var i;
        if(input.files.length>5){
            alert ('Bạn chỉ được phép chọn 5 ảnh trong 1 lượt upload');
        }else {
            for(i=0;i<input.files.length;i++){
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        var img = $('<img id="dynamic">'); //Equivalent: $(document.createElement('img'))
                        img.attr('src', e.target.result);
                        img.appendTo('#photo_frame');
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    }
</script>
</body>
