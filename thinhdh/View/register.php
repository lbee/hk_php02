<?php
/**
 * Created by PhpStorm.
 * User: THINHDO
 * Date: 10/10/2016
 * Time: 4:18 PM
 */
?>
<!DOCTYPE HTML>
<html>
<header>
    <meta charset="utf-8">
    <title>Đăng ký</title>
    <script type="text/javascript" src="../../Assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../../Assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../Assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="../../Assets/css/style_register.css">
</header>
<body>
<div class="container">
    <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <h2>Registration Form</h2>
        <?php if(isset($error)): ?>
            <p class="label label-danger"><?php echo $error; ?></p>
        <?php endif; ?>
        <div class="form-group">
            <label for="username" class="col-sm-3 control-label">User Name</label>
            <div class="col-sm-9">
                <input type="text" id="username" placeholder="User Name" class="form-control" name="username" autofocus required>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
                <input type="password" id="password" placeholder="Password" name="pass" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">Full Name</label>
            <div class="col-sm-9">
                <input type="text" id="firstName" placeholder="Full Name" class="form-control" name="fullname" autofocus required>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input type="email" id="email" placeholder="Email" class="form-control" name="email" required>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-3 control-label">Phone</label>
            <div class="col-sm-9">
                <input type="text" id="phone" name="phone" placeholder="Phone number"  class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
            <div class="col-sm-9">
                <input type="date" id="birthDate" class="form-control" name="DoB" required>
            </div>
        </div>
        <div class="form-group">
            <label for="country" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
                <input type="text" placeholder="Address" class="form-control" name="address" required>
            </div>
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label class="control-label col-sm-3">Gender</label>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <label class="radio-inline">
                            <input type="radio" id="femaleRadio" value="0" name="gender">Female
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <label class="radio-inline">
                            <input type="radio" id="maleRadio" value="1" name="gender">Male
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <label class="radio-inline">
                            <input type="radio" id="uncknownRadio" value="2" name="gender">Unknown
                        </label>
                    </div>
                </div>
            </div>
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="avatar" class="col-sm-3 control-label">Avatar</label>
            <div class="col-sm-6">
                <img src="../../img/default-avatar.jpg" id="avatar_img" alt="">
                <input type="file" id="avatar" name="avatar" onchange="readURL(this);">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
            </div>
        </div>
    </form> <!-- /form -->
</div> <!-- ./container -->
<script language="javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar_img')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#birthDate').datepicker;
</script>
</body>
</html>

