<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Profile</title>
        <link rel="stylesheet" href="../../assets/css/reset.css"/>
        <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
    </head>
    <body>
    <!-- Thêm phần navigation vào -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Photo Album</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="../../routes/album/album.php">Quản lí album <span class="sr-only">(current)</span></a></li>
                </ul>
                <!--  sổ menu-->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <!-- ảnh avt -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <img src="../../imgavatar/<?php echo $user->getUsername().'/'.$profile->getAvatar(); ?>" alt="avatar" width="24" height="24"/><?php echo $user->getUsername(); ?><span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../../routes/user/profile.php">Profile</a></li>
                            <li><a href="../../routes/user/logout.php">Đăng xuất</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<!-- ========================= Hết phần navigation -->
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Chỉnh sửa profile</h3>
            </div>
            <div class="panel-body">
                <?php if (isset($message)): ?>
                <div class="alert alert-success">
                    <?php echo $message; ?>
                </div>
                <?php endif; ?>
                <?php if (isset($error)): ?>
                    <div class="alert alert-danger">
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
                <!-- Form cập nhật-->
                <form method="post" action="#" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="fullname">FullName</label>
                        <input type="text" name="fname" class="form-control" id="fullname" placeholder="FullName"
                               value="<?php echo $profile->getFullname(); ?>">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" name='address' id="address"
                                  placeholder="Địa chỉ"><?php echo $profile->getAddress(); ?></textarea>
                    </div>

                    <!-- Giới tính -->
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <div class="radio">
                            <?php if ($profile->getGender()) { ?>
                                <label>
                                    <input type="radio" name="gender" id="gender" value="1" checked>
                                    Nam
                                </label>
                                <label>
                                    <input type="radio" name="gender" id="gender" value="0">
                                    Nữ
                                </label>
                            <?php } else { ?>
                                <label>
                                    <input type="radio" name="gender" id="gender" value="1">
                                    Nam
                                </label>
                                <label>
                                    <input type="radio" name="gender" id="gender" value="0" checked>
                                    Nữ
                                </label>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- Phone -->
                    <div class="form-group">
                        <label for="phoneNum">Phone number</label>
                        <input class="form-control" name="phone" id="phoneNum" placeholder="Phone number"
                               value="<?php echo $profile->getPhone(); ?>"/>
                    </div>

                    <!-- DOB -->
                    <div class="form-group">
                        <label for="dob">DOB</label>
                        <input class="form-control" id="dob" name='dob' placeholder="DOB" data-date-format="dd-mm-yyyy"
                               value="<?php echo DateTime::createFromFormat("Y-m-d", $profile->getDob())->format("d-m-Y"); ?>"/>
                    </div>

                    <!-- Avatar -->
                    <p><label for="avatar">Avatar</label></p>
                    <img src="<?php echo AVATARPATH . $user->getUsername() . '/' . $profile->getAvatar(); ?>" alt="" width="200" height="auto">
                    <input type="file" id="avatar" name="avatar" accept="image/*"/>
                    <input type="hidden" name="avatar" value="<?php echo $profile->getAvatar(); ?>" />
                    <br/>
                    <!-- Gửi form -->
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>

                </form>
                <!-- Hết form -->

            </div>
        </div>
    </div>
    <!-- End -->
    <script src="../../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
        $("#dob").datepicker();
    </script>

    </body>
</html>
