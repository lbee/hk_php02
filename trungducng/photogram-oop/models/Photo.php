<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/7/2016
 * Time: 7:27 PM
 */
class Photo
{
    private $id;
    private $album_id;
    private $photoName;
    private $photoURL;

    public function __construct($id, $album_id, $photoName, $photoURL)
    {
        $this->id = $id;
        $this->album_id = $album_id;
        $this->photoName = $photoName;
        $this->photoURL = $photoURL;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @return mixed
     */
    public function getPhotoName()
    {
        return $this->photoName;
    }

    /**
     * @return mixed
     */
    public function getPhotoURL()
    {
        return $this->photoURL;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $album_id
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
    }

    /**
     * @param mixed $photoName
     */
    public function setPhotoName($photoName)
    {
        $this->photoName = $photoName;
    }

    /**
     * @param mixed $photoURL
     */
    public function setPhotoURL($photoURL)
    {
        $this->photoURL = $photoURL;
    }

}