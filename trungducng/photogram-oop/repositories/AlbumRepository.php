<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/14/2016
 * Time: 9:00 PM
 */
include_once ('../../models/User.php');
include_once ('../../models/UserProfile.php');
include_once ('../../models/Album.php');
include_once ('Repository.php');
class AlbumRepository extends Repository
{
    /**
     * Thêm mới album
     * @param $conn : PDO
     * @param $name : Album name
     */
    public function createAlbum($name, $user_id)
    {
        $sql = "INSERT INTO `tbl_album`(`name`,`user_id`) VALUES('$name',$user_id)";
        $result = $this->connection->prepare($sql);
        $row = $result->execute();
        if($row){
            return $this->rowToModel($row);
        }
        return null;
    }

    /**
     * Lấy danh sach album thuộc người dùng $user_id
     * @param $conn
     * @param $user_id
     * @return mixed
     */
    public function retrieveAlbum($user_id)
    {
        $sql = "SELECT * FROM `tbl_album` WHERE user_id=$user_id ORDER BY id DESC";
        $obj = $this->connection->query($sql);
        $row = $obj->fetchAll();
        //nếu thực hiện thành công
        return $row;
    }

    public function rowToModel($row){
        $id = $row['id'];
        $name = $row['name'];
        $user_id = $row['user_id'];
        return new Album($id, $name, $user_id);
    }

    /**
     * Chỉnh sửa album
     * @param $conn : PDO

     */
    public function editAlbum($id, $name)
    {
        $sql = "UPDATE `tbl_album` SET `name`='$name' WHERE id=" . $id;
        $result = $this->connection->prepare($sql);
        // $result->execute();
        $row = $result->fetch();
        if($row){
            return $this->rowToModel($row);
        }
        return null;
    }

    /**
     * Delet album
     * @param $conn : PDO
     */
    function deleteAlbum($id)
    {
        $sql = "DELETE FROM `tbl_album` WHERE id=" . $id;
        $obj = $this->connection->prepare($sql);
        $obj->execute();
        $row = $obj->fetch();
        if($row){
            return $this->rowToModel($row);
        }
        return null;
    }
    /**
     * Load ảnh đầu tiên để hiển thị lên album
     * @param $conn : PDO
     * @param $album_id : ID album
     */
    public function loadFirstImage($album_id)
    {
        //giới hạn 1 kết quả trả về, sắp xếp theo id giảm dần (tức là ảnh mới nhất)
        $sql = "SELECT * FROM `tbl_photo` WHERE `album_id`=$album_id ORDER BY `id` DESC LIMIT 1";
        $object = $this->connection->query($sql);
        $row1 = $object->fetch();
        //kiểm tra xem có ảnh hay không bằng cách đếm số lượng object khi trả về, nếu có thì đưa ra đường dẫn và tên ảnh
        if($row1) {
                echo $row1['photoURL'] . '/' . $row1['photoName'];
        }
        //nếu album trống (ko có ảnh) thì load ảnh mặc định (GREY.PNG)
        else{
            return "../../img/grey.PNG";
        }
    }
}