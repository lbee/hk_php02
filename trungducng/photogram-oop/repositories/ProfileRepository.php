<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/10/2016
 * Time: 7:13 PM
 */
include_once ('../../models/User.php');
include_once ('../../models/UserProfile.php');
include_once ('Repository.php');

class ProfileRepository extends Repository
{


    /**
     * thêm mới người dùng với username, password
     * trả về true nếu thành công, và ngược lại
     * @param $username
     * @param $password
     */
    public function insert($username, $password) {
        //mã hóa password
        $passEncrypt = md5($password);
        //Đã có CURRENT_TIMESTAMP trong CSDL nên k cần nhập vào tbl_user
        $sql = "INSERT INTO `tbl_user`(`username`,`password`) VALUES('$username','$passEncrypt')";
        $result = $this->connection->query($sql);
        //nếu thực hiện thành công
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getLastID() {
        $user_id = $this->connection->lastInsertId();
        return $user_id;
    }


    /**
     * Hàm để thêm người dùng vào bảng tbl_profile
     * insertProfile
     */
    public function insertProfile($user_id, $fullname, $dob, $address, $gender, $phone, $avatar) {
        $sqlProfile = "INSERT INTO `tbl_profile`(`user_id`,`FullName`,`DOB`,`Address`,`Gender`,`Phone`,`Avatar`) VALUES('$user_id','$fullname','$dob','$address','$gender','$phone','$avatar')";

        $resultProfile = $this->connection->query($sqlProfile);

        if ($resultProfile) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Hàm đẻ tiến hành chỉnh sửa profile người dùng.
     * @param $conn : Connection
     * @param $username
     * @param $password
     * @param $fullName
     * @param $address
     * @param $gender
     * @param $phone
     * @param $avt
     */

    function editProfile($user_id, $fullname, $dob, $address, $gender, $phone, $avatar)
    {
        $sql = "UPDATE `tbl_profile` AS t SET t.user_id='$user_id',t.FullName='$fullname',t.DOB='$dob',t.Address='$address',t.Gender='$gender',
t.Phone='$phone',t.Avatar='$avatar' WHERE user_id=" . $user_id;

        $result = $this->connection->query($sql);
        return $result;
    }

    /**
     * Lấy profile của người dùng trong CSDL
     * @param $conn : PDO connection
     * @param $user_id : id_user
     */
    function loadProfile($user_id)
    {
        $sql = "SELECT * FROM `tbl_profile` AS p INNER JOIN `tbl_user` AS u ON p.user_id=u.id WHERE user_id=" . $user_id;
        $obj = $this->connection->query($sql);
        $result = $obj->fetch();
        // return $result;
        if ($result)
            return $this->rowToModel($result);
        return null;
    }

    public function rowToModel($result) {
        $user_id = $result['user_id'];
        $fullname = $result['FullName'];
        $dob = $result['DOB'];
        $address = $result['Address'];
        $gender = $result['Gender'];
        $phone = $result['Phone'];
        $file = $result['Avatar'];

        return new UserProfile($user_id, $fullname, $dob, $address, $gender, $phone, $file);
    }

    // function chỉ lấy đường dẫn avatar
    public function getAvatar($user_id) {
        $sql = "SELECT Avatar FROM `tbl_profile` AS p INNER JOIN `tbl_user` AS u ON p.user_id=u.id WHERE user_id=". $user_id;
        $obj = $this->connection->query($sql);
        $result = $obj->fetch();
        return $result;

    }
}