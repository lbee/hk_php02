<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/17/2016
 * Time: 3:03 PM
 */
include_once ('../../models/User.php');
include_once ('../../models/UserProfile.php');
include_once ('../../models/Album.php');
include_once ('../../models/Photo.php');
include_once ('Repository.php');
class PhotoRepository extends Repository
{
    /**
     * Lấy danh sách photo thuộc album_id
     * @param $conn : PDO Connection
     * @param $album_id : id album
     * @return mixed
     */
    function retrievePhoto($album_id)
    {
        //lấy danh sách photo được sắp xếp theo id giảm dần
        $sql = "SELECT * FROM `tbl_photo` WHERE `album_id`=$album_id ORDER BY id DESC";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        //nếu thực hiện thành công
        if($row){
            return $this->rowToModel($row);
        }
        return null;
    }

    public function rowToModel($row){
        $id = $row['id'];
        $album_id = $row['album_id'];
        $photoName = $row['photoName'];
        $photoURL = $row['photoURl'];
        return new Album($id, $album_id, $photoName, $photoURL);
    }
}