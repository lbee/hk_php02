<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/17/2016
 * Time: 3:03 PM
 */
include_once  ('../../repositories/UserRepository.php');
include_once  ('../../repositories/ProfileRepository.php');
include_once ('../../repositories/AlbumRepository.php');
include_once ('../../repositories/PhotoRepository.php');

class PhotoController
{
    private $photoRepository;
    private $profileRepository;
    public function __construct(){
        $this->photoRepository = new PhotoRepository();
        $this->profileRepository = new ProfileRepository();
    }

    public function actionPhoto() {
        if (!isset($_SESSION['_user'])) {
            header('Location: ../../routes/user/login.php');
        } else {
            // biến chuỗi đã serialize trở lại thành object
            $user = unserialize($_SESSION['_user']);

            // lấy avatar
            $avatar = $this->profileRepository->getAvatar($user->getId());

            //photo list
            $photoList = $this->photoRepository->retrievePhoto($user->getId());
        }
        // view
        include('../../views/photo/photo.php');
    }
}