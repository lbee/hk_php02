<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/10/2016
 * Time: 7:00 PM
 */
class User
{
    private $id;
    private $username;
    private $password;
    private $last_login;

    /**
     * User constructor.
     * @param $id
     * @param $username
     * @param $password
     * @param $last_login
     */

    public function __construct($id, $username, $password, $last_login)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->last_login = $last_login;
    }

    /**
     * User constructor.
     */




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @param mixed $last_login
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }



}