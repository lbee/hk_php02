<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/17/2016
 * Time: 7:40 PM
 */
class Album
{
    private $id;
    private $name;
    private $user_id;

    /**
     * Album constructor.
     * @param $id
     * @param $name
     * @param $user_id
     */
    public function __construct($id, $name, $user_id)
    {
        $this->id = $id;
        $this->name = $name;
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }



}