<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/17/2016
 * Time: 8:24 PM
 */
class Image
{
    private $id;
    private $album_id;
    private $photoName;
    private $photoURL;

    /**
     * Image constructor.
     * @param $id
     * @param $album_id
     * @param $photoName
     * @param $photoURL
     */
    public function __construct($id, $album_id, $photoName, $photoURL)
    {
        $this->id = $id;
        $this->album_id = $album_id;
        $this->photoName = $photoName;
        $this->photoURL = $photoURL;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @param mixed $album_id
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
    }

    /**
     * @return mixed
     */
    public function getPhotoName()
    {
        return $this->photoName;
    }

    /**
     * @param mixed $photoName
     */
    public function setPhotoName($photoName)
    {
        $this->photoName = $photoName;
    }

    /**
     * @return mixed
     */
    public function getPhotoURL()
    {
        return $this->photoURL;
    }

    /**
     * @param mixed $photoURL
     */
    public function setPhotoURL($photoURL)
    {
        $this->photoURL = $photoURL;
    }


}