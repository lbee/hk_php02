<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/11/2016
 * Time: 5:16 PM
 */
include '../../respositories/UserProfileRespository.php';
class UserProfileController
{
    public function getProfile(){
        $userprofileRespository = new UserProfileRespository();
        $userprofile =  $userprofileRespository->getProfile($_SESSION['id']);
        include ('../../views/users/profile.php');
    }
}