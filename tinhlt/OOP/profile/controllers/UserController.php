<?php
/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/10/2016
 * Time: 6:59 PM
 */
include ('../../respositories/UserRespository.php');
include ('../../respositories/UserProfileRespository.php');

class UserController{
    private $userRespository;
    private $userProfileRespository;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->userRespository = new UserRepository();
        $this->userProfileRespository = new UserProfileRespository();
    }

    public function actionLogin(){
        /**
         * check giá trị nhập vào
         */
        if(isset($_SESSION['id'])){
            header('Location:profile.php');
        }else{
            if(isset($_POST['signin'])){
                $username = $_POST['username'];
                $password = $_POST['password'];

                $user = $this->userRespository->getUser($username,$password);
                if($user != null){
                    $_SESSION['id'] = $user->getId();

                    header("Location:./profile.php");
                }else{
                    $error = "Thông tin vừa nhập không đúng. Vui lòng nhập lại";
                }
            }

            include ('../../views/users/login.php');
        }
    }

    public function actionRegister(){
        if(isset($_POST['submit'])){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fname = $_POST['fname'];
            $address = $_POST['address'];
            $dob = $_POST['dob'];
            $gender =$_POST['gender'];
            $phone = $_POST['phone'];
            /*Upload avatar*/
            $avatar = $_FILES['avatar'];
            $avatar_name = time().'_'.$avatar['name'];
            move_uploaded_file($avatar['tmp_name'],"../../upload/$avatar_name");

            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $last_login = date("Y-m-d h:i:s");
            $user = new User(0,$username,$password,$last_login);

            /*add user and get id_user */

            $user_id = $this->userRespository->addUser($user);

            /*add profile of user*/
            $userprofile = new UserProfile($user_id,$fname,$dob,$address,$gender,$phone,$avatar_name);

            $this->userProfileRespository->addProfile($userprofile);
            header("Location:login.php");
        }
        include ('../../views/users/register.php');
    }

    public function actionLogout(){
        session_unset('id');
        header('Location:login.php');
    }

    public function actionEdit(){
        if(isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            $user = $this->userRespository->getUserById($id);
            $userprofile = $this->userProfileRespository->getProfile($id);
        }

        if(isset($_POST['submit'])){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fname = $_POST['fname'];
            $address = $_POST['address'];
            $dob = $_POST['dob'];
            $gender =$_POST['gender'];
            $phone = $_POST['phone'];

            /*Delete avatar in upload foder*/
            if($_FILES['avatar']['size']>0&&!$_FILES['avatar']['error']){
                if(is_file('../../upload/'.$userprofile->getAvatar())){
                    unlink('../../upload/'.$userprofile->getAvatar());
                }

                /*Upload avatar*/
                $avatar = $_FILES['avatar'];
                $avatar_name = time().'_'.$avatar['name'];
                move_uploaded_file($avatar['tmp_name'],"../../upload/$avatar_name");
            }else{
                $avatar_name = $_POST['avatar_hidden'];
            }


            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $last_login = date("Y-m-d h:i:s");
            $user = new User($_SESSION['id'],$username,$password,$last_login);
            $userprofile = new UserProfile($_SESSION['id'],$fname,$dob,$address,$gender,$phone,$avatar_name);
            $this->userRespository->updateUser($user);
            $this->userProfileRespository->updateProfile($userprofile);
            header('location:profile.php');
        }
        include('../../views/users/edit.php');
    }
}