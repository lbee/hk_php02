<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/17/2016
 * Time: 7:13 PM
 */
include "../../respositories/AlbumRespository.php";

class AlbumController
{
    private  $albumRespository;

    /**
     * AlbumController constructor.
     * @param $albumRespository
     */
    public function __construct()
    {
        $this->albumRespository = new AlbumRespository();
    }


    public function actionIndex(){
        $listAlbum = $this->getListAlbum();
        include '../../views/album/index.php';
    }

    public function actionAddAlbum(){
        $albumName = $_POST['albumName'];
        $user_id = $_SESSION['id'];
        $album = new Album(0,$albumName,$user_id);
        $this->albumRespository->addAlbum($album);
        header("location:index.php");
    }

    public function getListAlbum(){
        return $this->albumRespository->getListAlbum();
    }

    public function getListImages(){
        $album_id = $_GET['album_id'];
        $listImage = $this->albumRespository->getListImages($album_id);
        include '../../views/album/listImages.php';
    }

    public function addPhoto(){
        $album_id = $_POST['album_id'];
        /*upload photo*/
        $photo = $_FILES['photoUpload'];
        $photo_name = time().'_'.$photo['name'];
        move_uploaded_file($photo['tmp_name'],'../../upload/album/'.$photo_name);

        /*store database*/
        $image = new Image(0,$album_id,$photo_name,'../../upload/album/'.$photo_name);
        $this->albumRespository->addPhoto($image);
        header("location:listImage.php?album_id=$album_id");
    }

    public function editAlbum(){
        $id_album = $_GET['id'];
        $album_name = $_POST['album_name'];
        $album_name_hidden = $_POST['name_hidden'];
        if(!empty($album_name)){
            $album = new Album($id_album,$album_name,0);
        }else{
            $album = new Album($id_album,$album_name_hidden,0);
        }
        $this->albumRespository->editAlbum($album);
        header("location:index.php");
    }
}