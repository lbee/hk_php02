<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/11/2016
 * Time: 4:59 PM
 */
include '../../models/UserProfile.php';
class UserProfileRespository
{
    private $connection;

    public function __construct()
    {
        $this->connect();
    }

    public function connect(){
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $db = 'profile';
        $this->connection = mysqli_connect($host, $username, $password,$db);
        mysqli_set_charset($this->connection , "utf8");
    }

    public function getProfile($id){
        $sql = "SELECT * FROM tbl_profile WHERE user_id ='$id'";
        $result = mysqli_query($this->connection,$sql);
        $row = $result->fetch_row();
        $userprofile = new UserProfile($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6]);
        if($result->num_rows>0){
            return $userprofile;
        }
        return null;
    }

    public function addProfile(UserProfile $profile){
        $user_id = $profile->getUserId();
        $fname = $profile->getFullname();
        $dob = date('Y-m-d',strtotime($profile->getDob()));
        $address = $profile->getAddress();
        $gender = $profile->getGender();
        $phone = $profile->getPhone();
        $avatar = $profile->getAvatar();
        $sql =  "INSERT INTO tbl_profile(user_id,FullName,DOB,Address,Gender,Phone,Avatar) VALUES('$user_id','$fname','$dob','$address','$gender','$phone','$avatar')";
        mysqli_query($this->connection,$sql);
    }

    public function updateProfile(UserProfile $profile){
        $user_id = $profile->getUserId();
        $fname = $profile->getFullname();
        $dob = date('Y-m-d',strtotime($profile->getDob()));
        $address = $profile->getAddress();
        $gender = $profile->getGender();
        $phone = $profile->getPhone();
        $avatar = $profile->getAvatar();
        $sql =  "UPDATE  tbl_profile SET user_id='$user_id',FullName='$fname',DOB='$dob',Address='$address',Gender='$gender',Phone='$phone',Avatar='$avatar' WHERE user_id='$user_id'";
        mysqli_query($this->connection,$sql);
    }
}