<?php

/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/17/2016
 * Time: 7:41 PM
 */
include '../../models/Album.php';
include '../../models/Image.php';
class AlbumRespository
{
    private $connection;

    public function __construct()
    {
        $this->connect();
    }

    public function connect(){
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $db = 'profile';
        $this->connection = mysqli_connect($host, $username, $password,$db);
        mysqli_set_charset($this->connection , "utf8");
    }

    public function addAlbum(Album $album){
        $albumName = $album->getName();
        $user_id = $album->getUserId();
        $sql = "INSERT INTO tbl_album(name,user_id) VALUES ('$albumName','$user_id')";
        mysqli_query($this->connection,$sql);
    }

    public function getListAlbum(){
        $sql = "SELECT * FROM tbl_album";
        $result = mysqli_query($this->connection,$sql);
        $list = [];
        while($row = mysqli_fetch_assoc($result)){
            $album = new Album($row['id'],$row['name'],$row['user_id']);
            array_push($list,$album);
        }
        return $list;
    }

    public function getListImages($album_id){
        $sql = "SELECT * FROM tbl_photo WHERE album_id = $album_id";
        $result = mysqli_query($this->connection,$sql);
        $listImage = [];
        while($row = mysqli_fetch_assoc($result)){
            $image = new Image($row['id'],$row['album_id'],$row['photoName'],$row['photoURL']);
            array_push($listImage,$image);
        }
        return $listImage;
    }

    public function addPhoto(Image $image){
        $album_id = $image->getAlbumId();
        $photoName = $image->getPhotoName();
        $photoURL = $image->getPhotoURL();
        $sql = "INSERT INTO tbl_photo(album_id,photoName,photoURL) VALUES ('$album_id','$photoName','$photoURL')";
        mysqli_query($this->connection,$sql);
    }

    public function editAlbum($album){
        $album_id = $album->getId();
        $album_name = $album->getName();
        $sql = "UPDATE tbl_album SET name='$album_name'  WHERE id='$album_id' ";
        mysqli_query($this->connection,$sql);
    }
}