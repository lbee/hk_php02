<?php
/**
 * Created by PhpStorm.
 * User: Le The Tinh
 * Date: 10/10/2016
 * Time: 7:00 PM
 */
include '../../models/User.php';
class UserRepository{

    private $connection;

    public function __construct()
    {
        $this->connect();
    }

    public function connect(){
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $db = 'profile';
        $this->connection = mysqli_connect($host, $username, $password,$db);
        mysqli_set_charset($this->connection , "utf8");
    }

    public function getUser($username, $password){
        $password_hash = md5($password);
        $sql = "SELECT * FROM tbl_user WHERE username = '$username' AND password = '$password_hash'";
        $row = mysqli_query($this->connection,$sql)->fetch_row();
        $user = new User($row['0'],$row['1'],$row['2'],$row['3']);
        if($row != null){
            return $user;
        }
        return null;
    }

    public function getUserById($id){
        $sql = "SELECT * FROM tbl_user WHERE id='$id'";
        $row = mysqli_query($this->connection,$sql)->fetch_row();
        $user = new User($row['0'],$row['1'],$row['2'],$row['3']);
        if($row != null){
            return $user;
        }
        return null;
    }

    public function addUser(User $user){
        $username = $user->getUsername();
        $password_hash =  md5($user->getPassword());
        $last_login = $user->getLastLogin();
        $sql = "INSERT INTO tbl_user(username,password,last_login)  VALUES ('$username','$password_hash','$last_login')";
        mysqli_query($this->connection,$sql);
        return mysqli_insert_id($this->connection);
    }

    public function updateUser(User $user){
        $id= $user->getId();
        $username = $user->getUsername();
        $password_hash =  md5($user->getPassword());
        $last_login = $user->getLastLogin();
        $sql = "UPDATE tbl_user SET  username='$username',password='$password_hash',last_login='$last_login' WHERE id='$id'";
        mysqli_query($this->connection,$sql);
    }

}