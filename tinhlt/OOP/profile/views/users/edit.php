<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
    <link rel="stylesheet"
          href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
</head>
<body>
<div class="container">
    <form action="#" class="form-signin" method="POST" enctype="multipart/form-data">
        <h2 class="form-signin-heading">Edit User</h2>
        <!-- Username -->
        <div class="form-group">
            <label for="inputUsername">Username</label>
            <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
                   name="username" value="<?php echo isset($username)?$username:$user->getUsername() ?>"/>
        </div>
        <!-- Password -->
        <div class="form-group">
            <label for="inputPassword">Password</label>
            <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
                   name="password"/>
        </div>

        <div class="form-group">
            <label for="inputFName">FullName</label>
            <input type="text" required placeholder="Full name" class="form-control" id="inputFName"
                   name="fname" value="<?php echo isset($fname)?$fname:$userprofile->getFullname() ?>"/>
        </div>

        <div class="form-group">
            <label for="add">Address</label>
            <textarea type="text" required placeholder="Full name" class="form-control" id="add"
                      name="address"><?php echo isset($addresss)?$addresss:$userprofile->getAddress() ?>
                </textarea>
        </div>

        <div class="form-group">
            <label for="day">DOB</label>
            <!-- Day -->
            <input type="text" class="form-control" name="dob" id="datepicker" placeholder="ChooseDOB"
                   data-date-format="dd-mm-yyyy" value="<?php echo isset($dob)?$dob:$userprofile->getDob() ?>"/>
        </div>

        <div class="form-group">
            <label for="gender">Gender</label>
            <label class="radio-inline">
                <input type="radio" name="gender" value="1"
                    <?php
                    if(isset($gender)&&$gender==1){
                        echo 'checked';
                    }else{
                        if($userprofile->getGender()==1){
                            echo 'checked';
                        }
                    }
                ?> > Nam
            </label>
            <label class="radio-inline">
                <input type="radio" name="gender" value="0"
                    <?php
                    if(isset($gender)&&$gender==0){
                        echo 'checked';
                    }else{
                        if($userprofile->getGender()==0){
                            echo 'checked';
                        }
                    }
                    ?>
                > Nữ
            </label>
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" required placeholder="PhoneNum" class="form-control" id="phone"
                   name="phone" value="<?php echo isset($phone)?$phone:$userprofile->getPhone() ?>"/>
        </div>

        <!-- Avatar-->
        <div class="form-group">
            <label for="avt">Avatar</label>
            <input type="hidden" value="<?php echo $userprofile->getAvatar() ?>" name="avatar_hidden">
            <input type="file" class="form-control" id="avt" name="avatar" accept="image/*"/>
        </div>
        <!-- Submit btn -->
        <button type="submit" class="btn btn-lg btn-primary" name="submit">Đăng kí</button>
        <!-- Nút quay lại-->
        <button type="button" class="btn btn-lg btn-default" onclick="history.go(-1)">Quay lại</button>
    </form>
</div>
<!-- JS script for datepicker-->
<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- script cho datepicker-->
<script>
    $("#datepicker").datepicker();
</script>
</body>
</html>