<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-xs-offset-3">
                <img class="img-responsive img-thumbnail" src="<?php echo '../../upload/'.$userprofile->getAvatar() ?>" alt="">
            </div>

        </div>
        <div class="row">
            <div class="col-xs-7 col-xs-offset-3">
                <label class="label-info"><?php echo $userprofile->getFullname() ?></label>
            </div>
            <div class="col-xs-7 col-xs-offset-3">
                Ngày sinh: <?php echo $userprofile->getDob() ?>
            </div>
            <div class="col-xs-7 col-xs-offset-3">
                Địa chỉ: <?php echo $userprofile->getAddress() ?>
            </div>
            <div class="col-xs-7 col-xs-offset-3">
                Giới tính: <?php echo $userprofile->getGender()==1?"Nam":"Nữ" ?>
            </div>
            <div class="col-xs-7 col-xs-offset-3">
                Số điện thoại: <?php echo $userprofile->getPhone() ?>
            </div>
        </div>
            <a href="<?php echo 'logout.php' ?>" class="btn btn-default">Logout</a>
            <a href="<?php echo '../album/index.php' ?>" class="btn btn-success">Abum</a>
            <a href="<?php echo 'edit.php' ?>" class="btn btn-danger">Edit</a>
    </div>
</body>
</html>