<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách photo thuộc album</h3>
        </div>
        <div class="panel-body">

            <div class="ui">
                <!-- Form để upload ảnh -->
                <form action="addPhoto.php" method="post" enctype="multipart/form-data">
                    Upload image:
                    <!-- Chấp nhận mọi thể loại ảnh sử dụng accept attr-->
                    <input type="file" class="ui primary button" accept="image/*" id="fileUpload" name="photoUpload"/>
                    <input type="hidden" value="<?php echo $album_id ?>" name="album_id">
                    <!-- Nút submit, bị ẩn nếu như file chưa được chọn -->
                    <button name="submit" class="ui secondary button hide submit" type="submit">
                        Save
                    </button>
                    <button class="ui button hide cancel" type="submit">
                        Cancel
                    </button>
                </form>
            </div>
            <br/>


            <?php
            foreach($listImage as $image) {
                ?>
                <a href="#">
                    <img style="width: 200px;height: 175px; float: left;" src="<?php echo $image->getPhotoURL() ?>" alt="">
                </a>
                <?php
            }
            ?>

            <!-- lấy danh sách ảnh để ở đây-->
            <div class="ui special cards three column">
                <div class="card">
                    <div class="blurring dimmable image">
                        <div class="ui inverted dimmer">
                            <div class="content">
                                <div class="center">

                                    <!-- Truyền 2 tham số (1): Photo id để xóa photo (2): album_id để quay về album-->
                                    <a href="#">
                                        <div class="negative ui button">Xóa</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <img src="<?php  ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End -->

<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../assets/components/semantic/dist/semantic.min.js"></script>
<script src="../../assets/js/dim.js"></script>
</body>
</html>