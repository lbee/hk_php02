<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/20/2016
 * Time: 12:28 PM
 */
class PostController extends AppController
{
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('posts', $this->Post->find('all'));
    }
}