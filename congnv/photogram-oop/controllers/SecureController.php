<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/15/2016
 * Time: 11:11 PM
 */
session_start();

require_once ('../../config/AppConfig.php');
require_once ('../../controllers/Controller.php');
require_once ('../../repositories/UserRepository.php');

class SecureController extends Controller
{
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * xử lý hành động đăng nhập
     */
    public function actionLogin()
    {
        // xử lý đăng nhập
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);

            // lấy ra người dùng với username & password_hash
            $user = $this->userRepository->getUser($username, $password_hash);

            // nếu người dùng tồn tại
            if ($user != null) {
                // login người dùng
                $_SESSION[AppConfig::LOGGED_IN_USER] = serialize($user);
                // chuyển người dùng sang trang profile
                return $this->redirect('profile/update.php');
            } else { // nếu người dùng không tồn tại
                // báo lỗi
                $error = 'Xin hãy kiểm tra lại tên người dùng và mật khẩu!';
            }
        }

        // hiển thị view
        include('../../views/secure/login.php');
    }

    /**
     * xử lý hành động đăng xuất
     */
    public function actionLogout() {
        session_destroy();

        // chuyển người dùng sang trang đăng nhập
        return $this->redirect('login.php');
    }
}