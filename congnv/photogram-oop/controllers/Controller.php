<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/15/2016
 * Time: 11:18 PM
 */
class Controller
{
    /**
     * chuyển người dùng sang trang với $url được cung cấp
     * @param $url
     */
    public function redirect($url) {
        // nếu url chứa / = chuyển trang khác controller -> thêm ../ để đi ra folder /routes
        if( strpos($url, '/') !== false )
            $url = "../$url";

        header("Location: $url");
    }
}