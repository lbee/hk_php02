<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/5/2016
 * Time: 8:28 PM
 */
require_once('../../controllers/Controller.php');
require_once('../../repositories/UserRepository.php');
require_once('../../repositories/ProfileRepository.php');
require_once('../../models/User.php');
require_once('../../helpers/UploadHelpers.php');

// khai báo sử dụng session
session_start();

class UserController extends Controller
{
    private $userRepository;
    private $profileRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->profileRepository = new ProfileRepository();
    }

    // xử lý hành động đăng ký
    public function actionRegister()
    {
        // xử lý dữ liệu gửi tới
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fullname = $_POST['fname'];
            $address = $_POST['address'];
            // format datepicker thành dạng Y-m-d giống với CSDL
            $dob = DateTime::createFromFormat('d-m-Y', $_POST['dob'])->format('Y-m-d');
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            $file = $_FILES['avatar'];

            // tạo người dùng
            $user = new User();
            $user->setUsername($username);
            $user->setPassword($password);

            // TODO: kiểm tra username tồn tại
            // lưu người dùng vào csdl & cập nhật id tự sinh cho $user
            $userCreated = $this->userRepository->create($user);

            // nếu tạo người dùng thành công
            if ($userCreated) {
                // tạo profile
                $profile = new Profile();
                $profile->setUserId($user->getId());
                $profile->setFullName($fullname);
                $profile->setAddress($address);
                $profile->setDob($dob);
                $profile->setGender($gender);
                $profile->setPhone($phone);

                // upload file & lấy về đường dẫn tới file đã upload
                $avatar = UploadHelpers::uploadImage($file, AppConfig::PATH_AVATAR."$username/");
                if ($avatar !== false) { // nếu upload thành công
                    $profile->setAvatar($avatar);
                }

                // lưu profile vào csdl
                $profileCreated = $this->profileRepository->create($profile);

                // TODO: áp dụng transaction để đảm bảo nếu tạo profile không thành công thì không tạo user
                // nếu tạo profile thành công
                if ($profileCreated) {
                    // chuyển người dùng sang trang đăng nhập
                    return $this->redirect('secure/login.php');
                } else {
                    // xóa bỏ người dùng đã tạo trước đó
                    $this->userRepository->delete($user->getId());

                    // nếu có avatar ->xóa ảnh đã upload
                    if ($avatar !== false) {
                        unlink($avatar);
                    }

                    // báo lỗi
                    $error = 'Lỗi không thể tạo profile người dùng, vui lòng thử lại.';
                }
            } else { // nếu không thành công
                // báo lỗi
                $error = 'Lỗi. Đăng ký người dùng thất bại, vui lòng thử lại.';
            }
        }

        // hiển thị view
        include('../../views/user/register.php');
    }
}