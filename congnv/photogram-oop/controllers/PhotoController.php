<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/18/2016
 * Time: 12:13 AM
 */
require_once ('../../controllers/SecuredController.php');
require_once ('../../repositories/AlbumRepository.php');
require_once ('../../repositories/PhotoRepository.php');
require_once ('../../helpers/UploadHelpers.php');
require_once ('../../config/AppConfig.php');

class PhotoController extends SecuredController
{
    private $photoRepository;

    public function __construct()
    {
        // chú ý gọi hàm khởi tạo của lớp cha
        parent::__construct();
        $this->photoRepository = new PhotoRepository();
    }

    /**
     * hiển thị danh sách ảnh trong một album
     */
    public function actionIndex() {
        // nếu không tồn tại album_id trên đường dẫn
        if (!isset($_GET['album_id']))
            return $this->redirect('album/index.php');

        $album_id = $_GET['album_id'];

        $albumRepository = new AlbumRepository();
        $album = $albumRepository->getById($album_id);

        // nếu album_id không đúng
        if ($album == null)
            return $this->redirect('album/index.php');

        $photos = $this->photoRepository->getByAlbumId($album_id);

        // view
        include('../../views/photo/index.php');
    }

    /**
     * xử lý hành động upload ảnh vào album
     */
    public function actionUpload() {
        // nếu không có album_id trên đường dẫn
        if (!isset($_GET['album_id']))
            return $this->redirect('album/index.php');

        $album_id = $_GET['album_id'];
        if (isset($_POST['submit'])) {
            $file = $_FILES['photoUpload'];

            // upload file và trả về đường dẫn file sau upload
            $photoURL = UploadHelpers::uploadImage($file, AppConfig::PATH_PHOTO);

            if ($photoURL !== false) { // upload thành công
                // tạo photo
                $photo = new Photo();
                $photo->setAlbumId($album_id);
                $photo->setPhotoName($file['name']);
                $photo->setPhotoUrl($photoURL);

                // lưu vào csdl và cập nhật id tự sinh cho $photo
                $created = $this->photoRepository->create($photo);
                if (!$created) {
                    $error = 'Upload file không thành công. vui lòng thử lại.';
                }
            }
        }

        // chuyển sang trang dsach ảnh theo album
        return $this->redirect("photo/index.php?album_id=$album_id");
    }

    /**
     * xử lý hành động xóa 01 ảnh
     */
    public function actionDelete() {
        // nếu id ảnh không xuất hiện trên thanh tiêu đề
        if (!isset($_GET['id']))
            return $this->redirect('album/index.php');

        $id = $_GET['id'];
        $photo = $this->photoRepository->getByID($id);

        // nếu id ảnh không đúng
        if ($photo == null)
            return $this->redirect('album/index.php');

        // xóa bản ghi photo
        $deleted = $this->photoRepository->delete($photo);
        if ($deleted) { // nếu xóa thành công
            // xóa file ảnh thực trên server
            unlink($photo->getPhotoUrl());
        } else {
            // báo lỗi
            $error = 'Xóa photo không thành công. vui lòng thử lại.';
        }

        // chuyển sang trang dsach photo trong album
        return $this->redirect('photo/index.php?album_id='.$photo->getAlbumId());
    }

    /**
     * xử lý hành động xem chi tiết 01 ảnh
     */
    public function actionView() {
        // nếu id ảnh không xuất hiện trên thanh tiêu đề
        if (!isset($_GET['id']))
            return $this->redirect('album/index.php');

        $id = $_GET['id'];
        $photo = $this->photoRepository->getById($id);
        // nếu id ảnh không đúng

        if ($photo == null)
            return $this->redirect('album/index.php');

        // view
        include ('../../views/photo/view.php');
    }
}