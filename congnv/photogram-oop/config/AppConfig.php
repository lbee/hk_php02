<?php

/**
 * phương án 01: lưu giữ thông tin cấu hình cho ứng dụng web sử dụng class Config với các hằng số (const)
 * ví dụ sử dụng: AppConfig::DB_HOST để lấy ra thông tin host cho database
 */
class AppConfig
{
    // cấu hình database
    const DB_HOST = 'localhost';
    const DB_USERNAME = 'root';
    const DB_PASSWORD = '';
    const DB_NAME = 'hk_photogram-oop';

    // thông tin session
    const LOGGED_IN_USER = '_user';

    // cấu hình website
    const PATH_AVATAR = '../../uploads/imgavatar/';
    const PATH_PHOTO = '../../uploads/img/';
}