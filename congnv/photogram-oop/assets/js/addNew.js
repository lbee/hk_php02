/**
 * Created by camnh on 7/21/2016.
 */
$(function () {
    /**
     * Hàm để toggle giữa thêm mới và bỏ qua trong trang album
     */
    $(".addNew button").click(function(){
        //chưa có class ignore thì nút đó là thêm mới
        if(!$(this).hasClass("ignore")){
            $(this).text("Bỏ qua");
            $(this).addClass("ignore");
            $('#addNewForm').css("display","block");

        }
        //Nếu có class ignore thì nút đó là bỏ qua
        else{
            $(this).text("Thêm mới");
            //nếu click nút thêm mới thì thêm class ignore
            $(this).removeClass("ignore");
            //xóa chữ trong input tên album
            $('#addNewForm input').val("");
            //ẩn phần nội dung của album
            $('#addNewForm').css("display","none");
        }
    });

    function getAlbum(){

    }
});