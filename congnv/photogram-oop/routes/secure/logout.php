<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/15/2016
 * Time: 11:16 PM
 */
require_once('../../controllers/SecureController.php');

// khởi tạo controller
$controller = new SecureController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionLogout();