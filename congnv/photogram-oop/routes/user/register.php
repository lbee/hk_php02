<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/12/2016
 * Time: 5:20 PM
 */
require_once ('../../controllers/UserController.php');

// khởi tạo controller
$controller = new UserController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionRegister();