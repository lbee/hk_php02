<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/18/2016
 * Time: 12:42 AM
 */
require_once('../../controllers/PhotoController.php');

// khởi tạo controller
$controller = new PhotoController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionUpload();