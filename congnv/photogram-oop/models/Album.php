<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/16/2016
 * Time: 2:50 PM
 */
class Album
{
    private $id;
    private $name;
    private $user_id;

    // thuộc tính phát sinh để hiển thị ảnh đầu tiên trong album
    private $first_photo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getFirstPhoto()
    {
        return $this->first_photo;
    }

    /**
     * @param mixed $first_photo
     */
    public function setFirstPhoto($first_photo)
    {
        $this->first_photo = $first_photo;
    }
}