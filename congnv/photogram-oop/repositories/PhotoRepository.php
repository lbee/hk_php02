<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/18/2016
 * Time: 12:18 AM
 */
require_once ('../../repositories/Repository.php');
require_once ('../../models/Photo.php');

class PhotoRepository extends Repository
{
    /**
     * @return trả về danh sách các photo trong album vs $album_id đã cho
     */
    public function getByAlbumId($album_id) {
        $sql = "SELECT * FROM tbl_photo WHERE album_id = $album_id";
        $result = $this->connection->query($sql);
        $rows = $result->fetchAll();

        $photos = [];
        foreach ($rows as $row) {
            $photo = $this->rowToModel($row);
            $photos[] = $photo;
        }

        return $photos;
    }

    /**
     * @return trả về ảnh đầu tiên trong album
     *  trả về null nếu album không chứa ảnh
     * @param $album_id
     */
    public function getFirstPhoto($album_id) {
        $sql = "SELECT * FROM tbl_photo WHERE album_id = $album_id LIMIT 1";
        $result = $this->connection->query($sql);
        $row = $result->fetch();

        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * @return trả về object photo vs id đã cho
     *  trả về null nếu id không đúng
     * @param $id
     */
    public function getById($id) {
        $sql = "SELECT * FROM tbl_photo WHERE id = $id";
        $result = $this->connection->query($sql);
        $row = $result->fetch();

        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng dữ liệu $row sang object photo
     * @return object photo từ mảng dữ liệu $row
     * @param $row
     */
    public function rowToModel($row) {
        $album = new Photo();
        $album->setId($row['id']);
        $album->setAlbumId($row['album_id']);
        $album->setPhotoName($row['photoName']);
        $album->setPhotoUrl($row['photoURL']);

        return $album;
    }

    /**
     * lưu photo vào csdl & cập nhật id tự sinh cho object $photo
     * @return trả về true nếu thành công và ngược lại
     * @param $photo
     */
    public function create($photo) {
        $album_id = $photo->getAlbumId();
        $photoName = $photo->getPhotoName();
        $photoURL = $photo->getPhotoURL();

        $sql = "INSERT INTO tbl_photo (album_id, photoName, photoURL)
                  VALUES ('$album_id', '$photoName', '$photoURL')";
        $result = $this->connection->query($sql);
        if ($result) {
            // cập nhật id tự sinh cho $album
            $photo->setId($this->connection->lastInsertId());

            return true;
        }

        return false;
    }

    /**
     * xoá ảnh trong csdl
     * @return true nếu thành công và ngược lại
     */
    public function delete($photo) {
        $id = $photo->getId();

        $sql = "DELETE FROM tbl_photo WHERE id=$id";
        $result = $this->connection->query($sql);

        return $result ? true : false;
    }
}