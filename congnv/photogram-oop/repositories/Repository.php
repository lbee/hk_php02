<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/7/2016
 * Time: 6:36 PM
 */
require_once('../../config/AppConfig.php');

class Repository {
    protected $connection;

    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect() {

        $host = AppConfig::DB_HOST;
        $username = AppConfig::DB_USERNAME;
        $password = AppConfig::DB_PASSWORD;
        $dbname = AppConfig::DB_NAME;

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            die("Connection failed: " . $e->getMessage());
        }
    }

    public  function close() {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }
}