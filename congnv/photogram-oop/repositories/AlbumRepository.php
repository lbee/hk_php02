<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/16/2016
 * Time: 2:50 PM
 */
require_once ('../../repositories/Repository.php');
require_once ('../../models/Album.php');

class AlbumRepository extends Repository
{
    /**
     * trả về danh sách các object album của $user_id
     * @param $user_id
     * @return array
     */
    public function getByUserId($user_id) {
        $sql = "SELECT * FROM tbl_album WHERE user_id=$user_id";
        $result = $this->connection->query($sql);
        $rows = $result->fetchAll();

        $albums = [];
        foreach ($rows as $row) {
            $album = $this->rowToModel($row);
            $albums[] = $album;
        }

        return $albums;
    }

    /**
     * @return trả về album vs $id đã cho
     *  trả về null nếu $id không đúng
     * @param $id
     * @return Album|null
     */
    public function getById($id) {
        $sql = "SELECT * FROM tbl_album WHERE id = $id";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng dữ liệu $row sang object album
     * @return object album từ mảng dữ liệu $row
     * @param $row
     */
    public function rowToModel($row) {
        $album = new Album();
        $album->setId($row['id']);
        $album->setName($row['name']);
        $album->setUserId($row['user_id']);

        return $album;
    }

    /**
     * lưu $album vào cơ sở dữ liệu và cập nhật id tự sinh cho $album
     * @return trả về true nếu thành công và ngược lại
     * @param $album
     */
    public function create($album) {
        $name = $album->getName();
        $user_id = $album->getUserId();

        $sql = "INSERT INTO tbl_album (name, user_id)
                  VALUES ('$name', '$user_id')";
        $result = $this->connection->query($sql);
        if ($result) {
            // cập nhật id tự sinh cho $album
            $album->setId($this->connection->lastInsertId());

            return true;
        }

        return false;
    }

    /**
     * cập nhật $album đã cho vào csdl
     * @return trả về true nếu thành công và ngược lại
     * @param $album
     */
    public function update($album) {
        $id = $album->getId();
        $name = $album->getName();
        $sql = "UPDATE tbl_album SET name = '$name' WHERE id=$id";
        $result = $this->connection->query($sql);

        return $result ? true : false;
    }

    /**
     * xóa album đã cho
     * @return trả về true nếu thành công và ngược lại
     * @param $album
     */
    public function delete($album) {
        $id = $album->getId();
        $sql = "DELETE FROM tbl_album WHERE id=$id";
        $result = $this->connection->query($sql);

        return $result ? true : false;
    }
}