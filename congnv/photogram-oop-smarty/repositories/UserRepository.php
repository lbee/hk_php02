<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:23 PM
 */
require_once('../../repositories/Repository.php');
require_once('../../models/User.php');

class UserRepository extends Repository {

    /**
     * @return trả về người dùng vs $username & $password
     *  trả về null nếu không tồn tại
     * @param $username
     * @param $password hashed_password
     */
    public function getUser($username, $password) {
        $sql = "SELECT * FROM tbl_user WHERE username='$username' AND password='$password'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();

        // nếu tồn tại ng dùng
        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng dữ liệu $row sang object user
     * @return object user từ mảng dữ liệu $row
     * @param $row
     */
    public function rowToModel($row) {
        $user = new User();

        $user->setId($row['id']);
        $user->setUsername($row['username']);
        $user->setPassword($row['password']);
        $user->setLastLogin($row['last_login']);

        return $user;
    }

    /**
     * lưu object user vào cơ sở dữ liệu & cập nhật id tự sinh cho object user
     * @return trả về true nếu thêm mới thành công và ngược lại
     * @param $user User
     */
    public function create($user) {
        $username = $user->getUsername();
        $password_hash = md5($user->getPassword());
        $sql = "INSERT INTO tbl_user(username, password) VALUES ('$username', '$password_hash')";
        $result = $this->connection->query($sql);

        if ($result) {
            // cập nhật id tự sinh cho object user
            $user->setId($this->connection->lastInsertId());

            return true;
        }

        return false;
    }

    /**
     * xóa người dùng với id đã cho
     * @return trả về true nếu xóa thành công và ngược lại
     * @param $id
     */
    public function delete($id) {
        $sql = "DELETE FROM tbl_user WHERE id = $id";
        $result = $this->connection->query($sql);

        return $result ? true: false;
    }
}