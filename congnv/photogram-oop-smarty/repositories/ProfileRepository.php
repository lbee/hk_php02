<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/16/2016
 * Time: 1:01 AM
 */
require_once('../../repositories/Repository.php');
require_once('../../models/Profile.php');

class ProfileRepository extends Repository
{
    /**
     * lưu object profile vào cơ sở dữ liệu
     * @return trả về true nếu thêm mới thành công và ngược lại
     * @param $profile Profile
     */
    public function create($profile) {
        $user_id = $profile->getUserId();
        $fullname = $profile->getFullName();
        $dob = $profile->getDob();
        $address = $profile->getAddress();
        $gender = $profile->getGender();
        $phone = $profile->getPhone();
        $avatar = $profile->getAvatar();

        $sql = "INSERT INTO tbl_profile (user_id, FullName, DOB, Address, Gender, Phone, Avatar)
                  VALUES ($user_id, '$fullname', '$dob', '$address', '$gender', '$phone', '$avatar')";

        $result = $this->connection->query($sql);
        return $result ? true : false;
    }

    /**
     * cập nhật profile trong cơ sở dữ liệu
     * @return trả về true nếu cập nhật thành công và ngược lại
     * @param $profile
     */
    public function update($profile) {
        $user_id = $profile->getUserId();
        $fullname = $profile->getFullName();
        $dob = $profile->getDob();
        $address = $profile->getAddress();
        $gender = $profile->getGender();
        $phone = $profile->getPhone();
        $avatar = $profile->getAvatar();

        $sql = "UPDATE tbl_profile
                  SET FullName = '$fullname', DOB = '$dob', Address = '$address',
                      Gender = '$gender', Phone = '$phone', Avatar = '$avatar'
                  WHERE user_id = $user_id";

        $result = $this->connection->query($sql);
        return $result ? true : false;
    }

    /**
     * @param $user_id
     * @return null
     */
    public function getByUserId($user_id) {
        $sql = "SELECT * FROM tbl_profile WHERE user_id=$user_id";
        $row =  $this->connection->query($sql)->fetch();

        // nếu tồn tại profile
        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng dữ liệu $row sang object profile
     * @return object profile từ mảng dữ liệu $row
     * @param $row
     */
    public function rowToModel($row) {
        $profile = new Profile();
        $profile->setUserId($row['user_id']);
        $profile->setFullName($row['FullName']);
        $profile->setDob($row['DOB']);
        $profile->setAddress($row['Address']);
        $profile->setGender($row['Gender']);
        $profile->setPhone($row['Phone']);
        $profile->setAvatar($row['Avatar']);

        return $profile;
    }
}