<?php
/**
 * phương án 02: lưu giữ thông tin cấu hình cho ứng dụng web sử dụng mảng
 * ví dụ sử dụng:
 *  $config = include(path_to_main.php);
 *  $config['db']['host']: để lấy ra thông tin host cho database
 */
return [
    'db' => [
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'hk_photogram-oop',
    ],
    'session' => [
        'logged_in_user' => '_user'
    ]
];
