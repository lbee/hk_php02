<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
</head>
<body>
<div class="container">
    <form action="" class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        {if isset($error)}
            <p class="alert alert-danger">{$error}</p>
        {/if}
        <!-- Username -->
        <label class="sr-only" for="inputUsername">Username</label>
        <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
               name="username">
        <!-- Password -->
        <br/>
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
               name="password">
        <br/>
        <!-- Submit btn -->
        <p><a href="../user/register.php">Chưa có tài khoản, đăng kí ở đây</a></p>
        <br/>
        <button type="submit" name="signin" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>
</div>
</body>
</html>