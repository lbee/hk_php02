<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
    <link rel="stylesheet"
          href="../../assets/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
</head>
<body>
<div class="container">
    <form action="#" class="form-signin" method="POST" enctype="multipart/form-data">
        <h2 class="form-signin-heading">Please sign up</h2>
        <!-- Username -->
        <div class="form-group">
            <label for="inputUsername">Username</label>
            <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
                   name="username"/>
        </div>
        <!-- Password -->
        <div class="form-group">
            <label for="inputPassword">Password</label>
            <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
                   name="password"/>
        </div>

        <div class="form-group">
            <label for="inputFName">FullName</label>
            <input type="text" required placeholder="Full name" class="form-control" id="inputFName"
                   name="fname"/>
        </div>

        <div class="form-group">
            <label for="add">Address</label>
                <textarea type="text" required placeholder="Full name" class="form-control" id="add"
                          name="address">
                </textarea>
        </div>

        <div class="form-group">
            <label for="day">DOB</label>
            <!-- Day -->
            <input type="text" class="form-control" name="dob" id="datepicker" placeholder="ChooseDOB"
                   data-date-format="dd-mm-yyyy"/>
        </div>

        <div class="form-group">
            <label for="gender">Gender</label>
            <label class="radio-inline">
                <input type="radio" name="gender" value="1" checked> Nam
            </label>
            <label class="radio-inline">
                <input type="radio" name="gender" value="0"> Nữ
            </label>
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" required placeholder="PhoneNum" class="form-control" id="phone"
                   name="phone"/>
        </div>

        <!-- Avatar-->
        <div class="form-group">
            <label for="avt">Avatar</label>
            <input type="file" class="form-control" id="avt" name="avatar" ACCEPT="image/*"/>
        </div>
        <!-- Submit btn -->
        <button type="submit" class="btn btn-lg btn-primary" name="submit">Đăng kí</button>
        <!-- Nút quay lại-->
        <button type="button" class="btn btn-lg btn-default" onclick="history.go(-1)">Quay lại</button>
    </form>
</div>
<!-- JS script for datepicker-->
<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- script cho datepicker-->
<script>
    $("#datepicker").datepicker();
</script>
</body>
</html>