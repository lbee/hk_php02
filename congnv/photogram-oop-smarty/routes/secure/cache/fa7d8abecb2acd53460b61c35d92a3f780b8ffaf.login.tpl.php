<?php
/* Smarty version 3.1.31-dev/38, created on 2016-10-21 12:38:48
  from "E:\xampp\htdocs\hk_php02\congnv\photogram-oop\views\secure\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31-dev/38',
  'unifunc' => 'content_5809f0381588d9_89641224',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '928e2cb936bf62960c061ef65db768a850e2c101' => 
    array (
      0 => 'E:\\xampp\\htdocs\\hk_php02\\congnv\\photogram-oop\\views\\secure\\login.tpl',
      1 => 1477046271,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 120,
),true)) {
function content_5809f0381588d9_89641224 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/css/style.css"/>
</head>
<body>
<div class="container">
    <form action="" class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
                    <p class="alert alert-danger">Xin hãy kiểm tra lại tên người dùng và mật khẩu!</p>
                <!-- Username -->
        <label class="sr-only" for="inputUsername">Username</label>
        <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
               name="username">
        <!-- Password -->
        <br/>
        <label class="sr-only" for="inputPassword">Password</label>
        <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
               name="password">
        <br/>
        <!-- Submit btn -->
        <p><a href="../user/register.php">Chưa có tài khoản, đăng kí ở đây</a></p>
        <br/>
        <button type="submit" name="signin" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>
</div>
</body>
</html><?php }
}
