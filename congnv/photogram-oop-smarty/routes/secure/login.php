<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/5/2016
 * Time: 8:29 PM
 */
require_once('../../controllers/SecureController.php');

// khởi tạo controller
$controller = new SecureController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionLogin();