<?php
/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/16/2016
 * Time: 12:37 AM
 */
require_once('../../controllers/ProfileController.php');

// khởi tạo controller
$controller = new ProfileController();
// gọi đến phương thức xử lý action (hành động) tương ứng
$controller->actionUpdate();