<?php

/**
 * Created by PhpStorm.
 * User: congnv
 * Date: 10/16/2016
 * Time: 2:22 PM
 */
require_once ('../../controllers/SecuredController.php');
require_once ('../../repositories/AlbumRepository.php');
require_once ('../../repositories/PhotoRepository.php');

class AlbumController extends SecuredController
{
    private $albumRepository;

    public function __construct()
    {
        parent::__construct();
        $this->albumRepository = new AlbumRepository();
    }

    /**
     * xử lý hành động xem danh sách các albums
     */
    public function actionIndex() {
        $albums = $this->albumRepository->getByUserId($this->loggedInUser->getId());

        // lấy ra ảnh đầu tiên cho mỗi album
        $photoRepository = new PhotoRepository();
        foreach ($albums as $album) {
            $first_photo = $photoRepository->getFirstPhoto($album->getId());
            $album->setFirstPhoto($first_photo);
        }

        include('../../views/album/index.php');
    }

    /**
     * xử lý hành động thêm mới album
     */
    public function actionCreate() {
        if (isset($_POST['submit'])) {
            $albumName = $_POST['albumName'];

            $album = new Album();
            $album->setUserId($this->loggedInUser->getId());
            $album->setName($albumName);

            // lưu vào csdl & cập nhật id tự sinh cho $album
            $albumCreated = $this->albumRepository->create($album);

            if (!$albumCreated) {
                $error = 'Lỗi. tạo album không thành công. vui lòng thử lại';
            }
        }

        // chuyển người dùng sang trang danh sách album
        return $this->redirect('index.php');
    }

    /**
     * xử lý hành động cập nhật album
     */
    public function actionUpdate() {
        if (isset($_POST['submit'])) {
            $id = $_GET['id'];
            $name = $_POST['album_name'];
            // lấy ra album vs $id nói trên
            $album = $this->albumRepository->getById($id);

            // nếu id không tồn tại -> chuyển về trang dsach album
            if ($album == null)
                return $this->redirect('index.php');
            $album->setName($name); // cập nhật $album vs tên mới
            // lưu thông tin cập nhật vào csdl
            $albumUpdated = $this->albumRepository->update($album);
            if (!$albumUpdated) {
                $error = 'Cập nhật album không thành công. vui lòng thử lại.';
            }
        }

        // chuyển về trang dsach album
        return $this->redirect('index.php');
    }

    /**
     * xử lý hành động xóa album
     */
    public function actionDelete() {
        $id = $_GET['id'];
        $album = $this->albumRepository->getById($id);

        // nếu id không tồn tại
        if ($album == null) {
            return $this->redirect('index.php');
        }

        $deleted = $this->albumRepository->delete($album);
        if (!$deleted)
            $error = 'Xóa album không thành công. vui lòng thử lại.';

        // chuyển về trang dsach album
        return $this->redirect('index.php');
    }
}