<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/16/2016
 * Time: 12:27 AM
 */
require_once('../../controllers/SecuredController.php');
require_once ('../../models/Profile.php');
require_once ('../../repositories/ProfileRepository.php');
require_once('../../helpers/UploadHelpers.php');

class ProfileController extends SecuredController
{
    private $profileRepository;

    public function __construct()
    {
        parent::__construct();
        $this->profileRepository = new ProfileRepository();
    }

    /**
     * xử lý cập nhật profile
     */
    public function actionUpdate() {
        $profile = $this->profileRepository->getByUserId($this->loggedInUser->getId());

        // xử lý dữ liệu gửi tới
        if (isset($_POST['submit'])) {
            $fullname = $_POST['fname'];
            $dob = DateTime::createFromFormat('d-m-Y', $_POST['dob'])->format('Y-m-d');
            $address = $_POST['address'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            $file = $_FILES['avatar'];

            $profile->setFullName($fullname);
            $profile->setDob($dob);
            $profile->setAddress($address);
            $profile->setGender($gender);
            $profile->setPhone($phone);

            // upload file & lấy về đường dẫn tới file đã upload
            $username = $this->loggedInUser->getUsername();
            $avatar = UploadHelpers::uploadImage($file, AppConfig::PATH_AVATAR."$username/");
            if ($avatar !== false) { // nếu upload thành công
                $profile->setAvatar($avatar);
            }

            $profileUpdated = $this->profileRepository->update($profile);
            if ($profileUpdated) {
                $message = 'Cập nhật thành công.';
            } else {
                $error = 'Lỗi. cập nhật profile thất bại. vui lòng thử lại.';
            }
        }

        // view
        include('../../views/profile/update.php');
    }
}