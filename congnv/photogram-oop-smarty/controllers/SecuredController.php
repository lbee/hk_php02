<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/15/2016
 * Time: 11:55 PM
 */

require_once ('../../controllers/Controller.php');
require_once ('../../config/AppConfig.php');
require_once ('../../models/User.php');
require_once ('../../repositories/ProfileRepository.php');

// khai báo sử dụng session
session_start();

/**
 * Giành cho người dùng đã đăng nhập
 */
class SecuredController extends Controller
{
    protected $loggedInUser; // object người dùng đã đăng nhập
    protected $profile;

    public function __construct()
    {
        parent::__construct();

        // bắt buộc người dùng đăng nhập trước
        $this->checkLogin();
    }

    /**
     * kiểm tra người dùng đã đăng nhập chưa
     * nếu chưa đăng nhập -> chuyển sang trang đăng nhập
     * nếu đã đăng nhập
     *  -> cập nhật $this->loggedInUser bằng object trong session
     *  -> load profile người dùng đã đăng nhập
     */
    public function checkLogin() {
        if (!isset($_SESSION[AppConfig::LOGGED_IN_USER])) {
            return $this->redirect('secure/login.php');
        } else {
            $this->loggedInUser = unserialize($_SESSION[AppConfig::LOGGED_IN_USER]);

            // load profile người dùng
            $profileRepository = new ProfileRepository();
            $this->profile = $profileRepository->getByUserId($this->loggedInUser->getId());
        }
    }
}