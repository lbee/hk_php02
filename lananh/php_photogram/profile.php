<?php 
include('includes/checklogin.php');
include('includes/ketnoi.php');

$username = $_SESSION['user'];

$sql="SELECT * FROM userprofile, user WHERE user.Username = '$username'";
$query = $conn->prepare($sql);
$query->execute();
$user = $query->fetch();
?>
<!DOCTYPE html>
<html>
<head>
	<title>User profile</title>
	<link rel="stylesheet" type="text/css" href="../../a/day17/css/bootstrap.min.css">
	<style>
		body{padding-top:30px;}

		.glyphicon {  margin-bottom: 10px;margin-right: 10px;}

		small {
		display: block;
		line-height: 1.428571429;
		color: #999;
		}
	</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                    	<?php if($user['Avatar'] != null): ?>
                        <img src="<?php echo $user['Avatar']; ?>" alt="" class="img-rounded img-responsive" />
                    	<?php else: ?>
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    	<?php endif; ?>
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4><?php echo $user['Fullname']; ?></h4>
                        <small><cite title="<?php echo $user['Address']; ?>"><?php echo $user['Address']; ?><i class="glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                       		Giới tính: <?php echo $user['Gender']==0 ? 'Nữ' : 'Nam'; ?>
							<br />
                            <i class="glyphicon glyphicon-envelope"></i><?php echo $user['Email']; ?>
                            <br />
                            <i class="glyphicon glyphicon-phone"></i><?php echo $user['Phone']; ?>
                            <br />
                            <i class="glyphicon glyphicon-gift"></i><?php echo date('F d, Y', strtotime($user['DoB'])); ?></p>
                        <!-- Split button -->
                        	<a href='albums.php' class="btn btn-primary">Xem album >></a>
                        	<a href='newuser.php' class="btn btn-primary">Thêm mới >></a>
                        	<a href='cprofile.php' class="btn btn-primary">Sửa thông tin <?php echo $user['Username']; ?> >></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../a/day17/js/jquery.js"></script>
<script type="text/javascript" src="../../a/day17/js/bootstrap.min.js"></script>
</body>
</html>