<?php
function getAlbumName($albumnID) {
	switch ($albumnID) {
		case 1: return 'Family';
		case 2: return 'Friends';
		case 3: return 'Travel';
		case 4: return 'Daily activities';
		case 5: return 'Selfie';
	}
}