<?php
include('includes/checklogin.php');
include('includes/ketnoi.php');
include('functions.php');
$username = $_SESSION['user'];

$sql = "SELECT * FROM photo WHERE Username = '$username'";
$query = $conn->query($sql);
$query->execute();
$photos = $query->fetchAll();

$albums = [];
foreach ($photos as $photo) {
	$albumID = $photo['AlbumID'];

	if (!isset($albums[$albumID])) {
		$albums[$albumID] = [];
	}

	$albums[$albumID][] = $photo;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Albums</title>
</head>
<body>

<?php foreach ($albums as $albumID => $photos): ?>
	<h2><?php echo getAlbumName($albumID); ?></h2>
	<?php foreach ($photos as $photo): ?>
	<img src="<?php echo $photo['PhotoURL']; ?>" alt="" style="width: 150px" />
	<?php endforeach; ?>
<?php endforeach; ?>
</body>
</html>