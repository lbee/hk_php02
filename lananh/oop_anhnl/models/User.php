<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:21 PM
 */
class User {
    private $id;
    private $username;
    private $password;
    private $last_login;


    public function __construct($id, $username, $password, $last_login)
    {
        $this->password = $password;
        $this->id=$id;
        $this->username=$username;
        $this->last_login=$last_login;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }


    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    public function getLastLogin()
    {
        return $this->last_login;
    }
}