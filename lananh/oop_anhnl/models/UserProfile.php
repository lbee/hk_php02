<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:21 PM
 */
class UserProfile {
    private $id;
    private $avatar;
    private $Fullname;
    private $DoB;
    private $gender;
    private $address;
    private $phone;

    public function __construct($id, $Fullname, $DoB, $address, $gender, $phone, $avatar)
    {
        $this->id = $id;
        $this->Fullname = $Fullname;
        $this->DoB = $DoB;
        $this->address = $address;
        $this->gender = $gender;
        $this->phone = $phone;
        $this->avatar = $avatar;
    }

    public function getid()
    {
        return $this->id;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function getFullname()
    {
        return $this->Fullname;
    }

    public function getDoB()
    {
        return $this->DoB;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getgender()
    {
        return $this->gender;
    }

    public function setid($id)
    {
        $this->id = $id;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function setDoB($DoB)
    {
        $this->DoB = $DoB;
    }

    public function setFullname($Fullname)
    {
        $this->Fullname = $Fullname;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
}