<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/14/2016
 * Time: 8:19 PM
 */
class Album
{
    private $user_id;
    private $id;
    private $name;

    public function __construct($user_id,$id,$name){
        $this->id=$id;
        $this->name=$name;
        $this->user_id=$user_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}