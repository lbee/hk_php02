<?php
/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/7/2016
 * Time: 7:38 PM
 */
class photo{
    private $username;
    private $photoID;
    private $AlbumID;

    public function __construct($username,$photoID,$AlbumID){
        $this->username=$username;
        $this->photoID=$photoID;
        $this->AlbumID=$AlbumID;
    }
    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPhotoID()
    {
        return $this->photoID;
    }

    public function setPhotoID($photoID)
    {
        $this->photoID = $photoID;
    }

    public function getAlbumID()
    {
        return $this->AlbumID;
    }

    public function setAlbumID($AlbumID)
    {
        $this->AlbumID = $AlbumID;
    }

}