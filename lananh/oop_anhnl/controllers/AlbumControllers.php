<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 5:11 PM
 */
include('../../repositories/AlbumRepository.php');
include('../../models/User.php');
class AlbumControllers
{
    private $albumRepository;

    public function __construct()
    {
        $this->albumRepository = new AlbumRepository();
    }

    public function actionAlbum()
    {
        if (!isset($_SESSION['user'])) {
            header("Location: ../../routes/user/login.php");
        } else {
            /*$userSessionId =*/
            $user = unserialize($_SESSION['user']);
            $id = $user->getId();
            $Album = $this->albumRepository->retrieveAlbum($id);
            if (isset($_SESSION['username'])) {
                $username = $_SESSION['username'];
            }
            if (isset($_POST['submit'])) {
                $albumName = $_POST['albumName'];
                $albumCreate = $this->albumRepository->createAlbum($albumName, $id);
                if ($albumCreate) {
                    //Refresh lại trang web
                    header("Refresh:0");
                } else {
                    echo "<script>alert('Kiểm tra lại câu lệnh SQL');</script>";
                }
            }
        }
        include('../../views/album/album.php');

    }
}