<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 12:33 PM
 */
include('../../repositories/UserRepository.php');
include('../../models/User.php');
class UserControllers
{
    private $userRepository;


    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        if (isset($_POST['signin'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password_hash = md5($password);

            // kiểm tra ng dùng vs username password có tồn tại hay k
            /// lấy ra ng dùng vs username password

            $user = $this->userRepository->getUser($username, $password_hash);
            if ($user != null) {
                // biến object user trở thành 1 chuỗi
                $_SESSION['user'] = serialize($user);
                // chuyển sang trang profile
                header("location:profile.php");
            } else {
                // nếu không tồn tại - báo lỗi
                $error = "Dang nhap khong thanh cong";
            }
        }

        // view
        include('../../views/user/login.php');
    }

    public function actionLogout()
    {
        session_destroy();

        // chuyển sang trang login
        header('location: ../user/login.php');
    }

    public function actionRegister(){
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $fullname = $_POST['fullname'];
            $add = $_POST['address'];
            //format datepicker thành dạng Y-m-d giống với CSDL
            $dob = date_format(DateTime::createFromFormat("d-m-Y", $_POST['dob']), "Y-m-d");
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];
            $file = $_FILES['avatar']['name'];
            $this->userRepository->register($username, $password, $fullname, $dob, $add, $gender, $phone, $file);
        }
        include('../../views/user/register.php');
    }

}