<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 3:25 PM
 */
include('../../repositories/ProfileRepository.php');
include('../../models/User.php');
class ProfileControllers
{
    private $profileRepository;

    public function __construct()
    {
        $this->profileRepository = new ProfileRepository();
    }

    public function actionProfile()
    {
        // kiểm tra login
        if (!isset($_SESSION['user'])) {
            header("Location: ../../routes/user/login.php");
        } else {
            $user = unserialize($_SESSION['user']);
            $id=$user->getId();
            $profile = $this->profileRepository->loadProfile($id);
            if (isset($_POST['submit'])) {
                $username = $_POST['username'];
                //khi cập nhật lại user profile thì phải cấp lại session cho username để lưu trên menu
                $_SESSION['username'] = $username;
                $password = $_POST['password'];
                $fullName = $_POST['fullname'];
                $address = $_POST['address'];
                $gender = $_POST['gender'];
                $phone = $_POST['phone'];
                $avatar=$_POST['avatar'];
                //do dùng datepicker nên phải chuyển format từ datepicker dạng d-m-Y sang dạng Y-m-d để lưu vào CSDL
                $dob = DateTime::createFromFormat("d-m-Y", $_POST['dob'])->format("Y-m-d");
                $file = $_FILES['avatar']['name'];
                $result = $this->profileRepository->editProfile($id, $username, $password, $fullName, $dob, $address, $gender, $phone, $file);
                //lưu ảnh avatar vào máy
                $this->profileRepository->saveAvatar($user, $_FILES['avatar']['tmp_name']);
                if ($result) {
                    header("Refresh:0");
                } else {
                    echo "<script>alert('123');</script>";
                }
            }
            include('../../views/user/profile.php');
        }
    }
}