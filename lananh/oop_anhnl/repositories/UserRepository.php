<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 12:34 PM
 */
include ('../../repositories/Repository.php');

class UserRepository extends Repository
{

    public function getUser($username, $password)
    {
        $sql = "Select * from tbl_user WHERE username='$username' AND password='$password'";
        $result = $this->connection->query($sql);
        $user = $result->fetch();

        if ($user)
            return $this->rowToModel($user);

        return null;
    }

    /**
     * chuyển đổi từ mảng sang object
     * @param $row
     */
    public function rowToModel($user)
    {
        $id = $user['id'];
        $username = $user['username'];
        $password = $user['password'];
        $last_login = $user['last_login'];

        return new User($id, $username, $password, $last_login);
    }

    public function getNewUser($username, $password_hash, $lastlogin)
    {
        $sql = "INSERT INTO tbl_user ( username, password,last_login) VALUES ('$username', '$password_hash', '$lastlogin')";
        //echo $sql;exit;
        $results = $this->connection->query($sql);
        //$newuser = $results->fetch();

        return $results;
    }
    public function getLastId()
    {
        return $this->connection->lastInsertId();
    }
    public function register($username, $password, $fullname, $dob, $add, $gender, $phone, $file)
    {
        //mã hóa password
        $passEncrypt = md5($password);
        //Đã có CURRENT_TIMESTAMP trong CSDL nên k cần nhập vào tbl_user
        $sql = "INSERT INTO `tbl_user`(`username`,`password`) VALUES('$username','$passEncrypt')";
        $result = $this->connection->query($sql);
        //nếu thực hiện thành công
        if ($result) {
            //cấp session cho username
            $_SESSION['username'] = $username;
            //cấp session cho user_id, dùng để xử lí tác vụ liên quan đến người dùng
            $last_id = $this->connection->lastInsertId();
            $_SESSION['user_id'] = $last_id;
            //thêm record profile cho người dùng, i.e. nếu đăng kí mới thì INSERT vào bảng profile
            $sqlProfile = "INSERT INTO `tbl_profile`(`user_id`,`FullName`,`DOB`,`Address`,`Gender`,`Phone`,`Avatar`) VALUES('$last_id','$fullname','$dob','$add','$gender','$phone','$file')";
            $resultProfile = $this->connection->query($sqlProfile);
            if ($resultProfile) {
                header("Location:login.php");
            } else {
                echo "<script>alert('Cập nhật không thành công ,vui lòng thử lại!')</script>";
            }
        }
    }
}

