<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/7/2016
 * Time: 7:41 PM
 */
class Repository
{
    protected $connection;

    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function close()
    {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }

}