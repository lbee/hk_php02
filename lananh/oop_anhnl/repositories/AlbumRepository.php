<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 5:11 PM
 */
include ('../../repositories/Repository.php');
include('../../models/Album.php');

class AlbumRepository extends Repository
{
    public function retrieveAlbum($id)
    {
        $sql = "SELECT * FROM `tbl_album` WHERE user_id=$id ";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        if($row){
            return $this->rowToModel($row);
        }
        return null;
    }
    public function rowToModel($row){
        $id=$row['id'];
        $name=$row['name'];
        $user_id=$row=['user_id'];
        return new Album($id,$name,$user_id);
    }
  public function createAlbum($name,$user_id){

      $sql = "INSERT INTO `tbl_album`(`name`,`user_id`) VALUES('$name',$user_id)";
      $result = $this->connection->query($sql);
      return $result;
  }


}