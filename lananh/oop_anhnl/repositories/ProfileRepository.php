<?php

/**
 * Created by PhpStorm.
 * User: LanAnh
 * Date: 10/16/2016
 * Time: 3:25 PM
 */
include('../../repositories/Repository.php');
include('../../models/UserProfile.php');

class ProfileRepository extends Repository
{
    public function editProfile($id, $username, $password, $fullName, $dob, $address, $gender, $phone, $avt)
    {
        $passwordEncrypted = md5($password);
        $sql = "UPDATE `tbl_profile` /*AS t INNER JOIN `tbl_user` AS u ON t.user_id = u.id SET t.user_id='$id',t.FullName='$fullName',t.DOB='$dob',t.Address='$address',t.Gender='$gender',
t.Phone='$phone',t.Avatar='$avt',u.username='$username',u.password='$passwordEncrypted'*/ WHERE user_id=" . $id;

        $result = $this->connection->query($sql);
        return $result;
    }

    public function loadProfile($id)
    {
        //var_dump($user_id);die;
        //$user_id = 40;
        $sql = "SELECT * FROM `tbl_profile` AS p INNER JOIN `tbl_user` AS u ON p.user_id=u.id WHERE user_id= '$id'";
        //var_dump($this->conn);die;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        //var_dump($row);die;
        if ($row) {

            return $this->rowToModel($row);
        }
        return null;
    }

    public function rowToModel($row)
    {
        $id = $row['id'];
        $avatar = $row['Avatar'];
        $Fullname = $row['FullName'];
        $DoB = $row['DOB'];
        $gender = $row['Gender'];
        $address = $row['Address'];
        $phone = $row['Phone'];
        return new UserProfile($id, $Fullname, $DoB, $address, $gender, $phone, $avatar);
    }

    public function saveAvatar($user, $file, $fileTmpName)
    {

        if ($_FILES['Avatar']['name'] != '') {
            // upload file avatar
            $target_dir = "../../imgavatar/$user->getUsername()/";
            // nếu thư mục chưa tồn tại >> tạo mới
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0700);
            }
            $file = $_FILES["Avatar"]["name"];
            $target_file = $target_dir . basename($file);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            // kiểm tra file có phải là ảnh hay không
            if (isset($_POST["submit"])) {
                $fileTmpName = $_FILES["Avatar"]["tmp_name"];
                $check = getimagesize($fileTmpName);

                if ($check == false) { // không phải ảnh
                    // báo lỗi
                    $error = 'Avatar phải là ảnh.';
                    $uploadOk = 0;
                }
            }

            // kiểm tra xem file đã tồn tại hay chưa
            if (file_exists($target_file)) {
                $error = 'Lỗi, file đã tồn tại.';
                $uploadOk = 0;
            }
            // kiểm tra size có vượt quá dung lượng cho phép hay không
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                $error = "File kích thước quá lớn.";
                $uploadOk = 0;
                // kiểm tra định dạng file cho phép
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif"
                ) {
                    $error = "Định dạng file không hỗ trợ (JPG, JPEG, PNG & GIF)";
                }

                // if everything is ok, try to upload file
                if ($uploadOk) {
                    if (!move_uploaded_file($_FILES["Avatar"]["tmp_name"], $target_file)) {
                        $error = 'Lỗi không thể upload file. Vui lòng thử lại.';
                    }
                }
                // lấy file mới
                $avatar = $target_file;
                return $avatar;
            }

        }
    }

}
