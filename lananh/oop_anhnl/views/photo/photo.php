<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quản lí album</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Photo Album</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="../album/index.php">Quản lí album <span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <!-- ảnh avt -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="<?php echo $user->getUsername() . '/' . $profile->getavatar()?>" alt="avatar" width="24" height="24"/><?php echo $username; ?><span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../userprofile/index.php">Profile</a></li>
                        <li><a href="../userprofile/logout.php">Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!-- Chỉnh sửa album -->
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Danh sách photo thuộc album</h3>
        </div>
        <div class="panel-body">

            <div class="ui">
                <!-- Form để upload ảnh -->
                <form method="post" enctype="multipart/form-data">
                    Upload image:
                    <!-- Chấp nhận mọi thể loại ảnh sử dụng accept attr-->
                    <input type="file" class="ui primary button" accept="image/*" id="fileUpload" name="photoUpload"/>
                    <!-- Nút submit, bị ẩn nếu như file chưa được chọn -->
                    <button name="submit" class="ui secondary button hide submit" type="submit">
                        Save
                    </button>
                    <button class="ui button hide cancel" type="submit">
                        Cancel
                    </button>
                </form>
            </div>
            <br/>

            <!-- lấy danh sách ảnh để ở đây-->
            <div class="ui special cards three column">
                <?php $result = retrievePhoto($conn, $album_id);
                while ($row = mysqli_fetch_assoc($result)) {
                    ?>
                    <div class="card">
                        <div class="blurring dimmable image">
                            <div class="ui inverted dimmer">
                                <div class="content">
                                    <div class="center">
                                        <a href="#">
                                            <div class="ui button">Xem</div>
                                        </a>
                                        <!-- Truyền 2 tham số (1): Photo id để xóa photo (2): album_id để quay về album-->
                                        <a href="#">
                                            <div class="negative ui button">Xóa</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <img src="<?php echo $row['photoURL'] . '/' . $row['photoName']; ?>" alt="">
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<!-- End -->

<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../assets/components/semantic/dist/semantic.min.js"></script>
<script src="../../assets/js/dim.js"></script>

</body>
</html>