<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:21 PM
 */
class User {
    private $id;
    private $username;
    private $password;
    private $last_login;

    public function __construct($id, $username, $password, $last_login)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->last_login = $last_login;
    }

    public function __contruct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getLastLogin() {
        return $this->last_login;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public  function setLastLogin($last_login) {
        $this->last_login = $last_login;
    }
}