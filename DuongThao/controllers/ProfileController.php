<?php
include('../../repositories/UserRepository.php');
include('../../repositories/ProfileRepository.php');
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14/10/2016
 * Time: 19:21 CH
 */
class ProfileController
{
    private $profileRepository;


    public function __construct()
    {
        $this->profileRepository = new ProfileRepository();


    }
    // hành động của xem thông tin
    //xem thông tin
    public function actionProfile()
    {
        if(!isset($_SESSION['_user'])){
            header('location: ../user/login.php');
        }
        $user=unserialize($_SESSION['_user']);

        // lấy ra id của ng dùng vừa thêm vào
        $user_id = $user->getId();

        // tạo profile
        $profile = $this->profileRepository->getProfile($user_id);
        // view
        include('../../views/profile/profile.php');
    }

    public function actionEditProfile(){
        if(!isset($_SESSION['_user'])){
            header('location: ../user/login.php');
        }

        $user=unserialize($_SESSION['_user']);
        // lấy ra id của ng dùng vừa thêm vào
        $user_id = $user->getId();

        // tạo profile
        $profile = $this->profileRepository->getProfile($user_id);
        if (isset($_POST['submit'])) {
            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $dob = $_POST['dob'];
            $gender = $_POST['gender'];
            $phone = $_POST['phone'];

            if ($_FILES['avatar']['name'] != '') {
                $avatar = $_FILES['avatar']['name'];
                // lưu ảnh avatar
                $this->profileRepository->saveAvatar($user->getUsername(), $avatar, $_FILES['avatar']['tmp_name']);
            } else {
                $avatar = $_POST['avatar'];
            }
            $result = $this->profileRepository->editProfile($user_id,$fullname, $dob, $address, $gender, $phone, $avatar);


        }
        // view
        include('../../views/profile/editProfile.php');
    }
}