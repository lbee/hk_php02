<?php
include('../../repositories/UserRepository.php');
include('../../repositories/ProfileRepository.php');
include('../../repositories/AlbumRepository.php');

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 17/10/2016
 * Time: 20:22 CH
 */
class AlbumController
{
    private $albumRepository;


    public function __construct()
    {
        $this->albumRepository = new AlbumRepository();
    }

    /**
     * phương thức xử lý hành động
     */
    public function actionIndex()
    {
        if(!isset($_SESSION['_user'])){
            header('location: ../user/login.php');
        }
        $user=unserialize($_SESSION['_user']);

        // lấy ra id của ng dùng vừa thêm vào
        $user_id = $user->getId();

        $albums = $this->albumRepository->getAlbums($user_id);

        include_once ('../../views/album/index.php');
    }

     public function actionAdd(){
         if(!isset($_SESSION['_user'])){
             header('location: ../user/login.php');
         }
         $user=unserialize($_SESSION['_user']);

         // lấy ra id của ng dùng vừa thêm vào
         $user_id = $user->getId();
         if(isset($_POST['submit'])){
             $albumName = $_POST['albumName'];
             $result = $this->albumRepository->creatAlbum($albumName,$user_id);

             if(!$result){
                 $error = "lỗi tạo mới album";
             }

             header('location: index.php');
         }
     }
     public function actionEdit(){
         if(!isset($_SESSION['_user'])){
             header('location: ../user/login.php');
         }
         $user=unserialize($_SESSION['_user']);
         if(isset($_POST['submit'])){
             $albumId = $_GET['id'];
             $albumName = $_POST['album_name'];
             $result = $this->albumRepository->editAlbum($albumId, $albumName);
             if(!$result){
                 $error = "lỗi sửa album";
             }

             header('location: index.php');
         }

     }
     public function actionDelete(){
         if (!isset($_SESSION['_user'])){
             header('location: ../user/login.php');
         }
         $user=unserialize($_SESSION['_user']);
         if(isset($_GET['id'])){
             $albumId = $_GET['id'];
             $result = $this->albumRepository->deleteAlbum($albumId);
             if(!$result){
                 $error = "lỗi xóa";
             }
             header('location: index.php');
         }
     }
}