<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 17/10/2016
 * Time: 20:24 CH
 */
include_once('../../models/Album.php');

class AlbumRepository
{

    private $connection;

    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect()
    {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function close()
    {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }

    /**
     * lấy ra dsach các album cho người dùng có $user_id
     * @return mixed
     */
    public function getAlbums($user_id)
    {
        $sql = "SELECT * FROM tbl_album WHERE user_id='$user_id'";
        $result = $this->connection->query($sql);
        $rows = $result->fetchAll();

        // nếu tồn tại ng dùng
        $albums = array();
        foreach ($rows as $row) {
            $album = $this->rowToModel($row);
            // thêm album vào mảng album
            $albums[] = $album;
        }
        return $albums;
    }

    public function rowToModel($row)
    {
        $id = $row['id'];
        $name = $row['name'];
        $user_id = $row['user_id'];

        return new Album($id, $name, $user_id);
    }

    public function getById($id) {
        $sql = "SELECT * FROM tbl_album WHERE id='$id'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();
        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    public function creatAlbum($name, $user_id)
    {
        $sql = "INSERT INTO tbl_album (name, user_id) VALUES ('$name','$user_id') ";
        $result = $this->connection->query($sql);
        if(!$result){
            return false;
        }else{
            return $result;
        }
    }

    public function editAlbum($id, $album_name){
        $sql = "UPDATE tbl_album SET name = '$album_name' WHERE id = $id";
        $result = $this->connection->query($sql);
        if(!$result){
            return false;
        }else{
            return $result;
        }
    }

    public function deleteAlbum($id){
        $sql = "DELETE FROM tbl_album WHERE id = $id";
        $result = $this->connection->query($sql);
        if(!$result){
            return false;
        }else{
            return $result;
        }
    }

}