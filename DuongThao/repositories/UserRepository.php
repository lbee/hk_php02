<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/5/2016
 * Time: 8:23 PM
 */
include('../../models/User.php');

class UserRepository {
    private $connection;

    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect() {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function close() {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }

    /**
     * trả về người dùng vs $username & $password
     * trả về null nếu không tồn tại
     * @param $username
     * @param $password hashed_password
     */
    public function getUser($username, $password) {
        $sql = "SELECT * FROM tbl_user WHERE username='$username' AND password='$password'";
        $result = $this->connection->query($sql);
        $row = $result->fetch();

        // nếu tồn tại ng dùng
        if ($row)
            return $this->rowToModel($row);

        return null;
    }

    /**
     * chuyển đổi từ mảng sang object
     * @param $row
     */
    public function rowToModel($row) {
        $id = $row['id'];
        $username = $row['username'];
        $password = $row['password'];
        $last_login = $row['last_login'];

        return new User($id, $username, $password, $last_login);
    }

    /**
     * tạo  người dùng với $username & password
     */
    public function create($username, $password_hash) {
        $sql = "INSERT INTO tbl_user (username, password) VALUES ('$username','$password_hash')";
        $result = $this->connection->query($sql);
        if ($result)
            return true;
        else
            return false;
    }

    public function getLastId() {
        return $this->connection->lastInsertId();
    }
}

