<?php

/**
 * Created by PhpStorm.
 * User: FoxTattoo
 * Date: 10/17/2016
 * Time: 3:03 PM
 */
include_once ('../../models/User.php');
include_once ('../../models/UserProfile.php');
include_once ('../../models/Album.php');
include_once ('../../models/Photo.php');
include_once ('Repository.php');
class PhotoRepository
{
    private $connection;
    /**
     * Thiết lập cơ sở dữ liệu và 1 số settings
     */
    public function connect() {
        $host = "localhost";
        $username = "root";
        $password = "";
        $dbname = "photo";

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function close() {
        $this->connection = null;
    }

    public function __construct()
    {
        // kết nối csdl
        $this->connect();
    }

    public function __destruct()
    {
        // đóng kết nối tới csdl
        $this->close();
    }
    /**
     * Lấy danh sách photo thuộc album_id
     * @param $conn : PDO Connection
     * @param $album_id : id album
     * @return mixed
     */
    public function retrievePhoto($album_id)
    {
        //lấy danh sách photo được sắp xếp theo id giảm dần
        $sql = "SELECT * FROM tbl_photo WHERE album_id = '$album_id'";
//        echo $sql;die();
        $result = $this->connection->query($sql);
        $row = $result->fetchAll();
        // var_dump($row); die();

        return $row;
    }

    /**
     * Upload photo
     * @param $conn : PDO connection
     * @param $album_id : Album_id
     * @param $path : Đường dẫn lưu ảnh
     * @param $photoName : Tên ảnh
     */
    public function uploadPhoto($album_id, $path, $photoName)
    {
        $sql = "INSERT INTO tbl_photo(album_id, photoName, photoURL) VALUES ('$album_id','$photoName','$path')";
        // echo $sql; die();
        $result = $this->connection->query($sql);
        return $result ? true : false;
    }

    // Xóa Ảnh
    public function deletePhoto($id) {
        //bắt đầu truy vấn
        $sql = "DELETE FROM tbl_photo WHERE id= $id";
        $result = $this->connection->query($sql);

        return $result;
    }

    /**
     * Xem chi tiết ảnh fullsize của photo, dựa trên đường dẫn có sử dụng $username
     * @param $conn
     * @param $id
     * @param $username
     * @return string
     */
    public function viewPhoto($id)
    {
        $sql = "SELECT * FROM tbl_photo WHERE id=$id";
        //echo $sql, die();
        $obj = $this->connection->query($sql);
        $row = $obj->fetch();
        //var_dump($row);die();
        return $row;
        //nếu có kết quả trả về, i.e. số records > 0
//        if ($row) {
//
//            return PHOTOPATH . $username . '/' . $row['photoName'];
//        } else {
//            return "";
//        }
    }

    public function rowToModel($row){
        $id = $row['id'];
        $album_id = $row['album_id'];
        $photoName = $row['photoName'];
        $photoURL = $row['photoURl'];
        return new Album($id, $album_id, $photoName, $photoURL);
    }
}