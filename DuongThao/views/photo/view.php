<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Xem ảnh</title>
    <link rel="stylesheet" href="../../assets/css/reset.css"/>
    <link rel="stylesheet" href="../../assets/components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../assets/components/semantic/dist/semantic.min.css"/>

</head>
<body>

<img src="<?php echo PHOTOPATH . $username . '/' . $viewphoto['photoName']; ?>" alt=""/>

<script src="../../assets/components/jquery/dist/jquery.min.js"></script>
<script src="../../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../assets/components/semantic/dist/semantic.min.js"></script>
<script src="../../assets/js/dim.js"></script>
</body>
</html>